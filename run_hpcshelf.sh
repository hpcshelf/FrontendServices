#!/bin/sh

export CORE_ADDRESS=127.0.0.1
export CORE_PORT=8080
export PlatformSAFe_ADDRESS=127.0.0.1
export PlatformSAFe_PORT=8079
export TEMPLATE_CODE="$HPCShelf_HOME/Case Study - Gust/HPC-Shelf-Gust/br.ufc.mdcc.hpcshelf.mpi.impl.MPILauncherForCImpl/MPILauncherForCImpl.hpe"

cd $HPCShelf_HOME/FrontendServices/HPCShelfCLI/bin/Debug

chmod +x ./hpc_shelf_service.exe
rm hpc_shelf_service.lock
mono-service hpc_shelf_service.exe -d:./ -l:hpc_shelf_service.lock --debug --no-daemon

cd $HPCShelf_HOME
