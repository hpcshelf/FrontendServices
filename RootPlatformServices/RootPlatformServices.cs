﻿using System;
using System.IO;
using org.hpcshelf.DGAC.utils;
//using org.hpcshelf.DGAC.database;
using org.hpcshelf.DGAC;
using System.Collections.Generic;
using System.ServiceModel.Description;
using System.ServiceModel;
using System.Net;
using org.hpcshelf.exception;
using System.Web.Services.Protocols;
using org.hpcshelf.common.utils;
using DGAC;

namespace org.hpcshelf.backend.platform
{

    public class RootPlatformServices0 : IPlatformServices
    {
        public string allocateBindingAddress(string channel_id, int n)
        {
            return RootPlatformServices.SingleSAFePlatformServices.allocateBindingAddress(channel_id, n);
        }

        public string deploy(string module_data, byte[] key_file_contents)
        {
            return RootPlatformServices.SingleSAFePlatformServices.deploy(module_data, key_file_contents);
        }

        public int getBaseBindingPort()
        {
            return RootPlatformServices.SingleSAFePlatformServices.getBaseBindingPort();
        }

        public int getNumberOfNodes()
        {
            return RootPlatformServices.SingleSAFePlatformServices.getNumberOfNodes();
        }

        public string getPlatformRef()
        {
            return RootPlatformServices.SingleSAFePlatformServices.getPlatformRef();
        }

        public string getStatus()
        {
            return RootPlatformServices.SingleSAFePlatformServices.getStatus();
        }

        public string instantiate(string component_ref, string[] ctree, int facet_instance, string[] channel_id, int[] channel_facet_instance, string[] channel_facet_address)
        {
            return RootPlatformServices.SingleSAFePlatformServices.instantiate(component_ref, ctree, facet_instance, channel_id, channel_facet_instance, channel_facet_address);
        }

        public void run(string component_ref)
        {
            RootPlatformServices.SingleSAFePlatformServices.run(component_ref);
        }

        public void release(string component_ref)
        {
            RootPlatformServices.SingleSAFePlatformServices.release(component_ref);
        }

        public void setPlatformRef(string arch_ref)
        {
            RootPlatformServices.SingleSAFePlatformServices.setPlatformRef(arch_ref);
        }
    }


    public class RootPlatformServices :  IPlatformServices
	{
        static int service_port = 8075;

        private static RootPlatformServices singleton = null;
        public static RootPlatformServices SingleSAFePlatformServices { get { if (singleton == null) singleton = new RootPlatformServices(); return singleton; }} 

        protected RootPlatformServices()
        {
            StartHTTPService(service_port);
        }

		private static ServiceHost m_svcHost;

		private static void StartHTTPService(int nPort)
		{
			string strAdr = "http://localhost:" + nPort.ToString() + "/PlatformSAFe";
            try
            {
                Uri adrbase = new Uri(strAdr);
                m_svcHost = new ServiceHost(typeof(RootPlatformServices0), adrbase);

                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                m_svcHost.Description.Behaviors.Add(mBehave);
                m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

                BasicHttpBinding httpb = new BasicHttpBinding();
                m_svcHost.AddServiceEndpoint(typeof(IPlatformServices), httpb, strAdr);
                m_svcHost.Open();
                Console.WriteLine("\n\nService is Running as >> " + strAdr);
            }
			catch (Exception eX)
			{
				m_svcHost = null;
				Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
			}
		}

		/* config_contents is the contents of the .hpe file */

		public int getNumberOfNodes()
		{
			string path_hosts_file = System.Environment.GetEnvironmentVariable ("PATH_HOSTS_FILE");
			if (path_hosts_file == null || path_hosts_file.Equals(""))
				path_hosts_file = System.Environment.GetEnvironmentVariable("HOME") + Path.PathSeparator + "hpe_nodes";

			string[] contents = File.ReadAllLines (path_hosts_file);
			int size = contents.Length;

			Console.WriteLine ("getNumberOfNodes was called and returned " + size);

			return size - 1;
		}


		private string address = null;

		public string Address
		{
			get { return address; } set { address = value; }
		}

		
		public int getBaseBindingPort()
		{
			int base_binding_port = -1;

			try
			{
				base_binding_port = Constants.BASE_BINDING_FACET_PORT;
			}
			catch (Exception e) 
			{
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				if (e.InnerException != null) 
				{
					Console.Error.WriteLine (e.InnerException.Message);
					Console.Error.WriteLine (e.InnerException.StackTrace);
				}
			}
			return base_binding_port;
		}


        public string deploy(string module_data, byte[] key_file_contents)
		{
            try
            {
                DeployArguments.DeployArgumentsUnitType infoCompile = LoaderApp.deserialize<DeployArguments.DeployArgumentsUnitType>(module_data);

                string path = Path.Combine(Constants.PATH_GAC, infoCompile.library_path);

                if (true /*!Directory.Exists(path)*/)
                {
                    SourceDeployer manager = new SourceDeployer();

                    return BackEnd.sendToCompile(manager, infoCompile, key_file_contents, null, null, null);
                }
                else
                    return "already deployed !";
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Local deploy failied: {0}", e.Message);
                Console.Error.WriteLine("STACK TRACE: {0}", e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.Error.WriteLine("INNER EXCEPTION: {0}", e.InnerException.Message);
                    Console.Error.WriteLine("INNER EXCEPTION STACK TRACE: {0}", e.InnerException.StackTrace);
                }


                throw e;
            }
        }


        private static string platform_ref = null;

		
		public void setPlatformRef(string arch_ref)
		{
			platform_ref = arch_ref;
		}

		
		public string getPlatformRef()
		{
			return platform_ref;
		}

		/* component_ref é a referência do componente no sistema computacional (identificador do componente aninhado do componente de sistema).
		 * A plataforma já possui o componente de sistema previamente instanciado. 
		 */

		public string instantiate(string component_ref, string[] ctree_str_list, int facet_instance, string[] channel_id, int[] channel_facet_instance, string[] channel_facet_address)
		{
			try
			{
                string system_name = component_ref.Substring(0, component_ref.IndexOf('.'));

                Console.WriteLine("ROOT PLATFORM SERVICES -- instantiate {0} in system {1}", component_ref, system_name);

                if (component_ref.EndsWith(Constants.PLATFORM_SAFE_ID))
                {
                    session[system_name] = ParallelComputingSystem.newSystem(component_ref, facet_instance);

                    Console.WriteLine("instantiate -- session.Count={0}", session.Count);
                    foreach (string s in session.Keys)
                        Console.WriteLine("instantiate -- session[{0}]", s);
                }
                else
                {
                    for (int i = 1; i < ctree_str_list.Length; i++)
                    {
                        string ctree_host_str = ctree_str_list[i];
						XMLComponentHierarchy.ComponentHierarchyType ctree_host_xml = LoaderApp.deserialize<XMLComponentHierarchy.ComponentHierarchyType>(ctree_host_str);
                        BackEnd.ResolveComponentHierarchy ctree_host = ConverterComponentHierachyToXML.readComponentHierarchy(ctree_host_xml, ctree_host_xml.id);
					}

                    string ctree_str = ctree_str_list[0];

                    XMLComponentHierarchy.ComponentHierarchyType ctree_xml = LoaderApp.deserialize<XMLComponentHierarchy.ComponentHierarchyType>(ctree_str);
                    BackEnd.ResolveComponentHierarchy ctree = ConverterComponentHierachyToXML.readComponentHierarchy(ctree_xml, component_ref);
                    ISystem system = ParallelComputingSystem.SystemInstance[session[system_name]];
                    system.ChannelID = channel_id;
                    system.ChannelFacetInstance = channel_facet_instance;
                    system.ChannelFacetAddress = channel_facet_address;
                    system.newComponentInstance(component_ref, ctree);
				}
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
			}

			string result = "instantiated";

			return result;
		}

		private static IDictionary<string, BackEnd.ISession> session = new Dictionary<string, BackEnd.ISession>();

        public IDictionary<string, BackEnd.ISession> Session { get { return session; } }


		private static string status = "";

		
		public string getStatus()
		{
			return status;
		}

		
		public void run (string component_ref)
		{
            string system_name = component_ref.Substring(0, component_ref.IndexOf('.'));

            status += platform_ref + ": * GO!!! BEGIN " + component_ref + "\n";

            BackEnd.runSolutionComponent (session[system_name].Services, component_ref);

			status += platform_ref + ": * GO!!! STARTED " + component_ref + "\n";
		}


        public void release(string component_ref)
        {
            try
            {
                string system_name = component_ref.Substring(0, component_ref.IndexOf('.'));
                Console.WriteLine("RELEASE X -- {0} / {1} / {2}", platform_ref, component_ref, system_name);

                Console.WriteLine("release -- session.Count={0}", session.Count);
                foreach (string s in session.Keys)
                    Console.WriteLine("release -- session[{0}]", s);

                if (component_ref.EndsWith(Constants.PLATFORM_SAFE_ID))
                    session.Remove(system_name);
                else
                {

                    if (!session.ContainsKey(system_name))
                        throw new UninstantiatedPlatformException(platform_ref);

                    ISystem system = ParallelComputingSystem.SystemInstance[session[system_name]];

                    system.releaseComponentInstance(component_ref);
                }
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "VirtualPlatformServices.release");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "VirtualPlatformServices.release", WebServicesException.FaultCode.Server);
            }
        }


        private int next_port = -1;

		public string allocateBindingAddress(string channel_id, int n)
		{
			if (next_port == -1)
				next_port = this.getBaseBindingPort();

            string binding_address_local = "127.0.0.1";           // Environment.GetEnvironmentVariable("BINDING_ADDRESS_LOCAL");
            string binding_address_global = Utils.GetPublicIpAddress(); // Environment.GetEnvironmentVariable("BINDING_ADDRESS_GLOBAL");

			string return_address = binding_address_local + ":" + next_port + ";" + binding_address_global + ":" + next_port;

			next_port += n;

			return return_address;

		}


	}
	}

