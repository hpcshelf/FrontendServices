﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Timers;
//using org.hpcshelf.common.utils;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using CoreCachedServices;
using gov.cca;
using org.hpcshelf.common.utils;

namespace org.hpcshelf.core
{
    public class CoreServices : ICoreServices
    {
        #region Service Methods

        private Services services;

        private ProvenanceManager provenance_manager = null;
        private LifeCycleManager lifecycle_manager = null;

        private CoreWebServices core_access = null;

        private AbstractFramework frw;

        private static string core_address = Constants.CORE_ADDRESS;
        public static string CORE_ADDRESS { get => core_address; set => core_address = value; }

        private static int core_port = Constants.CORE_PORT;
        public static int CORE_PORT { get => core_port; set => core_port = value; }

        private static string platformSAFe_ADDRESS = "127.0.0.1";   
        public static string PlatformSAFe_ADDRESS { get => platformSAFe_ADDRESS; }

        private static int platformSAFe_PORT = 8079;
        public static int PlatformSAFe_PORT { get => platformSAFe_PORT; set => platformSAFe_PORT = value; }

        public CoreServices(string app_name, out string session_id) : this()
        {
            this.app_name = app_name;

            string core_url = "http://" + CORE_ADDRESS + ":" + CORE_PORT + "/CoreWebServices.asmx";

            string errorMessage;
            Utils.checkConnection(core_url, 10000, false, out errorMessage);
            if (errorMessage != string.Empty)
            {
                errorMessage = string.Format("The Core services at {0}:{1} is not reachable. " +
                	                         "\n(1) Check your connectivity, (or)" +
                	                         "\n(2) contact the Core services administrator, or " +
                	                         "\n(3) change the target Core services.", core_address, core_port);
                throw new Exception(errorMessage);
            }
            else
            {
                Console.WriteLine("The Core services is accessible at {0}:{1} !", core_address, core_port);
            }

            core_access = new CoreWebServices(core_url);
			core_access.Timeout = int.MaxValue;

            platformSAFe_ADDRESS = CORE_ADDRESS.Equals("127.0.0.1") ? "127.0.0.1" : Utils.GetPublicIpAddress();

            Console.WriteLine("******* PLATFORM SAFE ADDRESS = {0}, PORT = {1}", PlatformSAFe_ADDRESS, PlatformSAFe_PORT);

			session_id = this.session_id = core_access.newCoreServices(app_name, PlatformSAFe_ADDRESS, PlatformSAFe_PORT);

			this.provenance_manager = new ProvenanceManager(this);
            this.lifecycle_manager = new LifeCycleManager(this);

            frw = BackEnd.getFrameworkInstance(app_name);

            services = frw.getServices("Core." + this.app_name, "CoreServices", new TypeMapImpl());

            Services.addProvidesPort(ProvenanceManager, Constants.CORE_PROVENANCE_PORT_NAME, Constants.PROVENANCE_PORT_TYPE, new TypeMapImpl());
            Services.addProvidesPort(lifecycle_manager, Constants.CORE_LIFECYCLE_PORT_NAME, Constants.LIFECYCLE_PORT_TYPE, new TypeMapImpl());
        }


        string session_id;

        System.Timers.Timer heartbeat_timer = null;

        public CoreServices()
        {
            Console.Write("STARTING HEARTBEAT TIMER -- {0} ... ", session_id);
            heartbeat_timer = new System.Timers.Timer(30 * 1000); // a cada 30s
            heartbeat_timer.Elapsed += OnTimedEvent;
            heartbeat_timer.AutoReset = true;
            heartbeat_timer.Start();
            Console.WriteLine("OK.");
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("HEARBEAT {0}", session_id);
            core_access.heartbeat(session_id);
        }

        ~CoreServices()
        {
            Console.WriteLine("STOPPING HEARBEAT TIMER {0}", session_id);
            heartbeat_timer.Stop();
        }

        private string app_name;

        public Services Services { get => services; }
        public ProvenanceManager ProvenanceManager { get { if (provenance_manager == null) provenance_manager = new ProvenanceManager(this); return provenance_manager; } }

        public AbstractFramework Framework { get => frw; set => frw = value; }

        // PARALLEL COMPUTING SYSTEM
        public void open()
        {
            core_access.open(session_id);
        }
		
        // PARALLEL COMPUTING SYSTEM
		public void open(string contract)
		{
			core_access.open_contract(session_id, contract);
		}

        // PARALLEL COMPUTING SYSTEM
        public void close()
        {
            core_access.close(session_id);
            BackEnd.releaseFrameworkInstance(app_name);
        }

        public string insertComponent(string solution_component_xml)
        {
            return core_access.insertComponent(session_id, solution_component_xml);
        }

        public void insertParameter(string parameter_id, string contractual_restriction)
        {
            core_access.insertParameter(session_id, parameter_id, "application", contractual_restriction);
        }

		public void insertParameter(string parameter_id, string parameter_class, string contractual_restriction)
		{
            core_access.insertParameter(session_id, parameter_id, parameter_class, contractual_restriction);
		}
		
        public void updateContract(string contract_xml)
        {
            core_access.updateContract(session_id, contract_xml);
        } 

        public void resolve(string c_ref)
        {
            core_access.resolve(session_id, c_ref);
        }

		public void resolve_system(string c_ref)
		{
            core_access.resolve_system(session_id, c_ref);
		}
		
        public string deploy(string arch_ref)
        {
			return core_access.deploy(session_id, arch_ref);
		}

        public string instantiate(string component_ref)
        {
			return core_access.instantiate(session_id, component_ref);
		}

        public void run(string arch_ref)
        {
            Console.WriteLine("CoreServices (frontend) ---- RUN {0}", arch_ref);

            core_access.run(session_id, arch_ref);
		}

        public void release(string arch_ref)
        {
			core_access.release(session_id, arch_ref);
		}

        public void certify(string certifiable_id)
        {
            core_access.certify(session_id, certifiable_id);
		}

		public void start(int level)
		{
            core_access.start(session_id, level);
		}

		public string confirm()
		{
            return core_access.confirm(session_id);
		}

		public void cancel()
		{
            core_access.cancel(session_id);
		}

        public string getVirtualPlatformAddress(string platform_ref)
        {
            return core_access.getVirtualPlatformAddress(session_id, platform_ref);
        }

        #endregion

        // REGISTER COMPONENTE IN THE CATALOG !
        public string register(string module_data, byte[] snk_contents)
        {
            return core_access.register(session_id, module_data, snk_contents);
		}

        private class LifeCycleManager : ICoreServices, Port
        {
            private CoreServices coreServices;

            public LifeCycleManager(CoreServices coreServices)
            {
                this.coreServices = coreServices;
            }

            public void certify(string arch_ref)
            {
                ((ICoreServices)coreServices).certify(arch_ref);
            }

            public string deploy(string arch_ref)
            {
                return ((ICoreServices)coreServices).deploy(arch_ref);
            }

            public string instantiate(string arch_ref)
            {
                return ((ICoreServices)coreServices).instantiate(arch_ref);
            }

            public void release(string arch_ref)
            {
                ((ICoreServices)coreServices).release(arch_ref);
            }

            public void resolve(string arch_ref)
            {
                ((ICoreServices)coreServices).resolve(arch_ref);
            }
			
            public void resolve_system(string arch_ref)
			{
				((ICoreServices)coreServices).resolve_system(arch_ref);
			}

			public void run(string arch_ref)
            {
                ((ICoreServices)coreServices).run(arch_ref);
            }
        }
    }
}
