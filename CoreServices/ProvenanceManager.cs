﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using org.hpcshelf.DGAC;
//using org.hpcshelf.DGAC.database;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.ports;
using org.hpcshelf.provenance;
using gov.cca;

namespace org.hpcshelf.core
{
	public class ProvenanceManager : ProvenancePort
	{
		private int provenance_level;
		public int ProvenanceLevel { get { return this.provenance_level; } }

		private IProvenanceTracer provenance_tracer;
		private CoreServices core_services;

		public IProvenanceTracer ProvenanceTracer { set { provenance_tracer = value; } }

		public string SystemRef => null;

		public ProvenanceManager(CoreServices core_services)
		{
			this.core_services = core_services;
		}

		public void start(int level)
		{
            this.provenance_level = level;
            core_services.start(level);
		}

		public string confirm()
		{
            return core_services.confirm();
		}

		public void cancel()
		{
            core_services.cancel();
		}
	}

}
