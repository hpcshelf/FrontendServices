﻿using System;
using System.IO;
using BackendLauncher;
using org.hpcshelf.backend;
using org.hpcshelf.command;

public class LocalBackendLauncher : IBackendLauncher
{
    private string backend_address = "<pending>";
    public string BackendAddress { get { return string.Format("http://{0}:8081/BackendServices.asmx", "127.0.0.1"); } }

    public void launch()
    {
        string path = Environment.GetEnvironmentVariable("Swirls_HOME");
        if (path == null)
        {
            Console.WriteLine("Swirls_HOME not set. Using the current directory.");
            path = Utils.CURRENT_FOLDER;
        }

        string script_command = Path.Combine(path, "run_local-backend_services.sh");

        Utils.commandExecBash(script_command);
    }

    public string launch(string name, string region_id, org.hpcshelf.core.CoreServices core_services)
    {
        launch();

        return BackendAddress;
    }

    public string start()
    {
        throw new NotImplementedException();
    }

    public void stop()
    {
        throw new NotImplementedException();
    }

    public void terminate()
    {
        throw new NotImplementedException();
    }
}
