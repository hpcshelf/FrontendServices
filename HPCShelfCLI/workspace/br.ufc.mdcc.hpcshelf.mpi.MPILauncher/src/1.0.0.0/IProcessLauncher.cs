using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;

namespace br.ufc.mdcc.hpcshelf.mpi.MPILauncher
{
	public interface IProcessLauncher<L> : BaseIProcessLauncher<L>
		where L:ILanguage
	{
	}
}