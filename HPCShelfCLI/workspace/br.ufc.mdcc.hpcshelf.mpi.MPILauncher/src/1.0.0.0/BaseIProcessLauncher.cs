/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;

namespace br.ufc.mdcc.hpcshelf.mpi.MPILauncher
{
	public interface BaseIProcessLauncher<L> : IComputationKind 
		where L:ILanguage
	{
	}
}