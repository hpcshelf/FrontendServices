using System.Runtime.InteropServices;
using br.ufc.mdcc.hpcshelf.mpi.language.C;
using br.ufc.mdcc.hpcshelf.mpi.MPILauncher;

namespace br.ufc.mdcc.hpcshelf.mpi.impl.MPILauncherForCImpl
{
	using MPI_Comm = System.Int32;

	public class IProcessLauncherForCImpl<L> : BaseIProcessLauncherForCImpl<L>, IProcessLauncher<L>
		where L:ILanguage_C
	{
		public override void main()
        {
            Run t = new Run();
            Run.run(this.Communicator.comm);
        }
	}

    class Run
    {
		[DllImport("/opt/mono-4.2.2/lib/mono/lib_c/mpirun.so", EntryPoint = "run")]
		public static extern void run(MPI_Comm comm);
	}
}
