/* Automatically Generated Code */

using br.ufc.mdcc.hpcshelf.mpi.language.C;
using br.ufc.mdcc.hpcshelf.mpi.MPILauncher;
using br.ufc.pargo.hpe.kinds;

namespace br.ufc.mdcc.hpcshelf.mpi.impl.MPILauncherForCImpl
{
    public abstract class BaseIProcessLauncherForCImpl<L>: Computation, BaseIProcessLauncher<L>
		where L:ILanguage_C
	{
		private L language = default(L);

		protected L Language
		{
			get
			{
				if (this.language == null)
					this.language = (L) Services.getPort("language");
				return this.language;
			}
		}
	}
}