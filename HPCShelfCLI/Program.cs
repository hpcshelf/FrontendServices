﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;
using HPCShelfCLIBase;


namespace HPCShelfCLI
{


    public static class Program
    {
        public static void Main(string[] args)
        {
           ServiceBase.Run(new ServiceBase[] { new CLIService() });

         //  foreach (string arg in args)
         //       (new CLIServices0()).run_command(arg.Split(' '));

        }
    }






    public class CLIService : ServiceBase
    {
        private static ServiceHost m_svcHost;
        
        private static void StartHTTPService(int nPort)
        {
            string strAdr = "http://localhost:" + nPort.ToString() + "/CLIService";
            try
            {
                Uri adrbase = new Uri(strAdr);
                m_svcHost = new ServiceHost(typeof(CLIServices0), adrbase);

                m_svcHost.OpenTimeout = new TimeSpan(0, 10, 0);
                m_svcHost.CloseTimeout = new TimeSpan(0, 10, 0);

                ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
                m_svcHost.Description.Behaviors.Add(mBehave);
                m_svcHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

                BasicHttpBinding httpb = new BasicHttpBinding();
                m_svcHost.AddServiceEndpoint(typeof(ICLIServices), httpb, strAdr);
                m_svcHost.Open();
                Console.WriteLine("\n\nService is Running as >> " + strAdr);
            }
            catch (Exception eX)
            {
                m_svcHost = null;
                Console.WriteLine("Service can not be started as >> [" + strAdr + "] \n\nError Message [" + eX.Message + "]");
            }
        }


        public CLIService()
        {
            Console.WriteLine("SERVICE");
        }

        protected override void OnStart(string[] args)
        {
            Console.WriteLine("START");
            StartHTTPService(8074);
        }


        protected override void OnStop()
        {
            Console.WriteLine("STOP");
        }




        /*      private const string usage = @"Naval Fate.

            Usage:
              naval_fate.exe ship new <name>...
              naval_fate.exe ship <name> move <x> <y> [--speed=<kn>]
              naval_fate.exe ship shoot <x> <y>
              naval_fate.exe mine (set|remove) <x> <y> [--moored | --drifting]
              naval_fate.exe (-h | --help)
              naval_fate.exe --version

            Options:
              -h --help     Show this screen.
              --version     Show version.
              --speed=<kn>  Speed in knots [default: 10].
              --moored      Moored (anchored) mine.
              --drifting    Drifting mine.

            ";
        */
    }
}