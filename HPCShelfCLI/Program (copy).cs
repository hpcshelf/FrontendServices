﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using DocoptNet;
using HPCShelfDriver;

namespace HPCShelfCLI
{
	internal class Program
	{
		private const string usage = @"HPC Shelf.

    Usage:
      hpc_shelf.exe exit
      hpc_shelf.exe new system <name> [--signature=<contract>] [--force]
      hpc_shelf.exe new computation <name> --contract=<contract> --platform=<platform>
      hpc_shelf.exe new connector <name> --contract=<contract> --platform=<platform>...
      hpc_shelf.exe new datasource <name> --contract=<contract> --platform=<platform>
      hpc_shelf.exe new platform <name> --contract=<contract>
      hpc_shelf.exe new action_binding <name> <port_type> --port=<port>...
      hpc_shelf.exe new service_binding <name> --contract=<contract> --user_port=<user_port> --provider_port=<provider_port>
      hpc_shelf.exe add parameter <name> [--variable=<variable>] --bound=<contract> 
      hpc_shelf.exe set argument <name> --contract=<contract>
      hpc_shelf.exe list system
      hpc_shelf.exe list component
      hpc_shelf.exe list computation
      hpc_shelf.exe list connector
      hpc_shelf.exe list datasource
      hpc_shelf.exe list platform
      hpc_shelf.exe list binding
      hpc_shelf.exe list action_binding
      hpc_shelf.exe list service_binding
      hpc_shelf.exe list parameter
      hpc_shelf.exe list argument
      hpc_shelf.exe list service port <name>
      hpc_shelf.exe list action port <name>
      hpc_shelf.exe contract <name>
      hpc_shelf.exe bound <name>
      hpc_shelf.exe argument <name>
      hpc_shelf.exe save
      hpc_shelf.exe load <name>
      hpc_shelf.exe clear
      hpc_shelf.exe open system <name>
      hpc_shelf.exe debug on
      hpc_shelf.exe debug off
      hpc_shelf.exe show log

    Options:
      -h --help     Show this screen.
      --version     Show version.
      --speed=<kn>  Speed in knots [default: 10].
      --moored      Moored (anchored) mine.
      --drifting    Drifting mine.

    ";

        public static string NewVariableName { get { return "VAR@" + (new object()).GetHashCode(); } }

		private static IDictionary<string, ISystem> system = new Dictionary<string, ISystem>();
		private static ISystem current_system = null;

		private static void Main(string[] args)
		{
     //       bool x = false;
       //     executeLine(args[0], out _, out _, ref x);

            IDictionary<string, ValueObject> arguments = null;

            bool exit = false;
            bool debug = false;

            do
            {
                Console.Write("HPC Shelf> ");
                string line = Console.ReadLine();

                try
                {
                    bool log;
                    exit = executeLine(line, out arguments, out log, ref debug);
                    if (log)
                        saveLog(line);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    if (debug)
                    {
                        Console.WriteLine(e.StackTrace);
                        foreach (KeyValuePair<string, ValueObject> argument in arguments)
                            Console.WriteLine("{0} = {1}", argument.Key, argument.Value);
                    }
                }
            }
            while (!exit);

		}

        private static bool executeLine(string line, out IDictionary<string, ValueObject> arguments, out bool log, ref bool debug)
        {
            log = true;
            string[] args_ = line.Trim().Split(' ');

			arguments = new Docopt().Apply(usage, args_, version: "HPC Shelf CLI", exit: false);

            string name;
            IDictionary<string, object> contract;

			if (arguments["debug"].IsTrue && arguments["on"].IsTrue)
			{
				debug = true;
                log = false;
                return false;
			}
            else if (arguments["debug"].IsTrue && arguments["off"].IsTrue)
            {
                debug = false;
                log = false;
                return false;
            }
            else if (arguments["exit"].IsTrue)
			{
				log = false;
                return true;
			}

			if (arguments["system"].IsFalse && arguments["load"].IsFalse && current_system == null)
				throw new Exception("No system ! Execute \"new system\" or \"open system\"");
            else 
            {
				name = arguments["<name>"] == null ? null : (string)arguments["<name>"].Value;
                contract = arguments["--contract"] != null ? buildContract((string)arguments["--contract"].Value) : arguments["--signature"] != null ? buildContract((string)arguments["--signature"].Value) : arguments["--bound"] != null ? buildContract((string)arguments["--bound"].Value) : null;
			}

			if (arguments["show"].IsTrue && arguments["log"].IsTrue)
			{
				log = false;
				showLog();
				return false;
			}
			
            if (arguments["new"].IsTrue)
			{
				if (arguments["system"].IsTrue)
				{
                    if (arguments["--force"].IsFalse && existsLog(name))
                    {
                        throw new Exception("A log for a system named " + name + " already exists. Use --force to delete it.");
                    }
                    else if (arguments["--force"].IsTrue && existsLog(name))
                        deleteLog(name);

                    // Forcing cast ...
                    IDictionary<string, Tuple<string, object>> signature = new Dictionary<string, Tuple<string, object>>();
                    foreach (KeyValuePair<string,object> parameter in contract)
                        signature[parameter.Key] = (Tuple<string, object>)parameter.Value;

					system.Add(name, SystemBuilder.build(name, signature));
					current_system = system[name];
				}
				else if (arguments["computation"].IsTrue)
				{
					string virtual_platform_name = (string)((ValueObject)((System.Collections.ArrayList)arguments["--platform"].Value)[0]).Value;
					IVirtualPlatform virtual_platform = current_system.VirtualPlatform[virtual_platform_name];
					current_system.newComputation(name, contract, virtual_platform);
				}
				else if (arguments["connector"].IsTrue)
				{
					// new connector "connector_name" --platform=vp0:0 --platform=vp1:1
					System.Collections.ArrayList virtual_platform_name = (System.Collections.ArrayList)arguments["--platform"].Value;
                    Tuple<int, IVirtualPlatform>[] virtual_platform = new Tuple<int, IVirtualPlatform>[virtual_platform_name.Count];
					for (int i = 0; i < virtual_platform.Length; i++)
					{
                        string virtual_platform_name_string = (string)((ValueObject)virtual_platform_name[i]).Value;
						string[] virtual_platform_name_item = virtual_platform_name_string.Split(':');
						int facet = int.Parse(virtual_platform_name_item[1]);
						virtual_platform[i] = new Tuple<int, IVirtualPlatform>(facet, current_system.VirtualPlatform[virtual_platform_name_item[0]]);
					}
					current_system.newConnector(name, contract, virtual_platform);
				}
				else if (arguments["datasource"].IsTrue)
				{
                    string virtual_platform_name = (string)((ValueObject)((System.Collections.ArrayList)arguments["--platform"].Value)[0]).Value;
					IVirtualPlatform virtual_platform = current_system.VirtualPlatform[virtual_platform_name];
					current_system.newDataSource(name, contract, virtual_platform);
				}
				else if (arguments["platform"].IsTrue)
				{
					current_system.newVirtualPlatform(name, contract);
				}
				else if (arguments["action_binding"].IsTrue)
				{
					string port_type = (string)arguments["port_type"].Value;
					string[] port_name_list = (string[])arguments["--port"].Value;
					IActionPort[] action_port_list = new IActionPort[port_name_list.Length];
					for (int i = 0; i < action_port_list.Length; i++)
					{
						IActionPort action_port;
						if (port_name_list.Equals("application"))
							action_port = current_system.Workflow.NewActionPort;
						else
						{
							string[] port_ref_ = port_name_list[i].Split(':');
							int index = int.Parse(port_ref_[1]);
							string[] port_ref = port_ref_[0].Split('.');
							string component_name = port_ref[0];
							string port_name = port_ref[1];
							action_port = ((IActionComponent)current_system.Component[component_name]).ActionPort[port_name][index];
						}
						action_port_list[i] = action_port;
					}

					current_system.newActionBinding(name, port_type, action_port_list);
				}
				else if (arguments["service_binding"].IsTrue)
				{
					string port_ref_user__ = (string)arguments["--user_port"].Value;

					IServicePort user_port;
					if (port_ref_user__.Equals("application"))
						user_port = current_system.Application.NewServicePort;
					else
					{
						string[] port_ref_user_ = (port_ref_user__).Split(':');
						int index_user = int.Parse(port_ref_user_[1]);
						string[] port_ref_user = port_ref_user_[0].Split('.');
						string component_name_user = port_ref_user[0];
						string port_name_user = port_ref_user[1];
						user_port = ((IServiceSolutionComponent)current_system.Component[component_name_user]).UserPort[port_name_user][index_user];
					}

					IServicePort provider_port;
					if (port_ref_user__.Equals("application"))
						provider_port = current_system.Application.NewServicePort;
					else
					{
						string[] port_ref_provider_ = ((string)arguments["--provider_port"].Value).Split(':');
						int index_provider = int.Parse(port_ref_provider_[1]);
						string[] port_ref_provider = port_ref_provider_[0].Split('.');
						string component_name_provider = port_ref_provider[0];
						string port_name_provider = port_ref_provider[1];
						provider_port = ((IServiceSolutionComponent)current_system.Component[component_name_provider]).UserPort[port_name_provider][index_provider];
					}
					current_system.newServiceBinding(name, contract, (IUserServicePort)user_port, (IProviderServicePort)provider_port);
				}
			}
			else if (arguments["add"].IsTrue && arguments["parameter"].IsTrue)
			{
				string variable = arguments["--variable"] == null ? NewVariableName : (string)arguments["--variable"].Value;
				current_system.addParameter(name, variable, contract);
			}
			else if (arguments["set"].IsTrue && arguments["argument"].IsTrue)
			{
				current_system.setArgument(name, contract);
			}
			else if (arguments["list"].IsTrue)
			{
                log = false;
                if (arguments["system"].IsTrue)
                {
                    Console.WriteLine("listing {0} systems ...", system.Count);
                    foreach (string id in system.Keys)
                    {
                        Console.Write(id);
                        if (id.Equals(current_system.Name))
                            Console.WriteLine(" (current)");
                        else
                            Console.WriteLine();
                    }
                }
                else if (arguments["component"].IsTrue)
                {
                    Console.WriteLine("listing {0} components ...", current_system.Component.Count);
                    foreach (string id in current_system.Component.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["computation"].IsTrue)
                {
                    Console.WriteLine("listing {0} computations ...", current_system.Computation.Count);
                    foreach (string id in current_system.Computation.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["connector"].IsTrue)
                {
                    Console.WriteLine("listing {0} connectors ...", current_system.Connector.Count);
                    foreach (string id in current_system.Connector.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["datasource"].IsTrue)
                {
                    Console.WriteLine("listing {0} data sources ...", current_system.DataSource.Count);
                    foreach (string id in current_system.DataSource.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["platform"].IsTrue)
                {
                    Console.WriteLine("listing {0} virtual platforms ...", current_system.VirtualPlatform.Count);
                    foreach (string id in current_system.VirtualPlatform.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["binding"].IsTrue)
                {
                    Console.WriteLine("listing {0} bindings ...", current_system.Binding.Count);
                    foreach (string id in current_system.Binding.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["action_binding"].IsTrue)
                {
                    Console.WriteLine("listing {0} action bindings ...", current_system.ActionBinding.Count);
                    foreach (string id in current_system.ActionBinding.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["service_binding"].IsTrue)
                {
                    Console.WriteLine("listing {0} service bindings ...", current_system.ServiceBinding.Count);
                    foreach (string id in current_system.ServiceBinding.Keys)
                        Console.WriteLine(id);
                }
                else if (arguments["parameter"].IsTrue)
                {
                    Console.WriteLine("listing {0} parameters ...", current_system.ContextualSignature.Count);
                    foreach (KeyValuePair<string, Tuple<string, object>> parameter in current_system.ContextualSignature)
                        Console.WriteLine("{0}:{1}={2}", parameter.Key, parameter.Value.Item1, parameter.Value.Item2);
                }
                else if (arguments["argument"].IsTrue)
                {
                    Console.WriteLine("listing {0} arguments ...", current_system.Contract.Count);
                    foreach (KeyValuePair<string, object> parameter in current_system.Contract)
                        Console.WriteLine("{0}={1}", parameter.Key, parameter.Value);
                }
                else if (arguments["port"].IsTrue)
                {
                    IComponent c_;
                    if (current_system.Component.TryGetValue(name, out c_))
                    {
                        if (arguments["service"].IsTrue)
                        {
                            if (c_ is IServiceSolutionComponent)
                            {

                                IServiceSolutionComponent c = (IServiceSolutionComponent)c_;
                                Console.WriteLine("listing {0} service ports ...", c.ServicePort.Count);
                                foreach (KeyValuePair<string, IServicePort[]> port in c.ServicePort)
                                {
                                    string port_name = port.Key;
                                    for (int i = 0; i < port.Value.Length; i++)
                                        Console.WriteLine("{0}:{1}", port_name, i);
                                }
                            }
                            else
                                throw new Exception("The component " + name + " does not support service ports (" + c_.GetType() + ").");

                        }
                        else if (arguments["action"].IsTrue)
                        {
                            if (c_ is IActionComponent)
                            {
                                IActionComponent c = (IActionComponent)c_;
                                Console.WriteLine("listing {0} action ports ...", c.ActionPort.Count);
                                foreach (KeyValuePair<string, IActionPort[]> port in c.ActionPort)
                                {
                                    string port_name = port.Key;
                                    for (int i = 0; i < port.Value.Length; i++)
                                        Console.WriteLine("{0}:{1}", port_name, i);
                                }
                            }
                            else
                                throw new Exception("The component " + name + " does not support have action ports (" + c_.GetType() + ").");
                        }
                    }
                    else
                        throw new Exception("There is no component named " + name + ".");
                }
			}
			else if (arguments["contract"].IsTrue)
			{
                log = false;
				IComponent c;
				if (current_system.Component.TryGetValue(name, out c))
					showContract(c.Contract);
                else
                    throw new Exception("There is no component named " + name + ".");
			}
			else if (arguments["bound"].IsTrue)
			{
                log = false;
                Console.WriteLine();
                object bound = current_system.ContextualSignature[name].Item2;
                if (bound is IDictionary<string, object>)
                    showContract((System.Collections.Generic.IDictionary<string, object>)bound);
                else
                    Console.WriteLine(bound);
			}
			else if (arguments["argument"].IsTrue)
			{
                log = false;
				IDictionary<string, object> argument_contract = (IDictionary<string, object>)current_system.Contract[name];
				showContract(argument_contract);
			}
			else if (arguments["save"].IsTrue)
			{
				log = false;
				throw new NotImplementedException();
			}
			else if (arguments["load"].IsTrue)
			{
				log = false;
				runLog((string)arguments["<name>"].Value);
			}
			else if (arguments["clear"].IsTrue)
			{
				log = false;
				current_system = null;
				system.Remove(current_system.Name);
				//removeLog();
			}
			else if (arguments["open"].IsTrue && arguments["system"].IsTrue)
			{
                log = false;

                string system_name = (string)arguments["<name>"].Value;
                if (system.ContainsKey(system_name))
                {
                    Console.Write("Changing system from {0} to ", current_system.Name);
                    current_system = system[system_name];
                    Console.WriteLine(current_system.Name);
                }
                else
                    Console.WriteLine("There is no system named {0}", system_name);
			}
            else 
            {
                throw new Exception("Unrecognized command !"); 
            }

            return false;
		}

        private static void deleteLog(string name)
        {
			string file_name = name + ".log";
            File.Delete(file_name);
		}

        private static bool existsLog(string name)
        {
			string file_name = name + ".log";
            return File.Exists(file_name);
		}

        private static void runLog(string system_name)
        {
            string file_name = system_name + ".log";
            bool debug = false;
            string[] commands = File.ReadAllLines(file_name);
            File.Delete(file_name);
            try
            {
                foreach (string line in commands)
                    executeLine(line, out _, out _, ref debug);
            }
            finally
            {
                File.WriteAllLines(file_name, commands);
            }
        }

        private static void removeLog()
        {
			string name = current_system.Name;
            File.Delete(name + ".log");
		}

        private static void saveLog(string line)
        {
            string name = current_system.Name;
            string file_name = name + ".log";
            if (File.Exists(file_name))
                File.AppendAllText(file_name, "\n" + line);
            else
                File.AppendAllText(file_name, line);
        }

		private static void showLog()
		{
			string name = current_system.Name;
            Console.WriteLine(File.ReadAllText(name + ".log"));
		}

		private static void showContract(IDictionary<string, object> contract)
		{
            showContract("", contract);
        }

        private static void showSignature(IDictionary<string, Tuple<string, object>> bound)
        {
            foreach (KeyValuePair<string, Tuple<string, object>> argument in bound)
            {
                if (argument.Value.Item2 is decimal)
                    Console.WriteLine("{0}@{2}={1} ", argument.Key, ((decimal)argument.Value.Item2).ToString(CultureInfo.InvariantCulture), argument.Value.Item1);
                else if (argument.Value.Item2 is IDictionary<string, object>)
                    showContract(argument.Key + "(" + argument.Value.Item1 + ")", (IDictionary<string, object>)argument.Value.Item2);
            }
        }

        private static void showContract(string prefix, IDictionary<string, object> contract)
        {
            //Console.WriteLine("{0}{1}={2}", prefix, "name", contract["name"]);

            foreach (KeyValuePair<string, object> argument in contract)
            {
                if (argument.Value is string)
                    Console.WriteLine("{0}{1}={2}", prefix, argument.Key, argument.Value);
                else if (argument.Value is decimal)
                    Console.WriteLine("{0}{1}={2}", prefix, argument.Key, ((decimal)argument.Value).ToString(CultureInfo.InvariantCulture));
				else if (argument.Value is int)
					Console.WriteLine("{0}{1}={2}", prefix, argument.Key, ((int)argument.Value).ToString(CultureInfo.InvariantCulture));
				else if (argument.Value is IDictionary<string,object>)
					showContract(prefix + argument.Key + ".", (IDictionary<string, object>)argument.Value);
            }
		}

        private static IDictionary<string, object> buildContract(string contract)
        {
            IDictionary<string, object> contract_dict = new Dictionary<string, object>();

            string contract_contents = File.Exists(contract) ? File.ReadAllText(contract) : contract;

            string[] contract_parameter_list = contract_contents.Split(new char[3] { '\n', ',', ';' });

            Queue<string> q_parameters = new Queue<string>();
            foreach (string contract_parameter in contract_parameter_list)
                q_parameters.Enqueue(contract_parameter);

            int count = 0;
            while (q_parameters.Count > 0)
            {
                string contract_parameter = q_parameters.Dequeue();

                string[] contract_parameter_parts = contract_parameter.Split('=');
                string[] parameter_id_var = contract_parameter_parts[0].Split(':');

                IDictionary<string, object> contract_par = contract_dict;
                string[] parameter_id_qual = parameter_id_var[0].Split('-');
                bool enqueued = false;
                while (parameter_id_qual.Length > 1)
                {
                    object contract_par_obj;
                    if (!contract_par.TryGetValue(parameter_id_qual[0], out contract_par_obj))
                    {
                        q_parameters.Enqueue(contract_parameter);
                        enqueued = true;
                        break;
                    }
                    contract_par = (IDictionary<string, object>)contract_par_obj;

                    string[] parameter_id_qual_ = new string[parameter_id_qual.Length - 1];
                    for (int i = 0; i < parameter_id_qual_.Length; i++)
                        parameter_id_qual_[i] = parameter_id_qual[i + 1];
                    parameter_id_qual = parameter_id_qual_;
                }

                if (enqueued)
                {
                    count++;
					if (count > 0 && count == q_parameters.Count)
                        throw new Exception("Invalid parameter in contract: " + contract_parameter);
					else
                        continue;
                }
                else
                    count = 0;


                string parameter_id = parameter_id_qual[0];
                string var_id = parameter_id_var.Length > 1 ? parameter_id_var[1] : null;

                if (parameter_id.Equals("name") && var_id != null)
                    throw new Exception("Invalid parameter in contract: " + contract_parameter);

                string argument = contract_parameter_parts[1];
                int int_value = default(int);
                decimal dec_value = default(decimal);
                if (int.TryParse(argument, out int_value))
                    contract_par[parameter_id] = var_id == null ? (object)int_value : (object)new Tuple<string, object>(var_id, int_value);
                else if (decimal.TryParse(argument, out dec_value))
                    contract_par[parameter_id] = var_id == null ? (object)dec_value : (object)new Tuple<string, object>(var_id, dec_value);
                else if (parameter_id.Equals("name"))
                    contract_par[parameter_id] = argument;
                else if (argument.StartsWith("@"))
                    contract_par[parameter_id] = argument.Substring(1);
				else
					contract_par[parameter_id] = var_id == null ? new Dictionary<string, object>() { { "name", argument } } : (object)new Tuple<string, object>(var_id, new Dictionary<string, object>() { { "name", argument } });
			}

            return contract_dict;
        }


		/*      private const string usage = @"Naval Fate.

			Usage:
			  naval_fate.exe ship new <name>...
			  naval_fate.exe ship <name> move <x> <y> [--speed=<kn>]
			  naval_fate.exe ship shoot <x> <y>
			  naval_fate.exe mine (set|remove) <x> <y> [--moored | --drifting]
			  naval_fate.exe (-h | --help)
			  naval_fate.exe --version

			Options:
			  -h --help     Show this screen.
			  --version     Show version.
			  --speed=<kn>  Speed in knots [default: 10].
			  --moored      Moored (anchored) mine.
			  --drifting    Drifting mine.

			";
		*/
	}
}