﻿using System;

public interface ICoreLauncher
{
    string CoreAddress { get; }
    string launch(string region_id);
    void stop();
    string start();
    void terminate();
}
