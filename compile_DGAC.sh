#!/bin/sh

MAKE=make_dgac.rsp
OLD="MONO_HOME"
NEW=$MONO_HOME

cp $MAKE temp.rsp
find ./ -iname temp.rsp | xargs sed -i --expression "s@$OLD@$NEW@"

if [ ! -d ./bin ]
then
        mkdir bin
fi

if [ ! -f DGAC/DGAC.snk ]
then
    sn -k DGAC/DGAC.snk
fi

mcs @temp.rsp
rm temp.rsp

./install_DGAC.sh
