﻿using System;
using System.ServiceModel;
using System.Web;
using System.Web.Services;
using org.hpcshelf.backend.platform;

namespace PlatformSAFeWebServices
{
    public class PlatformSAFe : WebService, IPlatformServices
    {
        static int port = 8075;

        IPlatformServices platformSAFe_service = null;
        IPlatformServices PlatformSAFeService { get { if (platformSAFe_service == null) platformSAFe_service = Get("localhost", port); return platformSAFe_service; } }

        [WebMethod]
        public string allocateBindingAddress(string channel_id, int n)
        {
            return PlatformSAFeService.allocateBindingAddress(channel_id, n);
        }

        [WebMethod]
        public string deploy(string module_data, byte[] key_file_contents)
        {
            return PlatformSAFeService.deploy(module_data, key_file_contents);
        }

        [WebMethod]
        public int getBaseBindingPort()
        {
            return PlatformSAFeService.getBaseBindingPort();
        }

        [WebMethod]
        public int getNumberOfNodes()
        {
            return PlatformSAFeService.getNumberOfNodes();
        }

        [WebMethod]
        public string getPlatformRef()
        {
            return PlatformSAFeService.getPlatformRef();
        }

        [WebMethod]
        public string getStatus()
        {
            return PlatformSAFeService.getStatus();
        }

        [WebMethod]
        public string instantiate(string component_ref, string[] ctree_str_list, int facet_instance, string[] channel_id, int[] channel_facet_instance, string[] channel_facet_address)
        {
            return PlatformSAFeService.instantiate(component_ref, ctree_str_list, facet_instance, channel_id, channel_facet_instance, channel_facet_address);
        }

        [WebMethod]
        public void run(string component_ref)
        {
            PlatformSAFeService.run(component_ref);
        }

        [WebMethod]
        public void release(string component_ref)
        {
            PlatformSAFeService.release(component_ref);
        }

        [WebMethod]
        public void setPlatformRef(string arch_ref)
        {
            PlatformSAFeService.setPlatformRef(arch_ref);
        }

        private static IPlatformServices Get(string strServer, int nPort)
        {
            IPlatformServices mathSvcObj = null;

            ChannelFactory<IPlatformServices> channelFactory = null;
            EndpointAddress ep = null;

            BasicHttpBinding httpb = new BasicHttpBinding();
            channelFactory = new ChannelFactory<IPlatformServices>(httpb);

            // End Point Address
            string strEPAdr = "http://" + strServer + ":" + nPort.ToString() + "/PlatformSAFe";
            try
            {
                // Create End Point
                ep = new EndpointAddress(strEPAdr);

                // Create Channel
                mathSvcObj = channelFactory.CreateChannel(ep);

                //channelFactory.Close();
            }
            catch (Exception eX)
            {
                // Something unexpected happended .. 
                Console.WriteLine("Error while performing operation [" +
                  eX.Message + "] \n\n Inner Exception [" + eX.InnerException + "]");
            }

            return mathSvcObj;
        }
    }
}
