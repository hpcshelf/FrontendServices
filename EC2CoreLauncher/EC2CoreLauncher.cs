﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using org.hpcshelf.backend;
using org.hpcshelf.backend.ec2;
using org.hpcshelf.command;
using org.hpcshelf.core;
using org.hpcshelf.DGAC.utils;

public class EC2CoreLauncher : ICoreLauncher
{
    public string launch(string region_id)
    {
        // launch backend service.
        launchEC2Instance(region_id);

        // configure environment
        launchCoreServices();

        return core_address;
    }

    private void launchCoreServices()
    {
        // EC2_BACKEND_IMAGE_ID and run_ec2-backend_service_local.sh are placed at the platform.
        // 1. copy the user credentials ("scp -i $1 ~/.aws/config  ubuntu@$2:.aws/config", "scp -i $1 ~/.aws/credentials  ubuntu@$2:.aws/credentials").
        // 2. launch the service ("nohup rsh -i ~/hpc-shelf-credential.pem ubuntu@$1 "bash ~/run_ec2-backend_service.sh" &").


        string script_command = Path.Combine(Constants.SWIRLS_HOME, "run_core_services_remote.sh");

        Utils.commandExecBash(string.Format("{0} {1} {2}", script_command, credential_file_name, core_address));
    }

    public void stop()
    {
        // launch backend service.
        stopCoreServices();
    }

    public string start()
    {
        // launch core services.
        startCoreServices();

        return core_address;
    }

    private string startCoreServices()
    {
        InstancesManager.StartInstance(instanceId, client);

        string status_check;
        do
        {
            status_check = InstancesManager.Instance_statusCheck(region, instanceId);
            Console.WriteLine("Waiting to start {0} -- {1}", instanceId, status_check);
            Thread.Sleep(2000);

        }
        while (!status_check.Equals("ok"));

        IList<IVirtualMachine> VirtualMachines = new List<IVirtualMachine>();
        DataRegistry(client, instanceId, VirtualMachines);
        core_address = VirtualMachines.ElementAt(0).PublicIpAddress;

        //return (core_address = string.Format("http://{0}:8080/CoreWebServices.asmx", platform_address));

        return core_address;
    }

    private void stopCoreServices()
    {
        InstancesManager.StopInstance(instanceId, client);

        /*  string status_check;
          do
          {
              status_check = InstancesManager.Instance_statusCheck(region, instanceId);
              Console.WriteLine("Waiting to stop {0} -- {1}", instanceId, status_check);
              Thread.Sleep(2000);

          }
          while (!status_check.Equals("ok"));
          */
        core_address = "<stopped>";
    }

    public void terminate()
    {
        InstancesManager.TerminateInstance(instanceId, client);
    }


    private string core_address = "<pending>";
    private InstanceType instanceId = null;
    private AmazonEC2Client client = null;
    private RegionEndpoint region = null;
    private string credential_file_name = null;

    public string CoreAddress { get { return core_address; } }

    private string EC2_BACKEND_IMAGE_ID { get { return File.ReadAllLines(Path.Combine(Constants.SWIRLS_HOME, "EC2_BACKEND_IMAGE_ID"))[0]; } }

    private string launchEC2Instance(string region_id)
    {
        string amID = EC2_BACKEND_IMAGE_ID;

        region = RegionEndpoint.GetBySystemName(region_id);

        client = new AmazonEC2Client(region);

        string keyPairName = "hpc-shelf-credential";
        credential_file_name = Path.Combine(Constants.SWIRLS_HOME, string.Format("{0}.pem", keyPairName));
        InstancesManager.CreateKeyPair(client, keyPairName, credential_file_name);

        string instance_type = InstanceType.T2Micro.ToString();

        string securityGroupName = "hpc-shelf";

        instanceId = InstancesManager.LaunchInstance(instance_type, amID, keyPairName, securityGroupName, client);

        string status_check;
        do
        {
            status_check = InstancesManager.Instance_statusCheck(region, instanceId);
            Console.WriteLine("Waiting to launch {0} -- {1}", instanceId, status_check);
            Thread.Sleep(2000);

        }
        while (!status_check.Equals("ok"));

        IList<IVirtualMachine> VirtualMachines = new List<IVirtualMachine>();
        DataRegistry(client, instanceId, VirtualMachines);
        core_address = VirtualMachines.ElementAt(0).PublicIpAddress;

        return core_address;
    }

    protected void DataRegistry(AmazonEC2Client client, string instanceId, IList<IVirtualMachine> VirtualMachines)
    {
        //lock (lock_1)
        {
            DescribeInstancesRequest req = new DescribeInstancesRequest()
            {
                Filters = new List<Filter>()
                {
                    new Filter()
                    {
                        Name = "instance-id",
                        Values = new List<String>() { instanceId }
                    }
                }
            };
            IVirtualMachine vm = null;
            while (vm == null || vm.PublicIpAddress == null)
            {
                List<Reservation> result = client.DescribeInstances(req).Reservations;

                foreach (Amazon.EC2.Model.Reservation reservation in result)
                {
                    foreach (Instance runningInstance in reservation.Instances)
                    {
                        vm = new VirtualMachine
                        {
                            InstanceId = runningInstance.InstanceId,
                            PrivateIpAddress = runningInstance.PrivateIpAddress,
                            PublicIpAddress = runningInstance.PublicIpAddress
                        };
                    }
                }
                if (vm == null || vm.PublicIpAddress == null)
                    Thread.Sleep(1000); //Wait 1s for aws release public IP address
            }
            VirtualMachines.Add(vm);
        }
    }


}
