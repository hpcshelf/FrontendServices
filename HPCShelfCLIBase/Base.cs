﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.ServiceModel;
using System.Threading;
using System.Web.Services.Protocols;
using System.Xml;
using org.hpcshelf.common.browser.RecvDataPortType;
using org.hpcshelf.common.BrowserBinding;
using org.hpcshelf.core;
using org.hpcshelf.kinds;
using DocoptNet;
using HPCShelfDriver;
using static SimpleKernel;
using BackendLauncher;

namespace HPCShelfCLIBase
{
    [ServiceContract]
    interface ICLIServices
    {
        [OperationContract]
        string run_command(string[] args, Session session,
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content,
                                          ExecutionCounter execution_count,
                                          object lock_message);

        [OperationContract]
        string run_command(string[] args, AutoResetEvent async, out string async_flag, Session session, 
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content,
                                          ExecutionCounter execution_count,
                                          object lock_message);

        [OperationContract]
        void shutdownSystems();
    }

    public class CLIServices0 : ICLIServices
    {
        private const string usage = @"HPC Shelf.

    Usage:
      ./hpc_shelf.exe create contract <name> <contract>
      ./hpc_shelf.exe new system <name> [--signature=<contract>] [--clear_log]
      ./hpc_shelf.exe new computation <name> --contract=<contract> --platform=<platform>
      ./hpc_shelf.exe new connector <name> --contract=<contract> --platform=<platform>...
      ./hpc_shelf.exe new datasource <name> --contract=<contract> --platform=<platform>
      ./hpc_shelf.exe new platform <name> --contract=<contract>
      ./hpc_shelf.exe new action-binding <name> --port-type=<port_type> --port=<port>...
      ./hpc_shelf.exe new service-binding <name> --contract=<contract> --user-port=<user_port> --provider-port=<provider_port>
      ./hpc_shelf.exe add parameter <name> [--variable=<variable>] --bound=<contract> 
      ./hpc_shelf.exe apply argument <name> --contract=<contract>
      ./hpc_shelf.exe set workflow <file_name>
      ./hpc_shelf.exe terminate system
      ./hpc_shelf.exe browse <name> 
      ./hpc_shelf.exe resolve [--async=<async_handle>] [--system] [--handle=<handle>] [--sequential] <name_list>... 
      ./hpc_shelf.exe certify [--async=<async_handle>] [--handle=<handle>] [--sequential] <name_list>...
      ./hpc_shelf.exe deploy [--async=<async_handle>] [--handle=<handle>] [--sequential] <name_list>...
      ./hpc_shelf.exe instantiate [--async=<async_handle>] [--handle=<handle>] [--sequential] <name_list>...
      ./hpc_shelf.exe run [--async=<async_handle>] [--handle=<handle>] [--sequential] <name_list>...
      ./hpc_shelf.exe release [--async=<async_handle>] [--handle=<handle>] [--sequential] <name_list>...
      ./hpc_shelf.exe async_wait <name_list>...
      ./hpc_shelf.exe invoke [--handle=<handle>] <name> --action=<action_name>...
      ./hpc_shelf.exe wait <handle_name>
      ./hpc_shelf.exe wait all <handle_name>...
      ./hpc_shelf.exe wait any <handle_name>...
      ./hpc_shelf.exe test <handle_name>
      ./hpc_shelf.exe test all <handle_name>...
      ./hpc_shelf.exe test any <handle_name>...
      ./hpc_shelf.exe list system 
      ./hpc_shelf.exe list component [--platform=<platform>...] 
      ./hpc_shelf.exe list computation [--platform=<platform>...] 
      ./hpc_shelf.exe list connector [--platform=<platform>...] 
      ./hpc_shelf.exe list datasource [--platform=<platform>...] 
      ./hpc_shelf.exe list platform [--platform=<platform>...]
      ./hpc_shelf.exe list binding [--platform=<platform>...] 
      ./hpc_shelf.exe list action-binding [--platform=<platform>...] 
      ./hpc_shelf.exe list service-binding [--platform=<platform>...] 
      ./hpc_shelf.exe list parameter
      ./hpc_shelf.exe list argument
      ./hpc_shelf.exe list service-port <name> 
      ./hpc_shelf.exe list action-port <name>
      ./hpc_shelf.exe list handle
      ./hpc_shelf.exe contract <name>
      ./hpc_shelf.exe bound <name>
      ./hpc_shelf.exe argument <name>
      ./hpc_shelf.exe save
      ./hpc_shelf.exe load <name>
      ./hpc_shelf.exe open system <name>
      ./hpc_shelf.exe debug on
      ./hpc_shelf.exe debug off
      ./hpc_shelf.exe show log
      ./hpc_shelf.exe create computation <name>  [--message-passing=<mp_library>] [--language=<language>] [--wrapper-source=<file_name>] --source=<path>  [--port=<contract>...] 
      ./hpc_shelf.exe backend local launch 
      ./hpc_shelf.exe backend list 
      ./hpc_shelf.exe backend ec2 local [--async=<async_handle>]
      ./hpc_shelf.exe backend ec2 remote [--async=<async_handle>] [--region=<region_id>] <name> 
      ./hpc_shelf.exe backend ec2 start [--async=<async_handle>] <name> 
      ./hpc_shelf.exe backend ec2 stop [--async=<async_handle>] <name> 
      ./hpc_shelf.exe backend ec2 terminate [--async=<async_handle>] <name> 
      ./hpc_shelf.exe backend gcp local [--async=<async_handle>] 
      ./hpc_shelf.exe backend gcp remote [--async=<async_handle>] [--zone=<zone_id>] <name>
      ./hpc_shelf.exe backend gcp start [--async=<async_handle>] <name> 
      ./hpc_shelf.exe backend gcp stop [--async=<async_handle>] <name> 
      ./hpc_shelf.exe backend gcp terminate [--async=<async_handle>] <name> 
      ./hpc_shelf.exe core set ip <ip> 
      ./hpc_shelf.exe core set port <port> 
      ./hpc_shelf.exe core get ip 
      ./hpc_shelf.exe core get port 
      ./hpc_shelf.exe core launch [--how=<provider>] [--async=<async_handle>] [--region=<region_id>]
      ./hpc_shelf.exe platform_SAFe set port <port> 

    Options:
      -h --help     Show this screen.
      --version     Show version.
      --speed=<kn>  Speed in knots [default: 10].
      --moored      Moored (anchored) mine.
      --drifting    Drifting mine.

    ";

        public static string NewVariableName { get { return "VAR@" + (new object()).GetHashCode(); } }

        private static IDictionary<string, ISystem> system = new Dictionary<string, ISystem>();
        private static ISystem current_system = null;


        public void shutdownSystems()
        {
            foreach (KeyValuePair<string,ISystem> one_system in system)
            {
                Console.WriteLine("Terminating system {0}", one_system.Key);
                one_system.Value.terminate();
            }

            system.Clear();
        }

        private static ServiceHost m_svcHost;

        private static bool debug = false;

        delegate void LifeCycleAction(IActionPortLifeCycle lifecycle_port);

        public string run_command(string[] args,
                                  Session session,
                                  IDictionary<string, object> m_header,
                                  IDictionary<string, object> m_parent_header,
                                  IDictionary<string, object> m_metadata,
                                  IDictionary<string, object> m_content,
                                  ExecutionCounter execution_count,
                                  object lock_message)
        {
            string async_flag;
            return run_command(args, null, out async_flag, session, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
        }

        public string run_command(string[] args,
                                  AutoResetEvent async,
                                  out string async_flag,
                                  Session session,
                                  IDictionary<string, object> m_header,
                                  IDictionary<string, object> m_parent_header,
                                  IDictionary<string, object> m_metadata,
                                  IDictionary<string, object> m_content,
                                  ExecutionCounter execution_count,
                                  object lock_message)
        {
            string return_message = null;
            async_flag = null;

            IDictionary<string, ValueObject> arguments = null;
            try
            {
                foreach (string arg in args)
                    Console.Write(arg + " ");
                Console.WriteLine(" #args = {0}", args.Length);

                bool log;
                return_message = executeLine(args, out arguments, out log, ref debug, async, out async_flag, session, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                if (log && current_system != null)
                    saveLog(args);
            }
            catch (SoapException e)
            {
                Console.WriteLine("--- SOAP EXCEPTION ---");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                sendMessage(handleServerException(e), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
            }
            catch (Exception e)
            {
                Console.WriteLine("--- EXCEPTION ---");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine("--- INNER EXCEPTION ---");
                    Console.WriteLine(e.InnerException.Message);
                    Console.WriteLine(e.InnerException.StackTrace);
                }

                sendMessage(e.Message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                if (debug)
                    sendMessage(e.StackTrace, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
            }
            finally
            {
                if (debug && arguments != null)
                {
                    string message = "";
                    foreach (KeyValuePair<string, ValueObject> argument in arguments)
                        message += "\n" + argument.Key + " = " + argument.Value;
                    sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                }
            }

            return return_message;
        }

        private string handleServerException(SoapException e)
        {
            XmlDocument doc = new XmlDocument();
            Console.WriteLine("******* {0} *******", e.Detail.OuterXml);
            doc.LoadXml(e.Detail.OuterXml);
            XmlNamespaceManager nsManager = new XmlNamespaceManager(doc.NameTable);
            // Add the namespace to the NamespaceManager
            nsManager.AddNamespace("errorNS", "http://tempuri.org/HPCShelfWebServices"); Console.WriteLine("namespace={0}", "http://tempuri.org/HPCShelfWebServices");
            XmlNode categoryNodeTest = doc.DocumentElement.SelectSingleNode("Error", nsManager); Console.WriteLine("categoryNode:{0}", categoryNodeTest == null);

            XmlNode categoryNode = doc.DocumentElement.SelectSingleNode("errorNS:Error", nsManager); Console.WriteLine("categoryNode:{0}", categoryNode == null);
            string errorNumber = categoryNode.SelectSingleNode("errorNS:ErrorNumber", nsManager).InnerText; Console.WriteLine("errorNumber:{0}", errorNumber == null);
            string errorMessage = categoryNode.SelectSingleNode("errorNS:ErrorMessage", nsManager).InnerText; Console.WriteLine("errorMessage:{0}", errorMessage == null);
            string errorSource = categoryNode.SelectSingleNode("errorNS:ErrorSource", nsManager).InnerText; Console.WriteLine("errorSource:{0}", errorSource == null);

            return string.Format(" --- SERVER EXCEPTION ---\n errorNumber: {0}\n errorMessage: {1}\n errorSource: {2}\n\n", errorNumber, errorMessage, errorSource);
        }

        private IDictionary<string, IActionFuture> invocation_future_list = new Dictionary<string, IActionFuture>();
        private IDictionary<IActionFuture, string> invocation_future_list_inv = new Dictionary<IActionFuture, string>();

        private string executeLine(string[] args_, out IDictionary<string, ValueObject> arguments, out bool log, ref bool debug,
                                  Session session,
                                  IDictionary<string, object> m_header,
                                  IDictionary<string, object> m_parent_header,
                                  IDictionary<string, object> m_metadata,
                                  IDictionary<string, object> m_content,
                                  ExecutionCounter execution_count,
                                  object lock_message)
        {
            string async_flag;
            return executeLine(args_, out arguments, out log, ref debug, null, out async_flag, session, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
        }

        private IDictionary<string, Thread> active_browser = new Dictionary<string, Thread>();

        private IDictionary<string, IDictionary<string, object>> contract_store = new Dictionary<string, IDictionary<string, object>>();

        private ICoreLauncher coreservices_launcher = null;
        private IDictionary<string, IBackendLauncher> backend_launcher_instances = new Dictionary<string, IBackendLauncher>();

        private string executeLine(string[] args_, out IDictionary<string, ValueObject> arguments, out bool log, ref bool debug, AutoResetEvent async, out string async_flag,
                                  Session session,
                                  IDictionary<string, object> m_header,
                                  IDictionary<string, object> m_parent_header,
                                  IDictionary<string, object> m_metadata,
                                  IDictionary<string, object> m_content,
                                  ExecutionCounter execution_count,
                                  object lock_message)
        {
            string return_message = null;

            async_flag = null;

            try
            {
                log = true;
                //string[] args_ = line.Trim().Split(' ');

                arguments = new Docopt().Apply(usage, args_, version: "HPC Shelf CLI", exit: false);

                string name = null;
                IDictionary<string, object> contract = null;

                if (arguments["debug"].IsTrue && arguments["on"].IsTrue)
                {
                    debug = true;
                    log = false;
                    async.Set();
                    sendMessage("debug mode is on", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                }
                else if (arguments["debug"].IsTrue && arguments["off"].IsTrue)
                {
                    debug = false;
                    log = false;
                    async.Set();
                    sendMessage("debug mode is off", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                }

                if (arguments["platform_SAFe"].IsTrue)
                {
                    if (arguments["set"].IsTrue)
                    {
                        if (arguments["port"].IsTrue)
                        {
                            string port = arguments["<port>"] == null ? null : (string)arguments["<port>"].Value;
                            CoreServices.PlatformSAFe_PORT = int.Parse(port);
                            sendMessage(string.Format("The Platform_SAFe IP port has been changed to {0}", port), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                }
                else if (arguments["core"].IsTrue)
                {
                    if (arguments["set"].IsTrue)
                    {
                        if (arguments["ip"].IsTrue)
                        {
                            string ip = arguments["<ip>"] == null ? null : (string)arguments["<ip>"].Value;
                            CoreServices.CORE_ADDRESS = ip;
                            sendMessage(string.Format("The Core IP address has been changed to {0}", ip), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["port"].IsTrue)
                        {
                            string port = arguments["<port>"] == null ? null : (string)arguments["<port>"].Value;
                            CoreServices.CORE_PORT = int.Parse(port);
                            sendMessage(string.Format("The Core IP port has been changed to {0}", port), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["get"].IsTrue)
                    {
                        if (arguments["ip"].IsTrue)
                        {
                            sendMessage(string.Format("The Core IP address is {0}", CoreServices.CORE_ADDRESS), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["port"].IsTrue)
                        {
                            sendMessage(string.Format("The Core IP port is {0}", CoreServices.CORE_PORT), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["launch"].IsTrue)
                    {
                        string how = arguments.ContainsKey("--how") && arguments["--how"] != null ? (string)arguments["--how"].Value : "local";

                        async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                        async.Set();

                        string region = arguments["--region"] == null ? null : (string)arguments["--region"].Value;

                        //coreservices_launcher = how.Equals("ec2") ? new EC2CoreLauncher() : (how.Equals("local") ? new LocalCoreLauncher() : null);

                        if (how.Equals("ec2"))
                            coreservices_launcher = new EC2CoreLauncher();
                        else if (how.Equals("local"))
                            coreservices_launcher = new LocalCoreLauncher();
                        else
                            coreservices_launcher = null; 

                        if (coreservices_launcher == null)
                        {
                            throw new Exception(string.Format("--how={0} not (yet?) supported", how));
                        }

                        coreservices_launcher.launch(region);

                        CoreServices.CORE_ADDRESS = coreservices_launcher.CoreAddress;

                        sendMessage(string.Format("A Core Services instance has been launched at {0}", coreservices_launcher.CoreAddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["terminate"].IsTrue)
                    {
                        if (coreservices_launcher != null)
                        {
                            async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                            async.Set();

                            coreservices_launcher.terminate();

                            sendMessage(string.Format("The Core services at {0} has been terminated.", coreservices_launcher.CoreAddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                            sendMessage("There is no Core services launched", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);

                    }
                    else if (arguments["start"].IsTrue)
                    {
                        if (coreservices_launcher != null)
                        {
                            async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                            async.Set();

                            coreservices_launcher.start();

                            sendMessage(string.Format("The Core services at '{0}' has been restarted", coreservices_launcher.CoreAddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                            sendMessage("There is no Core services launched", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["stop"].IsTrue)
                    {
                        if (coreservices_launcher != null)
                        {
                            async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                            async.Set();

                            coreservices_launcher.stop();

                            sendMessage(string.Format("The Core services at '{0}' has been stopped", coreservices_launcher.CoreAddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                            sendMessage("There is no Core services launched", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }

                }
                else if (arguments["backend"].IsTrue)
                {
                    name = arguments["<name>"] == null ? "local" : (string)arguments["<name>"].Value;
                    

                    if (arguments["ec2"].IsTrue)
                    {
                        name = name.Equals("local") ? "local (ec2)" : name;

                        if (arguments["remote"].IsTrue || arguments["local"].IsTrue)
                        {
                            if (!backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                string region = arguments["local"].IsTrue ? "local" : arguments["--region"] == null ? "us-east-1" : (string)arguments["--region"].Value;

                                EC2BackendLauncher backend_launcher = new EC2BackendLauncher();
                                backend_launcher.launch(name, region, RegisterCoreServices);
                                backend_launcher_instances[name] = backend_launcher;

                                sendMessage(string.Format("The EC2 backend '{0}' has been launched at {1} (region {2})", name, backend_launcher.BackendAddress, region), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("Backend qualifier '{0}' is already used.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["terminate"].IsTrue)
                        {
                            if (backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                IBackendLauncher backend_launcher = backend_launcher_instances[name];

                                backend_launcher.terminate();

                                backend_launcher_instances.Remove(name);

                                sendMessage(string.Format("The EC2 backend '{0}' has been terminated.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("The EC2 backend '{0}' does not exist.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["start"].IsTrue)
                        {
                            if (backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                IBackendLauncher backend_launcher = backend_launcher_instances[name];

                                backend_launcher.start();

                                sendMessage(string.Format("The EC2 backend '{0}' has been restarted at {1}", name, backend_launcher.BackendAddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("The EC2 backend '{0}' does not exist.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["stop"].IsTrue)
                        {
                            if (backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                IBackendLauncher backend_launcher = backend_launcher_instances[name];

                                backend_launcher.stop();

                                sendMessage(string.Format("The EC2 backend '{0}' has been stopped", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("The EC2 backend '{0}' does not exist.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["gcp"].IsTrue)
                    {
                        name = name.Equals("local") ? "local (gcp)" : name;

                        if (arguments["remote"].IsTrue || arguments["local"].IsTrue)
                        {
                            if (!backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                string zone = arguments["local"].IsTrue ? "local" : arguments["--zone"] == null ? "us-central1-a" : (string)arguments["--zone"].Value;

                                GCPBackendLauncher backend_launcher = new GCPBackendLauncher();
                                backend_launcher.launch(name, zone, RegisterCoreServices);
                                backend_launcher_instances[name] = backend_launcher;

                                sendMessage(string.Format("The Google backend '{0}' has been launched at {1} (region {2})", name, backend_launcher.BackendAddress, zone), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("Backend qualifier '{0}' is already used.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["terminate"].IsTrue)
                        {
                            if (backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                IBackendLauncher backend_launcher = backend_launcher_instances[name];

                                backend_launcher.terminate();

                                backend_launcher_instances.Remove(name);

                                sendMessage(string.Format("The Google backend '{0}' has been terminated.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("The Google backend '{0}' does not exist.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["start"].IsTrue)
                        {
                            if (backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                IBackendLauncher backend_launcher = backend_launcher_instances[name];

                                backend_launcher.start();

                                sendMessage(string.Format("The Google backend '{0}' has been restarted at {1}", name, backend_launcher.BackendAddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("The Google backend '{0}' does not exist.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["stop"].IsTrue)
                        {
                            if (backend_launcher_instances.ContainsKey(name))
                            {
                                async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                                async.Set();

                                IBackendLauncher backend_launcher = backend_launcher_instances[name];

                                backend_launcher.stop();

                                sendMessage(string.Format("The Google backend '{0}' has been stopped", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("The Google backend '{0}' does not exist.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["local"].IsTrue)
                    {
                        if (arguments["launch"].IsTrue)
                        {
                            LocalBackendLauncher local_backend_launcher = new LocalBackendLauncher();
                            local_backend_launcher.launch();
                            backend_launcher_instances["local"] = local_backend_launcher;
                            sendMessage(string.Format("The localhost backend has been launched at port 8081"), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["list"].IsTrue)
                    {
                        if (backend_launcher_instances.Count > 0)
                            foreach (KeyValuePair<string, IBackendLauncher> b in backend_launcher_instances)
                                sendMessage(string.Format("{0} at {1}", b.Key, b.Value.BackendAddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        else
                            sendMessage(string.Format("There is no launched Google backend."), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                }
                else if (arguments["system"].IsFalse && arguments["load"].IsFalse && current_system == null)
                    throw new Exception("No system ! Execute \"new system\" or \"open system\"");
                else
                {
                    name = arguments["<name>"] == null ? null : (string)arguments["<name>"].Value;

                    string contract_contents = arguments["--contract"] != null ? (string)arguments["--contract"].Value : arguments["--signature"] != null ? (string)arguments["--signature"].Value : arguments["--bound"] != null ? (string)arguments["--bound"].Value : null;

                    if (contract_contents != null)
                    {
                        if (contract_contents.StartsWith("!"))
                            contract = contract_store[contract_contents.Substring(1)];
                        else
                            contract = buildContract(contract_contents);
                    }
                    else
                        contract = new Dictionary<string, object>();

                    Console.WriteLine("CONTRACT 4");


                    if (arguments["show"].IsTrue && arguments["log"].IsTrue)
                    {
                        log = false;
                        sendMessage(showLog(), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["browse"].IsTrue)
                    {
                        async_flag = "_";
                        async.Set();
                        name = (string)arguments["<name>"].Value;

                        IBrowserBinding<IRecvDataPortType> browser_port = (IBrowserBinding<IRecvDataPortType>)current_system.Application.Services.getPort(name);

                        IRecvDataPortType o = (IRecvDataPortType)browser_port.Client;

                        active_browser[async_flag] = new Thread(() =>
                        {
                            bool termination_flag = false;
                            while (!termination_flag)
                            {
                                Console.WriteLine("WAIT FOR MESSAGE {0}", name);
                                IList<string> s_list = o.receive_output_message();
                                foreach (string output in s_list)
                                {
                                    termination_flag = output.Equals("!EOS");
                                    if (!termination_flag)
                                    {
                                        sendMessage(output, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                    }
                                }
                            }
                        });

                        active_browser[async_flag].Start();
                        active_browser[async_flag].Join();
                        active_browser.Remove(async_flag);

                        sendMessage("browse finished", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    // else if (arguments["browse"].IsTrue && arguments["--kill"].IsTrue)
                    // {
                    //     name = (string)arguments["<name>"].Value;
                    //
                    //     Console.WriteLine("browsing kill: active_browser.Count: {0}", active_browser.Count);
                    //     foreach (string k in active_browser.Keys) Console.WriteLine("browsing {0}", k);
                    //     if (active_browser.ContainsKey(name))
                    //         active_browser[name].Abort();
                    //      else
                    //          string message = string.Format("A browser named {0} does not exist.", name);
                    // }
                    else if (arguments["create"].IsTrue && arguments["contract"].IsTrue)
                    {
                        //var contract_name = arguments["<name>"] == null ? null : (string)arguments["<name>"].Value;
                        var contract_contents_1 = arguments["<contract>"] == null ? null : (string)arguments["<contract>"].Value;

                        IDictionary<string, object> new_contract = buildContract(contract_contents_1);
                        contract_store[name] = new_contract;
                        sendMessage(string.Format("contract {0} created", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["create"].IsTrue && arguments["computation"].IsTrue)
                    {
                        name = arguments["<name>"] == null ? null : (string)arguments["<name>"].Value;
                        string mp_library = arguments["--message-passing"] == null ? "MPI" : (string)arguments["--message-passing"].Value;
                        string language = arguments["--language"] == null ? "C" : (string)arguments["--language"].Value;
                        ValueObject wrapper_source_fn = arguments["--wrapper-source"];
                        //ArrayList sources = (ArrayList)arguments["--source"].Value;
                        string source = (string)arguments["--source"].Value;
                        Console.WriteLine(source);
                        IList<IDictionary<string, object>> port_list = new List<IDictionary<string, object>>();
                        var port_argument = arguments["--port"].Value;
                        ArrayList port_args = (ArrayList)port_argument;
                        Console.WriteLine(port_argument.GetType());
                        foreach (ValueObject port_contract_string in port_args)
                        {
                            Console.WriteLine("port contract is {0} {1}", port_contract_string, port_contract_string.GetType());
                            var port_contract = buildContract((string)port_contract_string.Value);
                            Console.WriteLine("port contract dictionary ok --- {0}", port_contract);
                            port_list.Add(port_contract);
                        }

                        IDictionary<string, object>[] port_array = new Dictionary<string, object>[port_list.Count];
                        port_list.CopyTo(port_array, 0);

                        current_system.createComputation(name, wrapper_source_fn != null ? (string)wrapper_source_fn.Value : null, new string[1] { source } /*(string[])sources.ToArray()*/, port_array, mp_library, language);

                        sendMessage(string.Format("The computation {0} has been inserted into the catalog\n", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["terminate"].IsTrue && arguments["system"].IsTrue)
                    {
                        if (current_system != null)
                        {
                            system.Remove(current_system.Name);
                            current_system.terminate();
                            sendMessage(string.Format("The system {0} has been terminated !", current_system.Name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);

                            current_system = null;
                            foreach (ISystem s in system.Values)
                                current_system = s;

                            if (current_system != null)
                                sendMessage(string.Format("The system {0} is now active !", current_system.Name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            else
                                sendMessage("There is no active system.", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                            sendMessage("There is no active system.", m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);

                        log = false;
                    }
                    else if (arguments["new"].IsTrue && arguments["system"].IsTrue)
                    {
                        if (!system.ContainsKey(name))
                        {
                            if (arguments["--clear_log"].IsFalse && existsLog(name))
                            {
                                sendMessage(string.Format("ERROR: A log for a system named {0} already exists. Use --clear_log to delete it.\n\n", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                            {
                                if (arguments["--clear_log"].IsTrue && existsLog(name))
                                    deleteLog(name);

                                // Forcing cast ...
                                IDictionary<string, Tuple<string, object>> signature = new Dictionary<string, Tuple<string, object>>();
                                foreach (KeyValuePair<string, object> parameter in contract)
                                {
                                    if (parameter.Value is Tuple<string,object>)
                                        signature[parameter.Key] = (Tuple<string, object>)parameter.Value;
                                    else if (parameter.Key.Equals("name") && parameter.Value is string)
                                        signature[parameter.Key] = new Tuple<string, object>(parameter.Key, parameter.Value);
                                }

                                system[name] = SystemBuilder.build(name, signature);
                                current_system = system[name];

                                sendMessage(string.Format("A system named {0} has been created and it is active\n", current_system.Name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                        }
                        else
                            sendMessage(string.Format("A system named {0} exists and it is active.", name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["new"].IsTrue)
                    {
                        if (current_system.Component.ContainsKey(name))
                        {
                            sendMessage(string.Format("ERROR: A component named {0} already exists in system {1}\n\n", name, current_system.Name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["computation"].IsTrue)
                        {
                            string virtual_platform_name = (string)((ValueObject)((System.Collections.ArrayList)arguments["--platform"].Value)[0]).Value;
                            if (virtual_platform_name.Equals("local") || current_system.VirtualPlatform.ContainsKey(virtual_platform_name))
                            {
                                IVirtualPlatform virtual_platform = virtual_platform_name.Equals("local") ? current_system.PlatformSAFe : current_system.VirtualPlatform[virtual_platform_name];
                                current_system.newComputation(name, contract, virtual_platform);
                                sendMessage(string.Format("A computation {0} placed at virtual platform {1} now exists in system {2} \n", name, virtual_platform_name, current_system.Name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                sendMessage(string.Format("ERROR: A virtual platform named {0} does not exist in system {1}\n\n", virtual_platform_name, current_system.Name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["connector"].IsTrue)
                        {
                            // new connector "connector_name" --platform=vp0:0 --platform=vp1:1
                            ArrayList virtual_platform_name = (ArrayList)arguments["--platform"].Value;
                            Tuple<int, IVirtualPlatform>[] virtual_platform = new Tuple<int, IVirtualPlatform>[virtual_platform_name.Count];
                            IList<Tuple<int, string>> facet_location = new List<Tuple<int, string>>();
                            for (int i = 0; i < virtual_platform.Length; i++)
                            {
                                int facet = 0;
                                string virtual_platform_name_string = (string)((ValueObject)virtual_platform_name[i]).Value;
                                Console.WriteLine("platform:facet = {0}", virtual_platform_name_string);
                                if (virtual_platform_name_string.IndexOf(':') > 0)
                                {
                                    string[] virtual_platform_name_item = virtual_platform_name_string.Split(':');
                                    facet = int.Parse(virtual_platform_name_item[1]);
                                    virtual_platform_name_string = virtual_platform_name_item[0];
                                }
                                virtual_platform[i] = virtual_platform_name_string.Equals("local") ? new Tuple<int, IVirtualPlatform>(facet, current_system.PlatformSAFe) : new Tuple<int, IVirtualPlatform>(facet, current_system.VirtualPlatform[virtual_platform_name_string]);
                                facet_location.Add(new Tuple<int, string>(facet, virtual_platform_name_string));
                            }
                            current_system.newConnector(name, contract, virtual_platform);
                            string message = string.Format("A connector {0} now exists in system {1}, whose facets are placed as following: \n", name, current_system.Name);
                            foreach (Tuple<int, string> facet_place in facet_location)
                                message += string.Format("\t facet {0} is placed at {1} \n", facet_place.Item1, facet_place.Item2);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["datasource"].IsTrue)
                        {
                            string virtual_platform_name = (string)((ValueObject)((System.Collections.ArrayList)arguments["--platform"].Value)[0]).Value;
                            IVirtualPlatform virtual_platform = virtual_platform_name.Equals("local") ? current_system.PlatformSAFe : current_system.VirtualPlatform[virtual_platform_name];
                            current_system.newDataSource(name, contract, virtual_platform);
                            string message = string.Format("A data source {0} placed at virtual platform {1} now exists in system {2} \n", name, virtual_platform_name, current_system.Name);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);

                        }
                        else if (arguments["platform"].IsTrue)
                        {
                            current_system.newVirtualPlatform(name, contract);
                            string message = String.Format("A virtual platform {0} now exists in system {1} \n", name, current_system.Name);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["action-binding"].IsTrue)
                        {
                            string port_type = (string)arguments["--port-type"].Value;
                            System.Collections.ArrayList port_name_list = (System.Collections.ArrayList)arguments["--port"].Value;
                            IActionPort[] action_port_list = new IActionPort[port_name_list.Count];
                            for (int i = 0; i < action_port_list.Length; i++)
                            {
                                string port_ref_complete = (string)((ValueObject)port_name_list[i]).Value;

                                IActionPort action_port;
                                if (port_ref_complete.Equals("workflow"))
                                    action_port = current_system.Workflow.NewActionPort;
                                else
                                {
                                    string[] port_ref_ = port_ref_complete.Split(':');
                                    int index = port_ref_.Length > 1 ? int.Parse(port_ref_[1]) : 0;
                                    string[] port_ref = port_ref_[0].Split('.');
                                    string component_name = port_ref[0];
                                    string port_name = port_ref[1];
                                    action_port = ((IActionComponent)current_system.Component[component_name]).ActionPort[port_name][index];
                                }
                                action_port_list[i] = action_port;
                            }

                            current_system.newActionBinding(name, port_type, action_port_list);
                            string message = string.Format("An action binding {0} now exists in system {1}. It binds the following action ports: \n", name, current_system.Name);
                            foreach (IActionPort action_port in action_port_list)
                                message += string.Format("\t port {0}:{1} of {2}; \n", action_port.Name, action_port.Index, action_port.Component.Name);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["service-binding"].IsTrue)
                        {
                            string port_ref_user__ = (string)arguments["--user-port"].Value;
                            string port_ref_provider__ = ((string)arguments["--provider-port"].Value);

                            IServicePort user_port;
                            if (port_ref_user__.Equals("application"))
                                user_port = current_system.Application.NewServicePort;
                            else
                            {
                                string[] port_ref_user_ = (port_ref_user__).Split(':');
                                int index_user = port_ref_user_.Length > 1 ? int.Parse(port_ref_user_[1]) : 0;
                                string[] port_ref_user = port_ref_user_[0].Split('.');
                                string component_name_user = port_ref_user[0];
                                string port_name_user = port_ref_user[1];
                                user_port = ((IServiceSolutionComponent)current_system.Component[component_name_user]).UserPort[port_name_user][index_user];
                            }

                            IServicePort provider_port;
                            if (port_ref_provider__.Equals("application"))
                                provider_port = current_system.Application.NewServicePort;
                            else
                            {
                                string[] port_ref_provider_ = (port_ref_provider__).Split(':');
                                int index_provider = port_ref_provider_.Length > 1 ? int.Parse(port_ref_provider_[1]) : 0;
                                string[] port_ref_provider = port_ref_provider_[0].Split('.');
                                string component_name_provider = port_ref_provider[0];
                                string port_name_provider = port_ref_provider[1];
                                provider_port = ((IServiceSolutionComponent)current_system.Component[component_name_provider]).ProviderPort[port_name_provider][index_provider];
                            }
                            current_system.newServiceBinding(name, contract, (IUserServicePort)user_port, (IProviderServicePort)provider_port);
                            string message = string.Format("A service binding {0} now exists in system {1}. It binds the following user and provider ports: \n", name, current_system.Name);
                            message += string.Format("\t the user port {0}:{1} of {2}; \n", user_port.Name, user_port.Index, user_port.Component.Name);
                            message += string.Format("\t the provider port {0}:{1} of {2}. \n", provider_port.Name, provider_port.Index, provider_port.Component.Name);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["add"].IsTrue && arguments["parameter"].IsTrue)
                    {
                        string variable = arguments["--variable"] == null ? NewVariableName : (string)arguments["--variable"].Value;
                        current_system.addParameter(name, variable, contract);
                        string message = String.Format("A parameter {0} with variable {1} now exists in system {2}. It has the following bound: \n {3}", name, variable, current_system.Name, showContract(contract, m_header, m_parent_header, m_metadata, m_content, execution_count));
                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["apply"].IsTrue && arguments["argument"].IsTrue)
                    {
                        current_system.setArgument(name, contract);
                        string message = String.Format("The following contract has been applied to parameter {0} in system {1}: \n {2}", name, current_system.Name, showContract(contract, m_header, m_parent_header, m_metadata, m_content, execution_count));
                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["set"].IsTrue && arguments["workflow"].IsTrue)
                    {
                        string file_contents;
                        if (File.Exists(name))
                            file_contents = File.ReadAllText(name);
                        else
                            throw new Exception("A SAFwSWL orchestration file named '" + name + "' was not found !");

                        setWorkflow(file_contents);
                        string message = string.Format("The following SAFeSWL orchestration code of file '{0}' has been applied to the worflow component of system {1} \n\n {2}", name, current_system.Name, file_contents);
                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["resolve"].IsTrue)
                    {
                        bool sequential = arguments.ContainsKey("--sequential") && arguments["--sequential"].IsTrue;
                        async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                        async.Set();
                        bool test_system = arguments["--system"].IsTrue;

                        if (test_system)
                        {
                            LifeCycleAction lf_action_resolve = ((IActionPortLifeCycle port) => port.resolve_system());
                            run_lifecycle_action(arguments, lf_action_resolve, "resolved", sequential, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                        {
                            LifeCycleAction lf_action_resolve = ((IActionPortLifeCycle port) => port.resolve());
                            run_lifecycle_action(arguments, lf_action_resolve, "resolved", sequential, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["certify"].IsTrue)
                    {
                        bool sequential = arguments.ContainsKey("--sequential") && arguments["--sequential"].IsTrue;
                        async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                        async.Set();
                        LifeCycleAction lf_action_certify = ((IActionPortLifeCycle port) => port.certify());
                        run_lifecycle_action(arguments, lf_action_certify, "certified", sequential, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["deploy"].IsTrue)
                    {
                        bool sequential = arguments.ContainsKey("--sequential") && arguments["--sequential"].IsTrue;
                        async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                        async.Set();
                        LifeCycleAction lf_action_deploy = ((IActionPortLifeCycle port) =>
                                                                    {
                                                                        port.deploy();
                                                                        string vpaddress = current_system.getVirtualPlatformAddress(port.ComponentName);
                                                                        if (!vpaddress.Equals("deployed"))
                                                                            sendMessage(string.Format("The address of the virtual platform '{0}' is {1}", port.ComponentName, vpaddress), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                                                    }
                                                                    );


                        run_lifecycle_action(arguments, lf_action_deploy, "deployed", sequential, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);


                    }
                    else if (arguments["instantiate"].IsTrue)
                    {
                        bool sequential = arguments.ContainsKey("--sequential") && arguments["--sequential"].IsTrue;
                        async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                        async.Set();
                        LifeCycleAction lf_action_instantiate = ((IActionPortLifeCycle port) => port.instantiate());
                        run_lifecycle_action(arguments, lf_action_instantiate, "instantiated", sequential, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["run"].IsTrue)
                    {
                        bool sequential = arguments.ContainsKey("--sequential") && arguments["--sequential"].IsTrue;
                        async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                        async.Set();
                        LifeCycleAction lf_action_run = ((IActionPortLifeCycle port) => port.run());
                        run_lifecycle_action(arguments, lf_action_run, "running", sequential, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["release"].IsTrue)
                    {
                        bool sequential = arguments.ContainsKey("--sequential") && arguments["--sequential"].IsTrue;
                        async_flag = arguments.ContainsKey("--async") && arguments["--async"] != null ? (string)arguments["--async"].Value : null;
                        async.Set();
                        LifeCycleAction lf_action_release = ((IActionPortLifeCycle port) => port.release());
                        run_lifecycle_action(arguments, lf_action_release, "released", sequential, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);

                        ArrayList name_list_ = (ArrayList)arguments["<name_list>"].Value;
                        string[] name_list = new string[name_list_.Count];
                        for (int i = 0; i < name_list.Length; i++)
                            name_list[i] = (string)((ValueObject)name_list_[i]).Value;

                        foreach (string n in name_list)
                        {
                            current_system.release(n);
                        }
                    }
                    else if (arguments["async_wait"].IsTrue)
                    {
                        async_flag = "";
                        ArrayList name_list_ = (ArrayList)arguments["<name_list>"].Value;
                        for (int i = 0; i < name_list_.Count; i++)
                            async_flag += "," + (string)((ValueObject)name_list_[i]).Value;
                        async.Set();
                        Console.WriteLine("async_wait {0}", async_flag);
                        return_message = string.Format("{0} arrived !", async_flag.Substring(1));
                        //sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["invoke"].IsTrue)
                    {
                        if (!current_system.Workflow.ActionPort.ContainsKey(name))
                        {
                            string message = string.Format("ERROR: An action port named {0} does not exist in system {1}\n\n", name, current_system.Name);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                        {
                            IActionPortWorkflow action_port = (IActionPortWorkflow)current_system.Workflow.ActionPort[name][0];
                            ArrayList action_name_array = (ArrayList)arguments["--action"].Value;
                            string[] action_name = new string[action_name_array.Count];
                            for (int i = 0; i < action_name.Length; i++)
                                action_name[i] = (string)((ValueObject)action_name_array[i]).Value;
                            if (action_name_array.Count == 1)
                            {
                                if (arguments["--handle"] == null)
                                {
                                    action_port.invoke(action_name[0]);
                                    string message = String.Format("The action {0} of port {1} has been activated and completed in system {2}.", action_name[0], action_port.Name, current_system.Name);
                                    sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                }
                                else
                                {
                                    IActionFuture future;
                                    action_port.invoke(action_name[0], out future);
                                    string handle = (string)arguments["--handle"].Value;
                                    invocation_future_list[handle] = future;
                                    invocation_future_list_inv[future] = handle;
                                    string message = String.Format("The action {0} of port {1} has been activated with handle {2} in system {3}\n", action_name[0], action_port.Name, handle, current_system.Name);
                                    sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                }
                            }
                            else
                            {
                                if (arguments["--handle"] == null)
                                {
                                    var action_result = action_port.invoke(action_name);
                                    string message = String.Format("The alternative action {0} of port {1} has been activated and completed with result {2} in system {3}.", action_name[0], action_port.Name, action_result, current_system.Name);
                                    sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                }
                                else
                                {
                                    IActionFuture future;
                                    action_port.invoke(action_name, out future);
                                    string handle = (string)arguments["--handle"].Value;
                                    invocation_future_list[handle] = future;
                                    invocation_future_list_inv[future] = handle;
                                    string message = String.Format("The alternative action {0} of port {1} has been activated with handle {2} in system {3}.", action_name[0], action_port.Name, handle, current_system.Name);
                                    sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                }
                            }
                        }
                    }
                    else if (arguments["wait"].IsTrue || arguments["test"].IsTrue)
                    {
                        if (arguments["any"].IsTrue || arguments["all"].IsTrue)
                        {
                            IActionFutureSet future_set = null;
                            ArrayList handle_list = (ArrayList)arguments["<handle_name>"].Value;

                            foreach (object o in handle_list)
                            {
                                string handle = (string)((ValueObject)o).Value;
                                if (!invocation_future_list.ContainsKey(handle))
                                    throw new Exception(string.Format("ERROR: A handle named {0} does not exist in system {1}\n\n", handle, current_system.Name));
                                IActionFuture future = invocation_future_list[handle];
                                if (future_set == null)
                                    future_set = future.createSet();
                                else
                                    future_set.addAction(future);

                            }

                            if (arguments["any"].IsTrue)
                            {
                                IActionFuture f = null;
                                if (arguments["wait"].IsTrue)
                                    f = future_set.waitAny();
                                else if (arguments["test"].IsTrue)
                                    f = future_set.testAny();

                                string message = (f != null ? invocation_future_list_inv[f] + "completed !" : "not completed !") + "\n";
                                sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else if (arguments["all"].IsTrue)
                            {
                                bool completed = true;
                                if (arguments["wait"].IsTrue)
                                    future_set.waitAll();
                                else if (arguments["test"].IsTrue)
                                    completed = future_set.testAll();

                                string message = completed ? "all completed !\n" : "not completed !\n";
                                sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                        }
                        else
                        {
                            string handle = (string)((ValueObject)((System.Collections.ArrayList)arguments["<handle_name>"].Value)[0]).Value;

                            if (invocation_future_list.ContainsKey(handle))
                            {
                                IActionFuture future = invocation_future_list[handle];
                                bool completed = true;
                                if (arguments["wait"].IsTrue)
                                    future.wait();
                                else if (arguments["test"].IsTrue)
                                    completed = future.test();
                                string message = completed ? "completed\n" : "not completed\n";
                                sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                            {
                                string message = string.Format("ERROR: A handle named {0} does not exist in system {1}\n\n", handle, current_system.Name);
                                sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                        }
                    }
                    else if (arguments["list"].IsTrue)
                    {
                        log = false;
                        if (arguments["system"].IsTrue)
                        {
                            string message = "listing " + system.Count + " systems:\n\n";
                            foreach (string id in system.Keys)
                            {
                                message += id;
                                if (id.Equals(current_system.Name))
                                    message += " (current)\n";
                                else
                                    message += "\n";
                            }
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["component"].IsTrue)
                        {
                            string message = list_component<IComponent>(current_system.Component, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["computation"].IsTrue)
                        {
                            string message = list_component<IComputation>(current_system.Computation, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["connector"].IsTrue)
                        {
                            string message = list_component<IConnector>(current_system.Connector, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["datasource"].IsTrue)
                        {
                            string message = list_component<IDataSource>(current_system.DataSource, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["platform"].IsTrue)
                        {
                            string message = list_component<IVirtualPlatform>(current_system.VirtualPlatform, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["binding"].IsTrue)
                        {
                            string message = list_component<IBinding>(current_system.Binding, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["action-binding"].IsTrue)
                        {
                            string message = list_component<IActionBinding>(current_system.ActionBinding, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["service-binding"].IsTrue)
                        {
                            string message = list_component<IServiceBinding>(current_system.ServiceBinding, arguments);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["parameter"].IsTrue)
                        {
                            string message = string.Format("listing {0} parameters:\n\n{1}", current_system.ContextualSignature.Count, showSignature(current_system.ContextualSignature, m_header, m_parent_header, m_metadata, m_content, execution_count));
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["argument"].IsTrue)
                        {
                            string message = string.Format("listing {0} arguments:\n\n{1}", current_system.Contract.Count, showContract(current_system.Contract, m_header, m_parent_header, m_metadata, m_content, execution_count));
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else if (arguments["action-port"].IsTrue || arguments["service-port"].IsTrue)
                        {
                            IComponent c_ = null;
                            if (name.Equals("application"))
                                c_ = current_system.Application;
                            else if (name.Equals("workflow"))
                                c_ = current_system.Workflow;

                            if (c_ != null || current_system.Component.TryGetValue(name, out c_))
                            {
                                if (arguments["service-port"].IsTrue)
                                {
                                    if (c_ is IServiceSolutionComponent)
                                    {
                                        IServiceSolutionComponent c = (IServiceSolutionComponent)c_;
                                        string message = "listing " + c.ServicePort.Count + " service ports:\n\n";
                                        foreach (KeyValuePair<string, IServicePort[]> port in c.ServicePort)
                                        {
                                            string port_name = port.Key;
                                            if (port.Value.Length > 1)
                                                for (int i = 0; i < port.Value.Length; i++)
                                                    message += port_name + ":" + i + "\n";
                                            else
                                                message += port_name + "\n";
                                        }
                                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                    }
                                    else
                                    {
                                        string message = string.Format("ERROR: The component named {0} of system {1} does support service ports ({2})\n\n", name, current_system.Name, c_.GetType());
                                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                    }

                                }
                                else if (arguments["action-port"].IsTrue)
                                {
                                    if (c_ is IActionComponent)
                                    {
                                        IActionComponent c = (IActionComponent)c_;
                                        string message = "listing " + c.ActionPort.Count + " action ports:\n\n";
                                        foreach (KeyValuePair<string, IActionPort[]> port in c.ActionPort)
                                        {
                                            string port_name = port.Key;
                                            if (port.Value.Length > 1)
                                                for (int i = 0; i < port.Value.Length; i++)
                                                    message += port_name + ":" + i + "\n";
                                            else
                                                message += port_name + "\n";
                                        }
                                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                    }
                                    else
                                    {
                                        string message = string.Format("ERROR: The component named {0} of system {1} does support action ports ({2})\n\n", name, current_system.Name, c_.GetType());
                                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                                    }
                                }
                            }
                            else
                            {
                                string message = string.Format("ERROR: A component named {0} does not exist in system {1}\n\n", name, current_system.Name);
                                sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                        }
                        else if (arguments["handle"].IsTrue)
                        {
                            string message = "listing " + invocation_future_list.Count + " handles:\n\n";

                            foreach (string handle in invocation_future_list.Keys)
                                message += handle + "\n";

                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["contract"].IsTrue)
                    {
                        log = false;
                        IComponent c;
                        if (current_system.Component.TryGetValue(name, out c))
                        {
                            string message = showContract(c.Contract, m_header, m_parent_header, m_metadata, m_content, execution_count);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                        {
                            string message = string.Format("ERROR: A component named {0} does not exist in system {1}\n\n", name, current_system.Name);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else if (arguments["bound"].IsTrue)
                    {
                        log = false;
                        string message = "\n";
                        object bound = current_system.ContextualSignature[name].Item2;
                        if (bound is IDictionary<string, object>)
                            message += showContract((System.Collections.Generic.IDictionary<string, object>)bound, m_header, m_parent_header, m_metadata, m_content, execution_count);
                        else
                            message += bound + "\n";
                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["argument"].IsTrue)
                    {
                        log = false;
                        IDictionary<string, object> argument_contract = (IDictionary<string, object>)current_system.Contract[name];
                        string message = showContract(argument_contract, m_header, m_parent_header, m_metadata, m_content, execution_count);
                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    else if (arguments["save"].IsTrue)
                    {
                        log = false;
                        throw new NotImplementedException();
                    }
                    else if (arguments["load"].IsTrue)
                    {
                        log = false;
                        runLog((string)arguments["<name>"].Value, session, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                    // else if (arguments["clear"].IsTrue)
                    // {
                    //     log = false;
                    //     current_system = null;
                    //     system.Remove(current_system.Name);
                    //removeLog();
                    // }
                    else if (arguments["open"].IsTrue && arguments["system"].IsTrue)
                    {
                        log = false;

                        string system_name = (string)arguments["<name>"].Value;
                        if (system.ContainsKey(system_name))
                        {
                            string message = "Changing system from " + current_system.Name + " to ";
                            current_system = system[system_name];
                            message += current_system.Name + "\n";
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                        else
                        {
                            string message = string.Format("ERROR: A system named {0} does not exist\n\n", name);
                            sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        }
                    }
                    else
                    {
                        string message = string.Format("ERROR: Unrecognized command\n\n", name, current_system.Name);
                        sendMessage(message, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
                }
            }
            finally
            {
                if (async_flag == null)
                    async.Set();
            }

            return return_message;
        }

       

        private static string list_component<T>(IDictionary<string, T> components, IDictionary<string, ValueObject> arguments)
            where T : IComponent
        {
            string return_message = "";

            IList<string> virtual_platform_name_list = take_virtual_platform_name_list(arguments);
            ICollection<string> current_virtual_platform_name_list = current_system.VirtualPlatform.Keys;

            IList<string> rm = new List<string>();
            foreach (string v in virtual_platform_name_list)
                if (!current_virtual_platform_name_list.Contains(v))
                {
                    return_message += string.Format("There is no virtual platform named {0}\n", v);
                    rm.Add(v);
                }

            foreach (string v in rm)
                virtual_platform_name_list.Remove(v);

            //return_message += "listing " + current_system.Computation.Count + " computations:\n\n";
            if (components != null && components.Count > 0)
                foreach (KeyValuePair<string, T> kv in components)
                {
                    string id = kv.Key;
                    if (kv.Value.Placement.Count > 0)
                    {
                        foreach (KeyValuePair<int, IVirtualPlatform[]> virtual_platform_placement_list in kv.Value.Placement)
                        {
                            int facet_count = virtual_platform_placement_list.Value.Length;
                            int facet = virtual_platform_placement_list.Key;
                            Console.WriteLine("{1}: virtual_platform_placement_list.Value.Length = {0}", virtual_platform_placement_list.Value.Length, kv.Key);
                            foreach (IVirtualPlatform virtual_platform_place in virtual_platform_placement_list.Value)
                            {
                                if (virtual_platform_name_list.Count == 0 || virtual_platform_name_list.Contains(virtual_platform_place.Name))
                                {
                                    if (facet_count > 1)
                                        return_message += string.Format("{0}:{1} at {2}\n", id, facet, virtual_platform_place.Name);
                                    else
                                        return_message += string.Format("{0} at {1}\n", id, virtual_platform_place.Name);
                                }
                            }
                        }
                    }
                    else
                        return_message += string.Format("{0}\n", id);
                }
            else
                return_message += "There is no component of this kind\n";

            return return_message;
        }
        private static IList<string> take_virtual_platform_name_list(IDictionary<string, ValueObject> arguments)
        {
            ArrayList virtual_platform_name = (ArrayList)arguments["--platform"].Value;
            IList<string> virtual_platform_name_list = new List<string>();
            for (int i = 0; i < virtual_platform_name.Count; i++)
            {
                string virtual_platform_name_string = (string)((ValueObject)virtual_platform_name[i]).Value;
                virtual_platform_name_list.Add(virtual_platform_name_string);
            }

            return virtual_platform_name_list;
        }

        private static void run_lifecycle_action(IDictionary<string, ValueObject> arguments, 
                                                 LifeCycleAction lf_action, 
                                                 string status, 
                                                 bool sequential,
                                                 IDictionary<string, object> m_header,
                                                 IDictionary<string, object> m_parent_header,
                                                 IDictionary<string, object> m_metadata,
                                                 IDictionary<string, object> m_content,
                                                 ExecutionCounter execution_count,
                                                 object lock_message)
        {
            bool test_handle = arguments["--handle"] == null;

            ArrayList name_list_ = (ArrayList)arguments["<name_list>"].Value;
            string[] name_list = new string[name_list_.Count];
            for (int i = 0; i < name_list.Length; i++)
                name_list[i] = (string)((ValueObject)name_list_[i]).Value;

            Exception[] run_exception = new Exception[name_list.Length];
            Thread[] t_list = new Thread[name_list.Length];

            int thread_index = 0;
            foreach (string name__ in name_list)
            {
                Thread t = t_list[thread_index] = new Thread((obj) =>
                {
                    Tuple<int, string> arg = (Tuple<int, string>)obj;

                    int tidx = (int)arg.Item1;
                    string name_ = (string)arg.Item2;

                    try
                    {
                        bool found = current_system.Component.ContainsKey(name_);
                        if (!found)
                            sendMessage(string.Format("ERROR: A component named '{0}' does not exist in system '{1}'", name_, current_system.Name), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                        else
                        {
                            if (test_handle)
                            {
                                IActionPortLifeCycle lifecycle_port = (IActionPortLifeCycle)current_system.Workflow.ActionPort[name_ + ".lifecycle"][0];
                                lf_action(lifecycle_port);
                                sendMessage(string.Format("The component '{0}' of system '{1}' is {2}.", name_, current_system.Name, status), m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                            }
                            else
                                throw new NotImplementedException("Asynchronous life-cycle operation.");
                        }
                    }
                    catch (Exception e)
                    {
                        run_exception[tidx] = e;
                    }
                });

                t.Start(new Tuple<int, string>(thread_index++, name__));
                if (sequential)
                    t.Join();
            }

            if (!sequential)
                foreach (Thread t in t_list)
                    t.Join();

            for (int i = 0; i < run_exception.Length; i++)
            {
                //Console.WriteLine("Exception test -- {0} !", run_exception[i] != null);
                if (run_exception[i] != null)
                    throw run_exception[i];
            }
        }

        private static void sendMessage(string message,
                                        IDictionary<string, object> m_header,
                                        IDictionary<string, object> m_parent_header,
                                        IDictionary<string, object> m_metadata,
                                        IDictionary<string, object> m_content,
                                        ExecutionCounter execution_count,
                                        object lock_message)
        {
            Dictionary<string, object> metadata = dict();
            Dictionary<string, object> content = dict("execution_count", execution_count.increment(), "data", dict("text/plain", message), "metadata", dict());
            lock (lock_message)
            {
                session.send(session.iopub_channel, "pyout", blist(), m_header, metadata, content);
            }
        }

        private static void lf(IActionPortLifeCycle lifecycle_port)
        {
            lifecycle_port.instantiate();
        }

        private static readonly object lock_core_services = new object();

        private static CoreServices core_services = null;
        private static CoreServices RegisterCoreServices
        {
            get
            {
                lock (lock_core_services)
                {
                    if (core_services == null) core_services = new CoreServices("register", out _);
                }
                return core_services;
            }
        }

        /* private static void createComputation(string qname, string wrapper_source_fn, string[] sources, string mp_library, string language)
         {
             if (mp_library.Equals("MPI") && language.Equals("C"))
             {
                 string packagePath = qname.Substring(0, qname.LastIndexOf('.'));
                 string name = qname.Substring(qname.LastIndexOf('.') + 1);

                 // ABSTRACT COMPONENT

                 string snk_fn_abstract = name + ".snk";
                 string pub_fn_abstract = name + ".pub";

                 string uid_abstract = FileUtil.create_snk_file(snk_fn_abstract, pub_fn_abstract);

                 string hpe_file_name_abstract = Path.Combine(Constants.HPCShelf_HOME, System.Environment.GetEnvironmentVariable("TEMPLATE_CODE_ABSTRACT")); // TEMPLATE CODE ...
                 string module_data_abstract = File.ReadAllText(hpe_file_name_abstract);
                 ComponentType c_abstract = LoaderApp.deserialize<ComponentType>(module_data_abstract);

                 c_abstract.header.hash_component_UID = uid_abstract;
                 c_abstract.header.name = name;
                 c_abstract.header.packagePath = packagePath;

                 // look for the single interface element
                 for (int k = 0; k < c_abstract.componentInfo.Length; k++)
                 {
                     object o = c_abstract.componentInfo[k];
                     if (o is InterfaceType)
                     {
                         InterfaceType it = (InterfaceType)o;
                         it.sources[0].moduleName = packagePath + "." + name + ".IProcessLauncher";
                         foreach (SourceFileType sft in it.sources[0].file)
                             sft.contents = sft.contents.Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);
                     }
                 }

                 string module_data_out_abstract = LoaderApp.serialize<ComponentType>(c_abstract);

                 byte[] snk_contents_abstract = File.ReadAllBytes(snk_fn_abstract);

                 RegisterCoreServices.register(module_data_out_abstract, snk_contents_abstract);

                 // CONCRETE COMPONENT

                 IDictionary<string, string[]> source_list = takeSources(sources);

                 string snk_fn_concrete = name + "_impl.snk";
                 string pub_fn_concrete = name + "_impl.pub";

                 string uid_concrete = FileUtil.create_snk_file(snk_fn_concrete, pub_fn_concrete);

                 string hpe_file_name_concrete = Path.Combine(Constants.HPCShelf_HOME, System.Environment.GetEnvironmentVariable("TEMPLATE_CODE_CONCRETE")); // TEMPLATE CODE ...
                 string module_data_concrete = File.ReadAllText(hpe_file_name_concrete);
                 ComponentType c_concrete = LoaderApp.deserialize<ComponentType>(module_data_concrete);

                 // Configure the new computation component ...

                 c_concrete.header.hash_component_UID = uid_concrete;
                 c_concrete.header.name = name + "Impl";
                 c_concrete.header.packagePath = packagePath;
                 c_concrete.header.baseType.component.name = name;
                 c_concrete.header.baseType.component.package = packagePath;
                 c_concrete.header.baseType.component.hash_component_UID = uid_abstract;

                 // look for the single interface element
                 for (int k = 0; k < c_concrete.componentInfo.Length; k++)
                 {
                     object o = c_concrete.componentInfo[k];
                     if (o is InterfaceType)
                     {
                         InterfaceType it = (InterfaceType)o;
                         SourceType[] it_sources = new SourceType[source_list.Count + 1];
                         it_sources[0] = it.sources[0];
                         it_sources[0].moduleName = packagePath + "." + name + "Impl.IProcessLauncherForCImpl";
                         foreach (SourceFileType sft in it.sources[0].file)
                             sft.contents = sft.contents.Replace("org.hpcshelf.mpi.impl.MPILauncherForCImpl", packagePath + "." + name + "Impl").Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);

                         if (wrapper_source_fn != null)
                             changeUserWrapperSource(it_sources[0], wrapper_source_fn);

                         int i = 1;
                         foreach (KeyValuePair<string, string[]> source_item in source_list)
                         {
                             string module_name = source_item.Key;
                             it_sources[i] = new SourceType();
                             it_sources[i].moduleName = module_name;
                             it_sources[i].deployType = module_name.Equals("mpirun") ? "C/MPI Interoperability" : "C Interoperability";
                             it_sources[i].sourceType = "C";
                             it_sources[i].file = new SourceFileType[source_item.Value.Length];
                             for (int j = 0; j < source_item.Value.Length; j++)
                             {
                                 string file_path = source_item.Value[j];
                                 string contents = File.ReadAllText(file_path);
                                 string file_name = Path.GetFileName(file_path);

                                 object file_ext = Path.GetExtension(file_name);
                                 string srcType = file_ext.Equals(".h") ? "header" : file_ext.Equals(".c") ? "source" : "unkown";

                                 it_sources[i].file[j] = new SourceFileType();
                                 it_sources[i].file[j].srcType = srcType;
                                 it_sources[i].file[j].name = file_name;
                                 it_sources[i].file[j].fileType = "C source code";
                                 it_sources[i].file[j].contents = contents;
                             }
                             i++;
                         }
                         it.sources = it_sources;
                     }
                 }

                 string module_data_out = LoaderApp.serialize<ComponentType>(c_concrete);

                 byte[] snk_contents_concrete = File.ReadAllBytes(snk_fn_concrete);

                 RegisterCoreServices.register(module_data_out, snk_contents_concrete);
             }
             else
                 throw new NotImplementedException();
         }

         private static void changeUserWrapperSource(SourceType st, string fn)
         {
             if (!File.Exists(fn))
                 throw new Exception("Wrapper source file name not found (" + fn + ")");

             foreach (SourceFileType sft in st.file)
                 if (sft.srcType.Equals("user"))
                     sft.contents = File.ReadAllText(fn);
         }

         private static IDictionary<string, string[]> takeSources(string[] sources)
         {
             IDictionary<string, string[]> result = new Dictionary<string, string[]>();

             foreach (string file_descriptor in sources)
             {
                 // Test whether the file_descriptor is a directory or not
                 if (Directory.Exists(file_descriptor))
                     result[file_descriptor] = Directory.GetFiles(file_descriptor);
                 // Otherwise, test whether it is a file_name or not (in this case, take all files with with some extension).
                 else
                 {
                     string current_directory = Directory.GetCurrentDirectory();
                     result[file_descriptor] = Directory.GetFiles(current_directory, file_descriptor + ".*");
                 }
             }

             return result;
         }*/

        private static void setWorkflow(string file_contents)
        {
            current_system.Workflow.Unit.SWLOrchestration = file_contents;
        }

        private static void deleteLog(string name)
        {
            string file_name = name + ".log";
            File.Delete(file_name);
        }

        private static bool existsLog(string name)
        {
            string file_name = name + ".log";
            return File.Exists(file_name);
        }

        protected internal void runLog(string system_name,
                                              Session session,
                                              IDictionary<string, object> m_header,
                                              IDictionary<string, object> m_parent_header,
                                              IDictionary<string, object> m_metadata,
                                              IDictionary<string, object> m_content,
                                              ExecutionCounter execution_count,
                                              object lock_message)
        {
            string file_name = system_name + ".log";
            bool debug = false;
            string[] commands = File.ReadAllLines(file_name);
            File.Delete(file_name);
            try
            {
                foreach (string line in commands)
                    if (!line.Trim().Equals(""))
                    {
                        Console.WriteLine("running {0}", line);
                        executeLine(line.Trim().Split(' '), out _, out _, ref debug, session, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    }
            }
            finally
            {
                File.WriteAllLines(file_name, commands);
                Console.WriteLine("{0} commands executed:\n\n", commands.Length);
                foreach (string c in commands)
                    Console.WriteLine(c);
            }
        }

        private static void removeLog()
        {
            string name = current_system.Name;
            File.Delete(name + ".log");
        }

        private static void saveLog(string[] args)
        {
            string line = "";
            foreach (string arg in args)
                line += arg + " ";

            string name = current_system.Name;
            string file_name = name + ".log";
            if (File.Exists(file_name))
                File.AppendAllText(file_name, "\n" + line);
            else
                File.AppendAllText(file_name, line);
        }

        private static string showLog()
        {
            string name = current_system.Name;
            return File.ReadAllText(name + ".log") + "\n";
        }

        private static string showContract(IDictionary<string, object> contract,
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content,
                                          ExecutionCounter execution_count)
        {
            return showContract("", contract, m_header, m_parent_header, m_metadata, m_content, execution_count);
        }

        private static string showSignature(IDictionary<string, Tuple<string, object>> bound,
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content,
                                          ExecutionCounter execution_count)
        {
            string message = "";
            foreach (KeyValuePair<string, Tuple<string, object>> argument in bound)
            {
                if (argument.Value.Item2 is decimal)
                    message += argument.Key + ":" + argument.Value.Item1 + " = " + ((decimal)argument.Value.Item2).ToString(CultureInfo.InvariantCulture) + "\n";
                else if (argument.Value.Item2 is IDictionary<string, object>)
                    message += showContract(argument.Key + ":" + argument.Value.Item1 + "-", (IDictionary<string, object>)argument.Value.Item2, m_header, m_parent_header, m_metadata, m_content, execution_count);
            }
            return message;
        }

        private static string showContract(string prefix, 
                                          IDictionary<string, object> contract,
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content,
                                          ExecutionCounter execution_count)
        {
            string message = "";
            foreach (KeyValuePair<string, object> argument in contract)
            {
                if (argument.Value is string)
                    message += prefix + argument.Key + " = " + argument.Value + "\n";
                else if (argument.Value is decimal)
                    message += prefix + argument.Key + " = " + ((decimal)argument.Value).ToString(CultureInfo.InvariantCulture) + "\n";
                else if (argument.Value is int)
                    message += prefix + argument.Key + " = " + ((int)argument.Value).ToString(CultureInfo.InvariantCulture) + "\n";
                else if (argument.Value is IDictionary<string, object>)
                    message += showContract(prefix + argument.Key + "-", (IDictionary<string, object>)argument.Value, m_header, m_parent_header, m_metadata, m_content, execution_count);
            }
            return message;
        }

        private static IDictionary<string, object> buildContract(string contract)
        {
            IDictionary<string, object> contract_dict = new Dictionary<string, object>();

            string contract_contents = File.Exists(contract) ? File.ReadAllText(contract) : contract;

            string[] contract_parameter_list = contract_contents.Split(new char[3] { '\n', ',', ';' });

            Queue<string> q_parameters = new Queue<string>();
            foreach (string contract_parameter in contract_parameter_list)
                q_parameters.Enqueue(contract_parameter);

            while (q_parameters.Count > 0)
            {
                string contract_parameter = q_parameters.Dequeue();

                string[] contract_parameter_parts = contract_parameter.Split('=');
                string[] parameter_id_var = contract_parameter_parts[0].Split(':');

                IDictionary<string, object> contract_par = contract_dict;

                string parameter_id = parameter_id_var[0];
                //string[] parameter_id__ = parameter_id_.Split('-');
                //string parameter_id = null;

               // foreach (string s in parameter_id__)
               // {
               //     if (!contract_par.ContainsKey(s))
               //     {
               //         parameter_id = s;
               //     }
               //     else
               //     {
               //         contract_par = (IDictionary<string, object>)contract_par[s];
               //     }
               // }

                string var_id = parameter_id_var.Length > 1 ? parameter_id_var[1] : null;

                if (parameter_id.Equals("name") && var_id != null)
                    throw new Exception("Invalid parameter in contract: " + contract_parameter);
                    
                string argument = contract_parameter_parts[1];
                int int_value = default(int);
                decimal dec_value = default(decimal);
                if (int.TryParse(argument, out int_value))
                    contract_par[parameter_id] = var_id == null ? (object)int_value : (object)new Tuple<string, object>(var_id, int_value);
                else if (decimal.TryParse(argument, out dec_value))
                    contract_par[parameter_id] = var_id == null ? (object)dec_value : (object)new Tuple<string, object>(var_id, dec_value);
                else if (parameter_id.Equals("name"))
                    contract_par[parameter_id] = argument;
                else if (argument.StartsWith("!"))
                    contract_par[parameter_id] = argument.Substring(1);
                else if (var_id != null)
                    contract_par[parameter_id] = (object)new Tuple<string, object>(var_id, new Dictionary<string, object>() { { "name", argument } });
                else
                {
                    contract_par[parameter_id] = new Dictionary<string, object>() { { "name", argument } };
                }
            }

            return contract_dict;
        }
    }
}
