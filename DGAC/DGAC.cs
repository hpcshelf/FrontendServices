﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
//using org.hpcshelf.DGAC.database;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.ports;
using gov.cca;
using gov.cca.ports;
using org.hpcshelf.exception;

namespace org.hpcshelf
{
    namespace DGAC
    {
        public class BackEnd
        {
            public static WorkerObject worker_framework = null;

            public BackEnd()
            {
                Console.WriteLine("DGAC is up and running.");
            }

            #region FRAMEWORK_INSTANTIATION

            public static void releaseFrameworkInstance(string app_name)
            {
                manager_object.Remove(app_name);
            }

            public static AbstractFramework getFrameworkInstance(string app_name)
            {
                Console.WriteLine("getFrameworkInstance 1 !!!");
                ManagerObject obj;
                if (!manager_object.TryGetValue(app_name, out obj))
                    obj = manager_object[app_name] = new ManagerObject();
                return obj;
            }

            static IDictionary<string, ManagerObject> manager_object = new Dictionary<string, ManagerObject>();

            public static AbstractFramework getFrameworkWorkerInstanceLocal()
            {
                WorkerObject wo = null;

                wo = new WorkerObject();
                Console.WriteLine("wo is null ? " + (wo == null));
                wo.sayHi();

                return wo;
            }

            #endregion FRAMEWORK_INSTANTIATION



            #region DEPLOY

            public static string sendToCompile(SourceDeployer deployer, DeployArguments.DeployArgumentsUnitType unitToCompile, byte[] key_file_contents, string userName, string password, string curDir)
			{
				//string cuid = unitToCompile.cuid;
				string library_path = unitToCompile.library_path;
				string moduleName = unitToCompile.moduleName;
				DeployArguments.SourceContentsFile[] sourceContents = unitToCompile.sourceContents;
				string unitName = unitToCompile.unitId;

				Console.Error.WriteLine("sendToCompile " + moduleName);

				string publicKey = sendCompileCommandToWorker(library_path,
															  moduleName,
															  deployer,
															  sourceContents,
															  unitToCompile.references,
															  key_file_contents,
															  userName,
															  password,
															  curDir);

				return publicKey;
			}

			public static void sendToCompile(SourceDeployer deployer, ICollection<LoaderApp.InfoCompile> infoCompile)
			{
				sendToCompile(deployer, infoCompile, null, null, null);
			}

			public static void sendToCompile(SourceDeployer deployer, ICollection<LoaderApp.InfoCompile> infoCompile, string userName, string password, string curDir)
			{
				Console.WriteLine("sendToCompile " + infoCompile);

				foreach (LoaderApp.InfoCompile unitToCompile in infoCompile)
				{
					int id = unitToCompile.id;
					string cuid = unitToCompile.cuid;
					string library_path = unitToCompile.library_path;
					string moduleName = unitToCompile.moduleName;
					Tuple<string, string>[] sourceContents = unitToCompile.sourceContents;
					string unitName = unitToCompile.unitId;

					Console.Error.Write(moduleName + ", ");

					string publicKey = sendCompileCommandToWorker(library_path,
																  moduleName,
																  deployer,
																  sourceContents,
																  unitToCompile.references,
																  userName,
																  password,
																  curDir);
				}
			}

			private static string sendCompileCommandToWorker(string library_path, string moduleName, SourceDeployer deployer, Tuple<string, string>[] sourceContents, string[] refs, string userName, string password, String curDir)
			{
				return deployer.compileClass(library_path, moduleName, sourceContents, refs, userName, password, curDir);
			}

			private static string sendCompileCommandToWorker(string library_path, string moduleName, SourceDeployer deployer, DeployArguments.SourceContentsFile[] sourceContents, string[] refs, byte[] key_file_contents, string userName, string password, String curDir)
			{
				return deployer.compileClass(library_path, moduleName, sourceContents, refs, key_file_contents, userName, password, curDir);
			}

            #endregion DEPLOY



            #region SESSIONS

            internal static IDictionary<string, ISession> open_sessions = new Dictionary<string, ISession>();

            public interface ISession
            {
                string SessionID { get; }
                gov.cca.Services Services { get; }
            }

            private class Session : ISession
            {
                private string session_id;
                private gov.cca.Services services;
                public Session(string session_id, gov.cca.Services services)
                {
                    this.session_id = session_id;
                    this.services = services;
                }
                public string SessionID { get { return session_id; } }
                public gov.cca.Services Services { get { return services; } }
            }

            public static ISession startSession(string session_id)
            {
                try
                {
                    string app_name = session_id.Substring(0, session_id.IndexOf('.'));

                    Console.WriteLine("START SESSION 1 --- {0}", session_id);

                    TypeMapImpl properties = new TypeMapImpl();

                    AbstractFramework frw = getFrameworkInstance(app_name);

                    Console.WriteLine("START SESSION 2 --- {0}", frw == null);


                    Services frwServices = frw.getServices(session_id, "Session", properties);

                    // Builder Service Port
                    frwServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, properties);

                    // GoPort Service Port
                    frwServices.registerUsesPort(Constants.GO_PORT_NAME, Constants.GO_PORT_TYPE, properties);

                    open_sessions[session_id] = new Session(session_id, frwServices);

                    return open_sessions[session_id];
                }
                catch (Exception e)
                {
                    Console.WriteLine("## Exception: " + e.Message);
                    Console.WriteLine("## Inner Exception: " + (e.InnerException == null ? "NULL" : e.InnerException.Message));
                    Console.WriteLine("## Trace: " + e.StackTrace);
                    throw e;
                }
            }

   			public static ICollection<string> getSessions()
			{
				return open_sessions.Keys;
			}

			public static void finishSession(string session_id)
            {
                try
                {
                    gov.cca.Services frwServices = open_sessions[session_id].Services;

                    frwServices.unregisterUsesPort(Constants.BUILDER_SERVICE_PORT_NAME);

                    open_sessions.Remove(session_id);

                    Console.WriteLine("Finishing");
                }
                catch (Exception e)
                {
                    Console.WriteLine("## Exception: " + e.Message);
                    Console.WriteLine("## Inner Exception: " + (e.InnerException == null ? "NULL" : e.InnerException.Message));
                    Console.WriteLine("## Trace: " + e.StackTrace);
                    throw e;
                }
            }


            #endregion SESSIONS


            #region INSTANTIATE

            private static IList<string> binding_sequentials = new List<string>();

            internal static void unbindComponentInstance(ComponentID provider_id, gov.cca.ports.BuilderService bsPort)
            {
                unbindComponentInstance(null, provider_id, bsPort);
            }

            internal static void unbindComponentInstance(ComponentID user_id, ComponentID provider_id, gov.cca.ports.BuilderService bsPort)
            {
                string component_id = provider_id.getInstanceName();

                Console.WriteLine(" BEGIN unbindComponentInstance ... {0}", component_id);

                ConnectionID[] conn_list = bsPort.getConnectionIDs(new ComponentID[] { provider_id });
                foreach (ConnectionID conn in conn_list)
                    if (conn.getProvider() == provider_id && (user_id == null || conn.getUser() == user_id))
                    {
                        Console.WriteLine("begin disconnecting {0} from {1} -- {2}", conn.getUser(), conn.getProvider(), provider_id);
                        bsPort.disconnect(conn, int.MaxValue);
                        Console.WriteLine("end disconnecting {0} from {1} -- {2}", conn.getUser(), conn.getProvider(), provider_id);
                    }

                Console.WriteLine(" END unbindComponentInstance ... {0}", component_id);
            }

            internal static void releaseComponentInstance(ResolveComponentHierarchy ctree, Services frwServices, gov.cca.ports.BuilderService bsPort)
            {
                string component_id = ctree.ID;

                Console.WriteLine("cid_dictionary.Count={0}", cid_dictionary.Count);
                foreach (string k in cid_dictionary.Keys)
                {
                    Console.WriteLine("cid_dictionary[{0}]", k);
                }

                ComponentID s_cid = cid_dictionary[component_id];

                string release_port_name = component_id + "_" + Constants.RELEASE_PORT_NAME /*+ "-" + i*/;

                // CONNECT THE RELEASE PORT OF THE APPLICATION TO THE SESSION DRIVER.
                gov.cca.ComponentID host_cid = frwServices.getComponentID();
                frwServices.registerUsesPort(release_port_name, Constants.RELEASE_PORT_TYPE, new TypeMapImpl());
                // gov.cca.ports.BuilderService bsPort = (gov.cca.ports.BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                ConnectionID conn = bsPort.connect(host_cid, release_port_name, s_cid, Constants.RELEASE_PORT_NAME);
                Console.WriteLine("BEFORE RELEASE PORT " + component_id + " --- " + frwServices.GetHashCode());
                ReleasePort release_port = (ReleasePort)frwServices.getPort(release_port_name);
                Console.WriteLine("AFTER RELEASE PORT " + component_id + " --- " + frwServices.GetHashCode());

                release_port.release1();

                // removeProvidesPorts do ctree ...
                // componente usuário (ctree) deve dar releasePort nas user ports ...

                foreach (KeyValuePair<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> inner in ctree.InnerComponents)
                {
                    string inner_id = inner.Key;

                    Console.WriteLine("releaseComponentInstance - LOOP inner {0} in {1}", inner_id, ctree.ID);

                    ResolveComponentHierarchy ctree_inner_1 = inner.Value[0].Item1;

                    if (ctree.IsPrivate[inner_id])
                    {
                        unbindComponentInstance(ctree_inner_1.CID, bsPort);
                        releaseComponentInstance(ctree_inner_1, frwServices, bsPort);
                    }
                    else
                    {
                        unbindComponentInstance(ctree.CID, ctree_inner_1.CID, bsPort);
                    }
                }

                frwServices.releasePort(release_port_name);
                bsPort.disconnect(conn, int.MaxValue);
                release_port.release2();
                frwServices.unregisterUsesPort(release_port_name);

                Console.WriteLine("BEGIN destroy instance -- {0}", ctree.CID);
                bsPort.destroyInstance(ctree.CID, int.MaxValue);
                Console.WriteLine("END destroy instance -- {0}", ctree.CID);

                cid_dictionary.Remove(ctree.ID);
                if (delayed_host_bindings.ContainsKey(ctree.ID))
                    delayed_host_bindings.Remove(ctree.ID);
              //  if (ctree_cache.ContainsKey(ctree.ID))
              //      ctree_cache.Remove(ctree.ID);
              //  if (delayed_set_host.ContainsKey(ctree.ID))
              //      delayed_set_host.Remove(ctree.ID);
            }


            private static IDictionary<string, ComponentID> cid_dictionary = new Dictionary<string, ComponentID>();

            internal static ComponentID createComponentInstance(ResolveComponentHierarchy ctree,
														int facet_instance,
													    string[] channel_id,
													    int[] channel_facet_instance,
													    string[] channel_facet_address,
                                                        BuilderService bsPort)
            {
                int kind = ctree.Kind;

                Instantiator.UnitMappingType[] unit_mapping = ctree.UnitMapping.Length > 1 ? ctree.UnitMapping[1] : ctree.UnitMapping[0];

                TypeMapImpl properties = new TypeMapImpl();
                properties[Constants.PORT_NAME] = ctree.PortName; //TODO ... ctree.getPortName(***) ????
                properties[Constants.UNIT_MAPPING] = unit_mapping;
                properties[Constants.FACET_INSTANCE] = facet_instance;
                properties[Constants.IGNORE] = ctree.FacetList[facet_instance].Count == 0;
                properties[Constants.COMPONENT_KIND] = kind;
				properties[Constants.CHANNEL_ID] = channel_id.Clone();
				properties[Constants.CHANNEL_FACET_INSTANCE] = channel_facet_instance.Clone();
				properties[Constants.CHANNEL_FACET_ADDRESS] = channel_facet_address.Clone();
				properties[Constants.COMPONENT_KIND] = kind;

                if (kind == Constants.KIND_BINDING)
                {
                    if (!binding_sequentials.Contains(ctree.ID))
                        binding_sequentials.Add(ctree.ID);
                    properties[Constants.BINDING_SEQUENTIAL] = binding_sequentials.IndexOf(ctree.ID);
                }

                int[] facet_topology = null;
                calculate_inner_facet_topology(unit_mapping, ref facet_topology);
                properties[Constants.FACET_TOPOLOGY] = facet_topology;

                string component = ctree.ComponentChoice.Length > 1 ? ctree.ComponentChoice[1] : ctree.ComponentChoice[0];

                Console.WriteLine("createComponentInstance - BEFORE createInstance {0}", ctree.ID);
                ctree.CID = (ManagerComponentID)bsPort.createInstance(ctree.ID, component, properties);
                Console.WriteLine("createComponentInstance - AFTER createInstance {0}", ctree.ID);

                cid_dictionary[ctree.ID] = ctree.CID;

                Console.WriteLine("cid_dictionary[{0}]={1}", ctree.ID, ctree.CID);

                int[] facets_host = ctree.FacetList[0].ToArray();

                foreach (KeyValuePair<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> inner in ctree.InnerComponents)
                {
                    string inner_id = inner.Key;

                    ResolveComponentHierarchy ctree_inner_0 = inner.Value[0].Item1;
                    ComponentID inner_cid;
                    if (ctree.IsPrivate[inner_id] && inner.Value.Count > 0 && !(inner.Value.Count > 0 && (inner.Value[0].Item1.Kind == Constants.KIND_QUALIFIER || inner.Value[0].Item1.Kind == Constants.KIND_CERTIFIER)))
                        inner_cid = createComponentInstance(ctree_inner_0, facet_instance, channel_id, channel_facet_instance, channel_facet_address, bsPort);
                    else
                        inner_cid = cid_dictionary[ctree_inner_0.ID];

                    bindComponentInstance(ctree_inner_0, facets_host, bsPort);
                    Console.WriteLine("INSTANTIATED " + inner_cid);                
                }

				if (delayed_host_bindings.ContainsKey(ctree.ID))
				{
					foreach (Thread t in delayed_host_bindings[ctree.ID])
					{
						t.Start();
						t.Join();
					}
                    delayed_host_bindings.Remove(ctree.ID);
                }

                return ctree.CID;
            }

			private static IDictionary<string, IList<Thread>> delayed_host_bindings = new Dictionary<string, IList<Thread>>();

			private static void calculate_inner_facet_topology(Instantiator.UnitMappingType[] unit_mapping_inner, ref int[] facet_topology)
			{
				IList<Tuple<int, int>> facet_topology_list = new List<Tuple<int, int>>();

				int max = 0;
				foreach (Instantiator.UnitMappingType um in unit_mapping_inner)
					if (um.facet_instance >= 0)
					{
						facet_topology_list.Add(new Tuple<int, int>(um.facet_instance, um.facet));
						max = um.facet_instance > max ? um.facet_instance : max;
					}

				facet_topology = new int[max + 1];
				for (int i = 0; i < facet_topology.Length; i++)
					facet_topology[i] = -1;

				foreach (Tuple<int, int> p in facet_topology_list)
					facet_topology[p.Item1] = p.Item2;
			}

            internal static void bindComponentInstance(ResolveComponentHierarchy ctree, int[] facets_host, gov.cca.ports.BuilderService bsPort)
			{
                Console.WriteLine("bindComponentInstance 2 --- start ... {0} delay={1} --- {2}", ctree.ID, ctree.InnerComponents.Count, facets_host == null);
				Console.WriteLine("cid_dictionary[{0}] ? {1} {2}", ctree.ID, cid_dictionary.ContainsKey(ctree.ID), ctree.ComponentChoice.Length > 1 ? ctree.ComponentChoice[1] : ctree.ComponentChoice[0]);

                bindAsProvider(ctree, bsPort);
			}

            private static void bindAsProvider(ResolveComponentHierarchy ctree, gov.cca.ports.BuilderService bsPort)
            {
                ComponentID provider_id = cid_dictionary[ctree.ID];
                if (provider_id is ManagerComponentID)
                {
                    IList<Tuple<string, ResolveComponentHierarchy>> hosts = ctree.HostComponents; //ctree_inner.checkHosts();

                    Console.WriteLine("BEFORE ITERATION HOST --- {0}", hosts.Count);
                    foreach (Tuple<string, ResolveComponentHierarchy> ctree_host_pair in hosts)
                    {
                        ResolveComponentHierarchy ctree_host = ctree_host_pair.Item2;
                        Console.WriteLine("ITERATION HOST NEW --- {0} {1}", ctree_host.ID, cid_dictionary.ContainsKey(ctree_host.ID));
                        Thread t_bind_host = new Thread(() =>
                        {
                            ComponentID user_id = cid_dictionary[ctree_host.ID];
                            Console.WriteLine("ctree_host.Key = {0}", ctree_host_pair.Item1);
                            ConnectionID[] conn_list = bsPort.getConnectionIDs(new ComponentID[1] { provider_id });
                            bool connected = false;
                            foreach (ConnectionID conn in conn_list)
                            {
                                Console.Write("TESTING CONNECTION -- {0} (user) to {1} (provider) ? ", user_id.getInstanceName(), provider_id.getInstanceName());
                                if (conn.getUser().Equals(user_id))
                                {
                                    Console.WriteLine(" TRUE");
                                    connected = true;
                                }
                                else
                                    Console.WriteLine(" FALSE");
                            }
                            if (!connected)
                                bsPort.connect(user_id, ctree_host_pair.Item1, provider_id, Constants.DEFAULT_PROVIDES_PORT_IMPLEMENTS);
                        });

                        // THE HOST COMPONENT HAS BEEN INSTANTIATED ?
                        // TODO: there is a problem here. The host component only appear as a host when it is instantiated. 
                        //       So, the test will allways return "true".
                        if (cid_dictionary.ContainsKey(ctree_host.ID))
                        {
                            // IF YES, BIND !
                            t_bind_host.Start();
                            t_bind_host.Join();
                        }
                        else
                        {
                            // IF NOT, CREATE A SUSPENSION.
                            Console.WriteLine("HOST NOT FOUND {0} -- top level ?", ctree_host.ID);
                            if (!delayed_host_bindings.ContainsKey(ctree_host.ID))
                                delayed_host_bindings[ctree_host.ID] = new List<Thread>();

                            delayed_host_bindings[ctree_host.ID].Add(t_bind_host);
                        }
                    }
                    Console.WriteLine("AFTER ITERATION HOST --- #{0}", ctree.GetHashCode());
                    //if (ctree.IsPrivate[ctree_inner.getPortName(ctree.ID)])
                }
                else
                    Console.WriteLine("UNEXPECTED -- provider_id ({0}) is not ManagerComponentID -- bindComponentInstance2", ctree.ID);
            }


            public static string tryReleaseGoPort(gov.cca.Services frwServices, string cRef)
            {
                string go_port_name = null;
                Tuple<gov.cca.ports.GoPort, ConnectionID> go_port_and_conn = null;

                Console.WriteLine("go_port_cache -- count={0}", go_port_cache.Count);
                foreach (string k in go_port_cache.Keys)
                    Console.WriteLine("go_port_cache -- k = {0}", k);

                if (go_port_cache.TryGetValue(cRef, out go_port_and_conn))
                {
                    Console.WriteLine("BEGIN tryReleaseGoPort {0}", cRef);
                    go_port_name = cRef + "_" + Constants.GO_PORT_NAME /*+ "-" + i*/;
                    gov.cca.ports.BuilderService bsPort = (gov.cca.ports.BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                    frwServices.releasePort(go_port_name);
                    Console.WriteLine("END tryReleaseGoPort {0}", cRef);
                    go_port_cache.Remove(cRef);
                }
                return go_port_name;
            }


            public static void copy_unit_mapping_to_dictionary(Instantiator.UnitMappingType[] unit_mapping,
                                                    out IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict)
            {
                unit_mapping_dict = new Dictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>>();
                foreach (Instantiator.UnitMappingType unit_node in unit_mapping)
                {
                    string key = unit_node.unit_id;
                    IList<Tuple<int, int[], int, string, string, string[]>> list = null;
                    if (!unit_mapping_dict.TryGetValue(key, out list))
                    {
                        list = new List<Tuple<int, int[], int, string, string, string[]>>();
                        unit_mapping_dict.Add(key, list);
                    }

                    list.Add(new Tuple<int, int[], int, string, string, string[]>(unit_node.facet_instance, unit_node.node, unit_node.facet, unit_node.assembly_string, unit_node.class_name, unit_node.port_name));

                }
            }

            #endregion INSTANTIATE

            #region RUN

            private static IDictionary<string, Tuple<gov.cca.ports.GoPort, ConnectionID>> go_port_cache = new Dictionary<string, Tuple<gov.cca.ports.GoPort, ConnectionID>>();

            public static void runSolutionComponent(Services frwServices, string cRef)
			{
                Tuple<GoPort, ConnectionID> go_port_and_conn = null;

                Console.WriteLine("runSolutionComponent {0} -- {1}", cRef, go_port_cache.ContainsKey(cRef));

                if (!go_port_cache.TryGetValue(cRef, out go_port_and_conn))
                {
                    if (!cid_dictionary.ContainsKey(cRef))
                        throw new UninstantiatedPlatformException(cRef);

                    ComponentID s_cid = cid_dictionary[cRef];

                    string go_port_name = cRef + "_" + Constants.GO_PORT_NAME /*+ "-" + i*/;

                    // CONNECT THE GO PORT OF THE APPLICATION TO THE SESSION DRIVER.
                    ComponentID host_cid = frwServices.getComponentID();
                    frwServices.registerUsesPort(go_port_name, Constants.GO_PORT_TYPE, new TypeMapImpl());
                    BuilderService bsPort = (BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                    ConnectionID conn = bsPort.connect(host_cid, go_port_name, s_cid, Constants.GO_PORT_NAME);
                    Console.WriteLine("BEFORE GO PORT " + cRef + " --- " + frwServices.GetHashCode());
                    GoPort go_port = (GoPort)frwServices.getPort(go_port_name);
                    Console.WriteLine("AFTER GO PORT " + cRef + " --- " + frwServices.GetHashCode());

                    go_port_and_conn = go_port_cache[cRef] = new Tuple<GoPort, ConnectionID>(go_port, conn);
                }

                go_port_and_conn.Item1.go();
            }

            #endregion RUN



            public interface BaseComponentHierarchy
            {
                string ID { get; }
                IDictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> InnerComponents { get; }

                void accept(BaseComponentHierarchyVisitor visitor);
                void addInnerComponent(string inner_id, Tuple<ResolveComponentHierarchy, int[]> inner_component);
                void addInnerComponent(string inner_id, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component);
			} 
 
            public interface ResolveComponentHierarchy : BaseComponentHierarchy
            {
                string[] ComponentChoice { get; }
                ComponentID CID { get; set; }
 
                IList<Tuple<string, ResolveComponentHierarchy>> HostComponents { get; }
                IDictionary<string, bool> IsPrivate { get; }
                int Kind { get; set; }
                Instantiator.UnitMappingType[][] UnitMapping { get; set; }
                IList<int>[] FacetList { get; set; }
                string PortName { get; set; }
                bool isInFacet(int facet_instance);

                void addHostComponent(string id_inner_host, ResolveComponentHierarchy host_component);

				void addInnerComponent(string inner_id, bool is_private, Tuple<ResolveComponentHierarchy, int[]> inner_component);
				void addInnerComponent(string inner_id, bool is_private, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component);

                IList<Tuple<string, ResolveComponentHierarchy>> checkHosts(int[] facet_list);

                void removeInnerComponent(string id_inner_removed);
            }

            public class BaseComponentHierarchyImpl : BaseComponentHierarchy
            {
                protected string id;
                public string ID { get { return this.id; } set { this.id = value; } }

                protected string library_path;
                public string LibraryPath { get { return this.library_path; } set { this.library_path = value; } }

                private IDictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> inner_components = new Dictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>>();
                public IDictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> InnerComponents => this.inner_components;

                public BaseComponentHierarchyImpl(string id, string library_path)
                {
                    this.ID = id;
                    this.LibraryPath = library_path;
                }

                public BaseComponentHierarchyImpl(string id)
                {
                    this.ID = id;
                }

                public void addInnerComponent(string inner_id, Tuple<ResolveComponentHierarchy, int[]> inner_component)
                {
                    if (!InnerComponents.ContainsKey(inner_id))
                        inner_components[inner_id] = new List<Tuple<ResolveComponentHierarchy, int[]>>();
                    inner_components[inner_id].Add(inner_component);
                }
                            
                public void addInnerComponent(string inner_id, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component)
                {
                    IList<Tuple<ResolveComponentHierarchy, int[]>> l = new List<Tuple<ResolveComponentHierarchy, int[]>>(inner_component);
                    if (!InnerComponents.ContainsKey(inner_id))
                        inner_components[inner_id] = l;
                    else
                        foreach (Tuple<ResolveComponentHierarchy, int[]> e in l)
                            inner_components[inner_id].Add(e);
                }

                public void accept(BaseComponentHierarchyVisitor visitor)
                {
                    visitor.visit(this);
                }
            }

            public class ResolveComponentHierarchySystemImpl : ResolveComponentHierarchyImpl
            {
                public ResolveComponentHierarchySystemImpl(string id) : base(id)
                {
                }

                public ResolveComponentHierarchySystemImpl(string id, string library_path) : base(id, library_path)
                {
                }
   			}

            public class ResolveComponentHierarchyImpl : BaseComponentHierarchyImpl, ResolveComponentHierarchy
            {
                public ResolveComponentHierarchyImpl(string id, string library_path) : base(id, library_path)
                {
                }

                public ResolveComponentHierarchyImpl(string id) : base(id)
                {
                }

                private string[] component_choice;
                public string[] ComponentChoice { get { return this.component_choice; } set { this.component_choice = value; } }

                public void addInnerComponent(string inner_id, bool is_private, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component)
                {
                    base.addInnerComponent(inner_id, inner_component);
                    if (!this.IsPrivate.ContainsKey(inner_id))
                        this.is_private[inner_id] = is_private;
                }

                public void addInnerComponent(string inner_id, bool is_private, Tuple<ResolveComponentHierarchy, int[]> inner_component)
                {
                    base.addInnerComponent(id, inner_component);
                    if (!this.IsPrivate.ContainsKey(inner_id))
                        this.is_private[inner_id] = is_private;
                }

                private IDictionary<string, ResolveComponentHierarchy> host_components_by_id = new Dictionary<string, ResolveComponentHierarchy>();
                private IList<Tuple<string, ResolveComponentHierarchy>> host_components = new List<Tuple<string, ResolveComponentHierarchy>>();
                public IList<Tuple<string, ResolveComponentHierarchy>> HostComponents => this.host_components;

                public void addHostComponent(string id_inner_host, ResolveComponentHierarchy host_component)
                {
                    if (!host_components_by_id.ContainsKey(host_component.ID))
                    {
                        host_components.Add(new Tuple<string, ResolveComponentHierarchy>(id_inner_host, host_component));
                        host_components_by_id[host_component.ID] = host_component;
                    }
                }


				private IDictionary<string, bool> is_private = new Dictionary<string, bool>();
                public IDictionary<string, bool> IsPrivate { get { return is_private; } }

                private bool is_qualifier = false;
                public bool IsQualifier { get { return is_qualifier; } set { is_qualifier = value; } }
                				
                private int kind = -1;
                public int Kind { get { return kind; } set { kind = value; } }

                private Instantiator.UnitMappingType[][] unit_mapping = null;
                public Instantiator.UnitMappingType[][] UnitMapping { get { return unit_mapping; } set { unit_mapping = value; } }

                private IList<int>[] facet_list = null;
                public IList<int>[] FacetList { get { return facet_list; } set { facet_list = value; } }

                private ComponentID cid = null;
                public ComponentID CID { get { return cid; } set { cid = value; } }

                private string port_name = null;
                public string PortName { get { return port_name; } set { port_name = value; } }

                public void accept(ResolveComponentHierarchyVisitor visitor)
                {
                    visitor.visit(this);
                }

                public bool isInFacet(int facet_instance)
                {
                    return facet_instance < this.FacetList.Length && this.FacetList[facet_instance].Count > 0;
                }








                public IList<Tuple<string, ResolveComponentHierarchy>> checkHosts(int[] facet_list)
                {
                    IList<Tuple<string, ResolveComponentHierarchy>> result_hosts = new List<Tuple<string, ResolveComponentHierarchy>>();

                    foreach (Tuple<string, ResolveComponentHierarchy> ctree_host in this.HostComponents)
                    {
//						Console.WriteLine("ctree_host.Item1 = {0}; ctree_host.Item2.ID = {1}", ctree_host.Item1, ctree_host.Item2.ID);
//						if (ctree_host.Item2.ID.Equals("teste.workflow"))
  //                      {
    //                        foreach (string k in ctree_host.Item2.InnerComponents.Keys)
      //                          Console.Write("{0}, ", k);
        //                    Console.WriteLine();
          //              }

						IList<Tuple<ResolveComponentHierarchy, int[]>> ctree_inner_host_list = ctree_host.Item2.InnerComponents[ctree_host.Item1];

                        foreach (Tuple<ResolveComponentHierarchy, int[]> ctree_inner_host_ in ctree_inner_host_list)
                        {
                            ResolveComponentHierarchy ctree_inner_host = ctree_inner_host_.Item1;
                            int[] facets = ctree_inner_host_.Item2;

                            if (ctree_inner_host == this && facets.Intersect(facet_list).Count() > 0) 
                                result_hosts.Add(new Tuple<string, ResolveComponentHierarchy>(ctree_host.Item1, ctree_host.Item2));
						}
					}

					// Remove "non-leaf" hosts ...

                   /* IList<Tuple<string, ResolveComponentHierarchy>> result_hosts_copy = new List<Tuple<string, ResolveComponentHierarchy>>(result_hosts);

                    bool next = false;
                    foreach (Tuple<string, ResolveComponentHierarchy> host_2 in result_hosts)
                    {
                        next = false;
                        foreach (Tuple<string, ResolveComponentHierarchy> host_1 in result_hosts)
                        {
                            foreach (Tuple<string, ResolveComponentHierarchy> host_host_1 in host_1.Item2.HostComponents)
                            {
                                if (host_host_1.Item2 == host_2.Item2)
                                {
                                    result_hosts_copy.Remove(host_2);
                                    next = true;
                                    break;
                                }
                            }
                        }
                        if (next)
                            break;
                    }*/

					return result_hosts;
                }

                public void removeInnerComponent(string id_inner_removed)
                {
                    this.IsPrivate.Remove(id_inner_removed);
                    foreach(Tuple<ResolveComponentHierarchy, int[]> t in this.InnerComponents[id_inner_removed])
                    {
                        ResolveComponentHierarchy ctree = t.Item1;
                        foreach (Tuple<string,ResolveComponentHierarchy> tt in ctree.HostComponents)
                        {
                            ResolveComponentHierarchy ctree_host = tt.Item2;
                            ctree_host.InnerComponents.Remove(tt.Item1);
                        }
                    }
                }



			}

            public interface BaseComponentHierarchyVisitor
            {
                object visit(BaseComponentHierarchy ctree);
            }

            public interface ResolveComponentHierarchyVisitor
            {
                object visit(ResolveComponentHierarchy ctree);
            }



        } //BackEnd




        public class PortUsageManager
        {
            bool fetched = false;

            private IDictionary<string, int> portFetches = new Dictionary<string, int>();
            private IDictionary<string, int> portReleases = new Dictionary<string, int>();

            private IDictionary<string, bool> portFetched = new Dictionary<string, bool>();

            public bool isFetched(string portname)
            {
                //int count = 0;
                //return portFetches.TryGetValue(portname, out count) && count > 0;
                Console.WriteLine("isFetched {0} -- {1} -- {2} -- {3}", portname, portFetches.ContainsKey(portname) ? portFetches[portname] : 0, portReleases.ContainsKey(portname) ? portReleases[portname] : 0, portFetched.ContainsKey(portname) ? portFetched[portname] : false);
                return portFetched.ContainsKey(portname) && portFetched[portname];
            }

            public bool isReleased(string portname)
            {
                //int count = 0;
                //return portReleases.TryGetValue(portname, out count) && count > 0;
                Console.WriteLine("isReleased {0} -- {1} -- {2} -- {3}", portname, portFetches.ContainsKey(portname) ? portFetches[portname] : 0, portReleases.ContainsKey(portname) ? portReleases[portname] : 0, portFetched.ContainsKey(portname) ? portFetched[portname] : false);
                return !portFetched.ContainsKey(portname) || !portFetched[portname];
            }

            public void addPortFetch(string portName)
            {
                if (!portFetches.ContainsKey(portName))
                    portFetches[portName] = 0;

                if (!portFetched.ContainsKey(portName))
                    portFetched[portName] = false;

                portFetches[portName]++;
                portFetched[portName] = true;

                // int count_fetch = 0;
                // if (portFetches.TryGetValue(portName, out count_fetch))
                //    portFetches.Remove(portName);

                // int count_release = 0;
                // if (portReleases.TryGetValue(portName, out count_release))
                //    portReleases.Remove(portName);

                // if (count_release > 0)
                // {
                //    Console.Error.WriteLine("Port " + portName + " was released (release count=" + count_release + ")");
                //    throw new CCAExceptionImpl(CCAExceptionType.PortNotInUse);
                // }

                // portFetches.Add(portName, count_fetch + 1);
            }

            public void addPortRelease(string portName)
            {
                if (!portReleases.ContainsKey(portName))
                    portReleases[portName] = 0;

                if (!portFetched.ContainsKey(portName))
                    portFetched[portName] = false;

                portReleases[portName]++;
                portFetched[portName] = false;

                /*int count_release = 0;
                if (portReleases.TryGetValue(portName, out count_release))
                    portReleases.Remove(portName);

                if (count_release > 0)
                {
                    Console.Error.WriteLine("Port " + portName + " still in use (release count = " + count_release + ")");
                    throw new CCAExceptionImpl(CCAExceptionType.UsesPortNotReleased);
                }

                portReleases.Add(portName, count_release + 1);
                */
            }

            public void resetPort(string portName)
            {
                if (portReleases.ContainsKey(portName))
                    portReleases.Remove(portName);
                if (portFetches.ContainsKey(portName))
                    portFetches.Remove(portName);
                if (portFetched.ContainsKey(portName))
                    portFetched[portName] = false;
            }
        }

        [Serializable]
		public class CertificationFailedException : Exception
		{
			public CertificationFailedException()
			{
			}

			public CertificationFailedException(string message) : base(message)
			{
			}

			public CertificationFailedException(string message, Exception innerException) : base(message, innerException)
			{
			}

			protected CertificationFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
			{
			}
		}


	}//namespace
}




