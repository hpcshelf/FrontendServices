﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace org.hpcshelf.DGAC
{
    public class LoaderApp
    {

/*	public static ComponentReference.ComponentReference loadComponentReference(string contents)
		{
			// Declare an object variable of the type to be deserialized.
			ComponentReference.ComponentReference i = null;
			FileStream fs = null;
			try
			{
				// Create an instance of the XmlSerializer specifying type and namespace.
				XmlSerializer serializer = new XmlSerializer(typeof(ComponentReference.ComponentReference));

				string filename = Path.GetTempFileName();
				File.WriteAllText(filename, contents);

				// A FileStream is needed to read the XML document.
				fs = new FileStream(filename, FileMode.OpenOrCreate);

				XmlReader reader =  new XmlTextReader(fs);

				// Use the Deserialize method to restore the object's state.
				i = (ComponentReference.ComponentReference)serializer.Deserialize(reader);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
			finally
			{
				if (fs != null)
					fs.Close();
			}

			return i;

		}
        */

        public static ComponentType DeserializeObject(string filename)
        {
            // Declare an object variable of the type to be deserialized.
            ComponentType i = null;
            FileStream fs = null;
            try
            {
                Console.WriteLine("Reading with XmlReader " + filename);

                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new XmlSerializer(typeof(ComponentType));

                // A FileStream is needed to read the XML document.
                fs = new FileStream(filename, FileMode.Open);

                XmlReader reader = new XmlTextReader(fs);

                // Use the Deserialize method to restore the object's state.
                i = (ComponentType)serializer.Deserialize(reader);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }

            return i;

        }

		public static T deserialize<T>(string contents)
		{
			string filename = Path.GetTempFileName ();
            File.WriteAllText(filename, contents);

			// Declare an object variable of the type to be deserialized.
			T i = default(T);
			FileStream fs = null;
			try
			{
				Console.WriteLine("Reading with XmlReader " + filename);

				// Create an instance of the XmlSerializer specifying type and namespace.
				XmlSerializer serializer = new XmlSerializer(typeof(T));

				// A FileStream is needed to read the XML document.
				fs = new FileStream(filename, FileMode.Open);

				XmlReader reader = new XmlTextReader(fs);

				// Use the Deserialize method to restore the object's state.
				i = (T)serializer.Deserialize(reader);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
			finally
			{
				if (fs != null)
					fs.Close();
			}

			return i;

		}

		public static Instantiator.InstanceType DeserializeInstantiator(string filename)
        {
            // Declare an object variable of the type to be deserialized.
            Instantiator.InstanceType i = null;
            FileStream fs = null;
            try
            {
                Console.WriteLine("Reading with XmlReader");

                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new XmlSerializer(typeof(Instantiator.InstanceType));

                // A FileStream is needed to read the XML document.
                fs = new FileStream(filename, FileMode.Open);

                XmlReader reader = new XmlTextReader(fs);

                // Use the Deserialize method to restore the object's state.
                i = (Instantiator.InstanceType)serializer.Deserialize(reader);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }

            return i;

        }



        public static string serialize<T>(T instance)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));

			string filename = Path.GetTempFileName ();

			FileStream fs = new FileStream(filename, FileMode.Open);

			XmlWriter writer = new XmlTextWriter(fs, null);

			serializer.Serialize(writer, instance);

			fs.Close();

			return File.ReadAllText(filename);	

		}



		//receives id_concrete and id inner wich belongs for that inner
		//returns a impl of the inner
		//return -1 if the impl doesnt exist    






        public struct InfoCompile
        {
			public string moduleName;
            public string[] references;
            public string unitId;
			public Tuple<string,string>[] sourceContents;
            public string cuid;
            public string library_path;
            public int id;
            public byte[] strong_key_file_contents;
        }




	}//class

}//namespace
