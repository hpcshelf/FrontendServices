﻿using org.hpcshelf.DGAC;
using org.hpcshelf.provenance;

namespace org.hpcshelf.core
{
    public interface ICoreServices 
	{
        void   resolve        (string arch_ref);
		void   resolve_system (string arch_ref);
		string deploy         (string arch_ref);
		string instantiate    (string arch_ref);
		void   run            (string arch_ref);
        void   release        (string arch_ref);
		void   certify        (string arch_ref);

	}
}
