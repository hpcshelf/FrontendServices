﻿// /home/jefferson/projetos/appexample/appexample/main/TestMain.cs created with MonoDevelop
// User: jefferson at 15:03 12/5/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using org.hpcshelf.unit;
using gov.cca.ports;

namespace org.hpcshelf.kinds
{

    public interface ISAFeApplicationKind : IUnit
	{	
	}
	
	//[Serializable]
	public abstract class SAFeApplication : Unit, ISAFeApplicationKind {
	   
	}		
}
