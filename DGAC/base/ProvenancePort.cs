﻿/*=============================================================
(c) all rights reserved
================================================================*/

using org.hpcshelf.provenance;
using gov.cca;

namespace org.hpcshelf.ports
{
    public interface ProvenancePort : IProvenance, Port
    {
		int ProvenanceLevel { get; }
		IProvenanceTracer ProvenanceTracer { set; }
		string SystemRef { get; }
	}

	public interface IProvenance
	{
		void start(int level);
		string confirm();
		void cancel();
	}

}
