﻿/*=============================================================
(c) all rights reserved
================================================================*/

using gov.cca;
using org.hpcshelf.unit;
using System;

namespace org.hpcshelf.ports
{

    public interface AutomaticSlicesPort : Port
    {
        void create_slices();
      //  void initialize_slices();
      //  void post_initialize_slices();
        void destroy_slices();
    }

}
