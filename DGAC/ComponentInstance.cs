﻿using System;
using System.Collections.Generic;
using org.hpcshelf.DGAC.utils;
using gov.cca;
using gov.cca.ports;
using static org.hpcshelf.DGAC.BackEnd;

namespace DGAC
{
    public interface ISystem
    {
		string SessionID { get; }
		void newComponentInstance(string id, ResolveComponentHierarchy ctree);
        void releaseComponentInstance(string id);

        string[] ChannelID { get; set; }
        int[] ChannelFacetInstance { get; set; }
        string[] ChannelFacetAddress { get; set; }
    }

    public class ParallelComputingSystem : ISystem
    {
        private IDictionary<string, IComponentInstance> component_instances = new Dictionary<string, IComponentInstance>();
        private int facet_instance;
        private ISession session;
        private string session_id_string;

        internal ParallelComputingSystem(string session_id_string, int facet_instance, ISession session)
        {
            this.session_id_string = session_id_string;
            this.facet_instance = facet_instance;
            this.session = session;
        }

        public static IDictionary<object, ISystem> SystemInstance = new Dictionary<object, ISystem>();

        public string SessionID { get { return session_id_string; } }

        private string[] channel_id;
        public string[] ChannelID { get { return channel_id; } set { channel_id = value; }}

        private int[] channel_facet_instance;
        public int[] ChannelFacetInstance { get { return channel_facet_instance; } set { channel_facet_instance = value; }}

        private string[] channel_facet_address;
        public string[] ChannelFacetAddress { get { return channel_facet_address; } set { channel_facet_address = value; }}

        public static ISession newSystem(string session_id_string, int facet_instance)
        {
            object handle = new object();

            ISession session = startSession(session_id_string);

            ParallelComputingSystem system = new ParallelComputingSystem(session_id_string, facet_instance, session);

            SystemInstance[session] = system;

            return session;
        }

        public void newComponentInstance(string id, ResolveComponentHierarchy ctree)
        {
            component_instances[id] = new ComponentInstance(ctree, facet_instance, this);

            Console.WriteLine("newComponentInstance -- component_instances.Count={0}", component_instances.Count);
            foreach (string k in component_instances.Keys)
                Console.WriteLine("newComponentInstance -- component_instances[{0}]", k);
        }

        public void releaseComponentInstance(string id)
        {

            Console.WriteLine("releaseComponentInstance -- component_instances.Count={0}", component_instances.Count);
            foreach (string k in component_instances.Keys)
                Console.WriteLine("releaseComponentInstance -- component_instances[{0}]", k);

            component_instances[id].release();
            component_instances.Remove(id);
        }
    }


    interface IComponentInstance
    {
        void release();
    }

    class ComponentInstance : IComponentInstance
    {
        private ISystem system;
        private ResolveComponentHierarchy ctree;
        BuilderService bsPort;
        Services frwServices;

        internal ComponentInstance(ResolveComponentHierarchy ctree, int facet_instance, ISystem system)
        {
            this.system = system;
            this.ctree = ctree;

            ISession session = open_sessions[system.SessionID];
            frwServices = session.Services;

            bsPort = (BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

            ComponentID inner_cid = createComponentInstance(ctree, facet_instance, system.ChannelID, system.ChannelFacetInstance, system.ChannelFacetAddress, bsPort);
            bindComponentInstance(ctree, new int[1] {0}, bsPort);
		}

        public void release()
        {
            string go_port_name = tryReleaseGoPort(frwServices, ctree.ID);
            unbindComponentInstance(ctree.CID, bsPort);
            releaseComponentInstance(ctree, frwServices, bsPort);
            if (go_port_name != null)
                frwServices.unregisterUsesPort(go_port_name);
        }
    }
}
