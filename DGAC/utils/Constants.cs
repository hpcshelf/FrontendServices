﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace org.hpcshelf.DGAC.utils
{
    public class MPIWorkerMessagingConstants
    {
        public const int DEFAULT_TAG = 999;
        public const int CREATE_INSTANCE = 0;
    }

    public enum EnvironmentPortType { user, provider, platform_user, platform_provider};

    public class Constants
    {
        public const string PLATFORM_SOURCE_ID = "platform_data_source";

        public const string PLATFORM_SAFE_ID = "platform_SAFe";
        public const string WORKFLOW_COMPONENT_ID = "workflow";
        public const string APPLICATION_COMPONENT_ID = "application";
        public const string SOURCE_COMPONENT_ID = "source";
        public const string CORE_CERTIFICATION_CONTROLLER_COMPONENT_ID = "core.certification_launcher";
        public const string CERTIFICATION_WORKFLOW_COMPONENT_ID = "certification_workflow";

        public static string SWIRLS_HOME
        {
            get
            {
                string path = Environment.GetEnvironmentVariable("Swirls_HOME");
                if (path == null)
                {
                    Console.WriteLine("Swirls_HOME not set. Using the current directory.");
                    path = Environment.CurrentDirectory;
                }
                return path;
            }
        }

        public static string PATH_TEMP_WORKER
        {
            get
            {
                string file_temp_worker = Environment.GetEnvironmentVariable("Swirls_HOME");

                if (file_temp_worker == null)
                    file_temp_worker = Path.Combine(HOME_PATH, ".hpcshelf_temp");
                else
                    file_temp_worker = Path.Combine(file_temp_worker, "temp");

                if (file_temp_worker != null)
                    if (!Directory.Exists(file_temp_worker))
                    {
                        Directory.CreateDirectory(file_temp_worker);
                    }

                return file_temp_worker;
            }
        }

                public static string PROPERTIES_FILE = getPropertiesFilePath(); 

        private static string getPropertiesFilePath()
        {
            Trace.Write("LOADING PROPERTIES FILE: ---- " + HOME_PATH);
            string properties_file_path = getArgVal("--properties");
            Console.WriteLine(properties_file_path);
            if (properties_file_path == null)
            {
               properties_file_path = Environment.GetEnvironmentVariable("HPCShelf_PROPERTIES");
               if (properties_file_path == null)
                    properties_file_path = Path.Combine(HOME_PATH, "hpcshelf.properties");
            }
            Console.WriteLine(properties_file_path);
            return properties_file_path;
        }

        private static int base_binding_facet_port = -1;
        public static int BASE_BINDING_FACET_PORT { 
            get 
            {  
                if (base_binding_facet_port == -1) 
                {
                    string v = Environment.GetEnvironmentVariable ("BASE_BINDING_FACET_PORT");
                    base_binding_facet_port = v != null ? int.Parse (v) : 5000;
                }
                return base_binding_facet_port;
            }
        }

        private static int NEXT_FREE_BINDING_FACET_PORT = BASE_BINDING_FACET_PORT;

        public static Object binding_facet_port_lock = new Object();

        public static  int FREE_BINDING_FACET_PORT
        {
            get 
            {
                int result = 0;
                lock (binding_facet_port_lock) 
                {
                    result = NEXT_FREE_BINDING_FACET_PORT ++;
                }     
                return result;
            }
        }

        private static string home_path = null;

        public static string HOME_PATH { 
            get 
            {  
                if (home_path == null)
                    home_path = (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
                                      ? Environment.GetEnvironmentVariable("HOME") : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
                return home_path;
            }
        }

        public static string SYSTEM_COMPONENT_PATH = "SYSTEM_COMPONENT_PATH";

        public static string DIRECTORY_NAME_BINDING_WEB_SERVICES;

        public static int WORKER_PORT = 4865;
        public static int MANAGER_PORT = 4864;
        public static string WORKER_SERVICE_NAME = "WorkerService";
        public static string MANAGER_SERVICE_NAME = "ManagerService";
        public const string BUILDER_SERVICE_PORT_NAME = "builder_service";
        public const string BUILDER_SERVICE_PORT_TYPE = "gov.cca.BuilderService";
        public const string REGISTRY_PORT_NAME = "registry";
        public const string REGISTRY_PORT_TYPE = "gov.cca.ports.ServiceRegistry";
        public static string MANAGER_PORT_NAME = "ManagerHost";
        public const int DLL_OUT = 0;
        public const int EXE_OUT = 1;

//        public static string IP_ADDRESS_BINDING_ROOT = FileUtil.readConstant ("ip_address_binding_root","0.0.0.0");
        public static string SITE_NAME = FileUtil.readConstant("site_name", "unnamed");
        public static string SITE_URL = FileUtil.readConstant("site_url");
        //public static string PATH_DGAC = FileUtil.readConstant("path_dgac");
        public static string UNIT_PACKAGE_PATH = FileUtil.readConstant("unit_package_path"); // HASH_UNIT_PACKAGE";
       // public static string PATH_BIN = FileUtil.readConstant("path_bin");
       // public static string cs_compiler = FileUtil.readConstant("cs_compiler","dmcs"); // "mcs"
        public static string cs_compiler_flags = FileUtil.readConstant("cs_compiler_flags"); // "mcs"
       // public static string cli_runtime = FileUtil.readConstant("cli_runtime"); // "mono"
      //  public static string key_generator = FileUtil.readConstant("key_generator");
      //  public static string gac_util = FileUtil.readConstant("gac_util");
        public static string mpi_run = FileUtil.readConstant("mpi_run");
        public static string mpi_run_flags = FileUtil.readConstant("mpi_run_flags");
        public static string connectionString = FileUtil.readConstant("connection_string");
        public static string externalRefsFile = FileUtil.readConstant("external_references_file");

        public static string HOSTS_FILE 
        {                                 
            get 
            {
                string path_hosts_file = Environment.GetEnvironmentVariable ("PATH_HOSTS_FILE");
                Console.WriteLine ("PATH_HOSTS_FILE is null ? " + (path_hosts_file == null));
                if (path_hosts_file == null)
                    path_hosts_file = FileUtil.readConstant("hosts_file", Path.Combine(HOME_PATH, "hpe_nodes"));
                Console.WriteLine ("PATH_HOSTS_FILE is " + path_hosts_file);
                return path_hosts_file;
            }
        }

        public static string PATH_CATALOG_FOLDER
        {
            get
            {
                string path_dgac_folder = Environment.GetEnvironmentVariable("PATH_CATALOG_FOLDER");
                Console.WriteLine("PATH_CATALOG_FOLDER is null ? " + (path_dgac_folder == null));
                if (path_dgac_folder == null)
                    path_dgac_folder = FileUtil.readConstant("path_catalog_folder", Path.Combine(HOME_PATH, "DGAC"));
                Console.WriteLine("PATH_CATALOG_FOLDER is " + path_dgac_folder);
                return path_dgac_folder;
            }
        }

        public static string PATH_GAC
        {
            /* get
             {
                 string path_gac = Environment.GetEnvironmentVariable("PATH_GAC"); 
                 if (path_gac == null)
                     path_gac = FileUtil.readConstant("path_gac");
                 Console.WriteLine("PATH_GAC is null ? " + (path_gac == null ? "null" : path_gac));
                 return path_gac;
             }
             */

             get
             {
                string path_gac = Path.Combine(PATH_MONO, "lib", "mono");
                 Console.WriteLine("PATH_GAC is {0}", path_gac);
                 return path_gac;
             }
             
        }

        public static string PATH_DGAC
        {
            get
            {
                string path_dgac = Path.Combine(PATH_GAC, "DGAC");
                Console.WriteLine("PATH_DGAC is {0}", path_dgac);
                return path_dgac;
            }

        }

        public static string CS_COMPILER_PATH
        {
            get
            {
                string cs_compiler_command = FileUtil.readConstant("cs_compiler");
                if (cs_compiler_command == null)
                    cs_compiler_command = "mcs";

                string cs_compiler_path = Path.Combine(PATH_MONO, "bin", cs_compiler_command);
                Console.WriteLine("CS_COMPILER_PATH is {0}", cs_compiler_path);
                return cs_compiler_path;
            }

        }

        public static string GACUTIL_PATH
        {
            get
            {
                string gacutil_path = Path.Combine(PATH_MONO, "bin", "gacutil");
                Console.WriteLine("GACUTIL_PATH is {0}", gacutil_path);
                return gacutil_path;
            }

        }

        public static string SN_PATH
        {
            get
            {
                string sn_path = Path.Combine(PATH_MONO, "bin", "sn");
                Console.WriteLine("SN_PATH is {0}", sn_path);
                return sn_path;
            }

        }

        public static string CLI_RUNTIME
        {
            get
            {
                string cli_runtime_path = Path.Combine(PATH_MONO, "bin", "mono");
                Console.WriteLine("CLI_RUNTIME_PATH is {0}", cli_runtime_path);
                return cli_runtime_path;
            }

        }

        public static string PATH_MONO
        {
            get
            {
                string path_mono = Environment.GetEnvironmentVariable("MONO_HOME");
                if (path_mono == null)
                    path_mono = FileUtil.readConstant("path_mono");
                if (path_mono == null)
                    throw new Exception("WHERE IS MONO ??? Inform in MONO_HOME variable or path_mono entrey of hpcshelf.properties");

                return path_mono;
            }

        }

        public static string HPCShelf_HOME
        {
            get
            {
                string hpcshelf_home = Environment.GetEnvironmentVariable("HPCShelf_HOME");
                if (hpcshelf_home == null)
                    hpcshelf_home = FileUtil.readConstant("hpcshelf_home", Path.Combine(HOME_PATH, "HPC-Shelf"));
                Console.WriteLine("HPCShelf_HOME is null ? " + (hpcshelf_home == null ? "null" : hpcshelf_home));
                return hpcshelf_home;
            }
        }

                public static string PlatformSAFe_ADDRESS 
        {
            get
            {
                string core_address = Environment.GetEnvironmentVariable("PlatformSAFe_ADDRESS");
                if (core_address == null)
                    core_address = FileUtil.readConstant("core_address");
                Console.WriteLine("CORE_ADDRESS is null ? " + (core_address == null ? "null" : core_address));
                return core_address == null ? "127.0.0.1" : core_address;
            }
        }

        public static int PlatformSAFe_PORT
        {
            get
            {
                string core_port = Environment.GetEnvironmentVariable("PlatformSAFe_PORT");
                if (core_port == null)
                    core_port = FileUtil.readConstant("core_port");
                Console.WriteLine("CORE_PORT is null ? " + (core_port == null ? "null" : core_port));
                return core_port == null ? 8077 : int.Parse(core_port);
            }
        }

        public static string CORE_ADDRESS
        {
            get
            {
                string core_address = Environment.GetEnvironmentVariable("CORE_ADDRESS");
                if (core_address == null)
                    core_address = FileUtil.readConstant("core_address");
                Console.WriteLine("CORE_ADDRESS is null ? " + (core_address == null ? "null" : core_address));
                return core_address == null ? "127.0.0.1" : core_address;
            }
        }

        public static int CORE_PORT
        {
            get
            {
                string core_port = Environment.GetEnvironmentVariable("CORE_PORT");
                if (core_port == null)
                    core_port = FileUtil.readConstant("core_port");
                Console.WriteLine("CORE_PORT is null ? " + (core_port == null ? "null" : core_port));
                return core_port == null ? 8077 : int.Parse(core_port);
            }
        }
        
        public static string VIRTUAL_PLATFORM_DEFAULT_ID = "platform";

        public const string ENUMS_KEY = "enumerator";
        public const string UNIT_MAPPING = "nodes";
        public const string KEY_KEY = "key";      
        public const string UID_KEY = "uid";
        public const string SESSION_KEY = "session";
        //public static string ASSEMBLY_STRING_KEY = "assembly_string";
        public const string COMPONENT_KEY = "id_concrete";
        public const string ID_FUNCTOR_APP = "id_functor_app";
        public const string COMPONENT_KIND = "component_kind";

        //public static string ID_ABSTRACT_KEY = "id_abstract";
        //public static string ID_INNER_KEY = "id_inner";
        public const string UNIT_KEY = "id_interface";
        public const string PORT_NAME = "port_name";
        public const string ENCLOSING_ARGUMENTS = "arguments";
        public const string ASSEMBLY_STRING_KEY = "assembly_string";
        public const string PORT_NAMES_KEY = "port_name";
    //    public const string CLASS_NAME_KEY = "class_name";
        public static string KIND_KEY = "kind";
        public static string PARTITION_INDEX_KEY = "partition_index";

        public const string DEFAULT_PROVIDES_PORT_IMPLEMENTS = "implements";
        //public const string DEFAULT_CREATESLICES_PORT_IMPLEMENTS = "create_slices";
        public const string DEFAULT_PROVIDES_PORT_SERVICE = "service_client";
        public const string DEFAULT_USES_PORT_SERVICE = "service_server";

        //public const string FACET_IP_ADDRESS = "facet_ip_address";
        //public const string FACET_PORT = "facet_ip_port";
        public const string CHANNEL_ID = "channel_id";
        public const string CHANNEL_FACET_INSTANCE = "channel_facet_instance";
        public const string CHANNEL_FACET_ADDRESS = "channel_address";
        public static string FACET_INSTANCE = "facet_instance";
        public static string ENCLOSING_FACET_INSTANCE = "enclosing_facet_instance";
        public static string FACET = "facet";
        public static string ENCLOSING_FACET = "enclosing_facet";
        public static string FACET_TOPOLOGY = "facet_topology";
        public static string BINDING_SEQUENTIAL = "binding_sequential";
        public static string IGNORE = "ignore";

        public static string INITIALIZE_PORT_NAME = "initialize_port";
        public static string INITIALIZE_PORT_TYPE = "org.hpcshelf.ports.AutomaticSlicesPort";
        public static string PROVENANCE_PORT_NAME = "provenance_port";
        public static string PROVENANCE_PORT_TYPE = "org.hpcshelf.ports.ProvenancePort";
        public static string CORE_PROVENANCE_PORT_NAME = "core_provenance_port";
        public static string CORE_LIFECYCLE_PORT_NAME = "core_lifecycle_port";
        public static string LIFECYCLE_PORT_TYPE = "org.hpcshelf.ports.LifeCyclePort";
        public static string RECONFIGURE_PORT_NAME = "reconfigure_port";
        public static string RECONFIGURE_PORT_TYPE = "org.hpcshelf.ports.ReconfigurationAdvicePort";
        public static string RELEASE_PORT_NAME = "release_port";
        public static string RELEASE_PORT_TYPE = "org.hpcshelf.ports.ReleasePort";
        public const string GO_PORT_TYPE = "gov.cca.ports.GoPort";
        public const string GO_PORT_NAME = "go";
        public const string CERTIFICATION_PORT_TYPE = "gov.cca.ports.CertificationPort";
        public const string CERTIFICATION_PORT_NAME = "certification";
        public const string FETCH_FILES_PORT_TYPE = "gov.cca.ports.FetchFilesPort";
        public const string FETCH_FILES_PORT_NAME = "fetch_files";
        public const string PARAMETER_PORT_TYPE = "gov.cca.ports.ParameterPort";
        public const string PARAMETER_PORT_NAME = "parameter";

        public const string CERTIFIER_INNER_ID = "certification_binding";

        public const string LIFECYCLE_RUN = "run";
        public const string LIFECYCLE_INSTANTIATE = "instantiate";
        public const string LIFECYCLE_DEPLOY = "deploy";
        public const string LIFECYCLE_RESOLVE = "resolve";
        public const string LIFECYCLE_RESOLVE_SYSTEM = "resolve_system";
        public const string LIFECYCLE_CERTIFY = "certify";
        public const string LIFECYCLE_RELEASE = "release";

        public const int KIND_UNRECOGNIZED = -1;
        public const int KIND_APPLICATION = 0;
        public const int KIND_COMPUTATION = 1;
        public const int KIND_SYNCHRONIZER = 2;
        public const int KIND_ENVIRONMENT = 3;
        public const int KIND_PLATFORM = 4;
        public const int KIND_QUALIFIER = 5;
        public const int KIND_DATASTRUCTURE = 6;
        public const int KIND_BINDING = 7;
        public const int KIND_SYSTEM = 8;
        public const int KIND_CERTIFIER = 9;
        public const int KIND_TACTICAL = 10;

        public const string KIND_APPLICATION_NAME = "Application";
        public const string KIND_COMPUTATION_NAME = "Computation";
        public const string KIND_SYNCHRONIZER_NAME = "Synchronizer";
        public const string KIND_ENVIRONMENT_NAME = "Environment";
        public const string KIND_PLATFORM_NAME      = "Platform";
        public const string KIND_QUALIFIER_NAME = "Qualifier";
        public const string KIND_DATASTRUCTURE_NAME = "Data";
        public const string KIND_TOPOLOGY_NAME = "Topology";
        public const string KIND_BINDING_NAME = "Binding";
        public const string KIND_SYSTEM_NAME = "System";
        public const string KIND_CERTIFIER_NAME = "Certifier";
        public const string KIND_TACTICAL_NAME = "Tactical";


        public static IDictionary<EnvironmentPortType, string> envUnitName = new Dictionary<EnvironmentPortType, string>()
        {
            {EnvironmentPortType.user,"client"},
            {EnvironmentPortType.provider, "server"},
            {EnvironmentPortType.platform_user, "node"},
            {EnvironmentPortType.platform_provider, "node"}
        };

        public static string tskUnitName = "peer";

        public static IDictionary<EnvironmentPortType, string> envInterfaceName = new Dictionary<EnvironmentPortType, string>()
        {
            {EnvironmentPortType.user,"IClient"},
            {EnvironmentPortType.provider, "IServer"},
            {EnvironmentPortType.platform_provider, "IProcessingNode"},
            {EnvironmentPortType.platform_user, "IProcessingNode"}
        };

        public static Dictionary<string, int> kindInt = new Dictionary<string, int>()
        {
            {KIND_APPLICATION_NAME, Constants.KIND_APPLICATION},
            {KIND_PLATFORM_NAME, Constants.KIND_PLATFORM},
            {KIND_COMPUTATION_NAME, Constants.KIND_COMPUTATION},
            {KIND_DATASTRUCTURE_NAME,Constants.KIND_DATASTRUCTURE},
            {KIND_ENVIRONMENT_NAME,Constants.KIND_ENVIRONMENT},
            {KIND_QUALIFIER_NAME,Constants.KIND_QUALIFIER},
            {KIND_SYNCHRONIZER_NAME, Constants.KIND_SYNCHRONIZER},
            {KIND_BINDING_NAME, Constants.KIND_BINDING},
            {KIND_SYSTEM_NAME, Constants.KIND_SYSTEM},
            {KIND_CERTIFIER_NAME, Constants.KIND_CERTIFIER},
            {KIND_TACTICAL_NAME, Constants.KIND_TACTICAL}
        };

        public static Dictionary<int, string> kindString = new Dictionary<int, string>()
        {
            { Constants.KIND_APPLICATION, KIND_APPLICATION_NAME},
            {Constants.KIND_PLATFORM, KIND_PLATFORM_NAME},
            {Constants.KIND_COMPUTATION, KIND_COMPUTATION_NAME},
            {Constants.KIND_DATASTRUCTURE, KIND_DATASTRUCTURE_NAME},
            {Constants.KIND_ENVIRONMENT, KIND_ENVIRONMENT_NAME},
            {Constants.KIND_QUALIFIER, KIND_QUALIFIER_NAME},
            {Constants.KIND_SYNCHRONIZER, KIND_SYNCHRONIZER_NAME},
            {Constants.KIND_BINDING, KIND_BINDING_NAME},
            {Constants.KIND_SYSTEM, KIND_SYSTEM_NAME},
            {Constants.KIND_CERTIFIER, KIND_CERTIFIER_NAME},
            {Constants.KIND_TACTICAL, KIND_TACTICAL_NAME}
        };
        
        public static string getArgVal(string argId)
        {
            string[] args = Environment.GetCommandLineArgs();
            int pos = 0;
            foreach (string arg in args)
            {
                if (arg.Equals(argId))
                {
                    if (pos <= args.Length)
                    {
                        return args[pos + 1];
                    }
                    else
                    {
                        return null;
                    }
                }
                pos++;
            }
            return null;
        }


    }

}
