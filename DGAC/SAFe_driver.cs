// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.17020
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

// 
//This source code was auto-generated by MonoXSD
//
namespace ComponentXML {
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    [System.Xml.Serialization.XmlRootAttribute("component", Namespace="http://www.example.org/SAFe_driver", IsNullable=false)]
    public partial class IDriverComponent {
        
        private IDriverContextArgument[] context_argumentField;
        
        private IDriverPlacement[] placementField;
        
        private IDriverPort[] portField;
        
        private string nameField;
        
        private IDriverComponentKind kindField;
        
        private string library_pathField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("context_argument")]
        public IDriverContextArgument[] context_argument {
            get {
                return this.context_argumentField;
            }
            set {
                this.context_argumentField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("placement")]
        public IDriverPlacement[] placement {
            get {
                return this.placementField;
            }
            set {
                this.placementField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("port")]
        public IDriverPort[] port {
            get {
                return this.portField;
            }
            set {
                this.portField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public IDriverComponentKind kind {
            get {
                return this.kindField;
            }
            set {
                this.kindField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string library_path {
            get {
                return this.library_pathField;
            }
            set {
                this.library_pathField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    public partial class IDriverContextArgument {
        
        private object[] itemsField;
        
        private string parameter_idField;
        
        private string library_pathField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("quantifier", typeof(decimal))]
        [System.Xml.Serialization.XmlElementAttribute("restriction", typeof(IDriverContextArgument))]
        public object[] Items {
            get {
                return this.itemsField;
            }
            set {
                this.itemsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string parameter_id {
            get {
                return this.parameter_idField;
            }
            set {
                this.parameter_idField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string library_path {
            get {
                return this.library_pathField;
            }
            set {
                this.library_pathField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    public partial class IDriverPortInfoElement {
        
        private string nameField;
        
        private int indexField;
        
        private string roleField;
        
        private int facetField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int index {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string role {
            get {
                return this.roleField;
            }
            set {
                this.roleField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int facet {
            get {
                return this.facetField;
            }
            set {
                this.facetField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    public partial class IDriverPort {
        
        private int indexField;
        
        private int facet_hostField;
        
        private int facet_bindingField;
        
        private string bindingField;
        
        private string nameField;
        
        private string componentField;
        
        private IDriverPortRole roleField;
        
        public IDriverPort() {
            this.bindingField = "null";
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int index {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int facet_host {
            get {
                return this.facet_hostField;
            }
            set {
                this.facet_hostField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int facet_binding {
            get {
                return this.facet_bindingField;
            }
            set {
                this.facet_bindingField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute("null")]
        public string binding {
            get {
                return this.bindingField;
            }
            set {
                this.bindingField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string component {
            get {
                return this.componentField;
            }
            set {
                this.componentField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public IDriverPortRole role {
            get {
                return this.roleField;
            }
            set {
                this.roleField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.example.org/SAFe_driver")]
    public enum IDriverPortRole {
        
        /// <remarks/>
        peer,
        
        /// <remarks/>
        client,
        
        /// <remarks/>
        server,
        
        /// <remarks/>
        client_server,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    public partial class IDriverPlacement {
        
        private string[] virtual_platformField;
        
        private int facetField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("virtual_platform")]
        public string[] virtual_platform {
            get {
                return this.virtual_platformField;
            }
            set {
                this.virtual_platformField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int facet {
            get {
                return this.facetField;
            }
            set {
                this.facetField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.example.org/SAFe_driver")]
    public enum IDriverComponentKind {
        
        /// <remarks/>
        Binding,
        
        /// <remarks/>
        Connector,
        
        /// <remarks/>
        Computation,
        
        /// <remarks/>
        VirtualPlatform,
        
        /// <remarks/>
        DataSource,
        
        /// <remarks/>
        Workflow,
        
        /// <remarks/>
        Application,
        
        /// <remarks/>
        System,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    [System.Xml.Serialization.XmlRootAttribute("contract", Namespace="http://www.example.org/SAFe_driver", IsNullable=false)]
    public partial class IDriverContract {
        
        private object[] itemsField;
        
        private string library_pathField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("context_argument", typeof(IDriverContextArgument))]
        [System.Xml.Serialization.XmlElementAttribute("quantifier", typeof(decimal))]
        public object[] Items {
            get {
                return this.itemsField;
            }
            set {
                this.itemsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string library_path {
            get {
                return this.library_pathField;
            }
            set {
                this.library_pathField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    [System.Xml.Serialization.XmlRootAttribute("port_info", Namespace="http://www.example.org/SAFe_driver", IsNullable=false)]
    public partial class IDriverPortInfo {
        
        private IDriverPortInfoElement[] portField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("port")]
        public IDriverPortInfoElement[] port {
            get {
                return this.portField;
            }
            set {
                this.portField = value;
            }
        }
    }
}
