﻿using System.ServiceModel;

namespace org.hpcshelf.backend.platform
{
    [ServiceContract]
	public interface IPlatformServices 
	{
		[OperationContract]
		int    getNumberOfNodes   ();
		
        [OperationContract]
		int    getBaseBindingPort ();
		
        [OperationContract]
		string deploy             (string module_data, byte[] key_file_contents);
		
        [OperationContract]
		void   setPlatformRef     (string arch_ref);
		
        [OperationContract]
		string getPlatformRef     ();
		
        [OperationContract]
		string instantiate        (string component_ref, string[] ctree, int facet_instance, string[] channel_id, int[] channel_facet_instance, string[] channel_facet_address);
		
        [OperationContract]
		void   run                (string component_ref);

        [OperationContract]
        void release(string component_ref);

        [OperationContract]
		string getStatus          ();

		[OperationContract]
		string allocateBindingAddress(string channel_id, int n);
     }
}
