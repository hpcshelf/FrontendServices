﻿using System;
using System.Text;
using System.IO;
using System.Threading;
using ZeroMQ;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

// CustomCreationConverter
using System.Collections.Generic; // IDictionary
using System.Security.Cryptography;

// IList
using System.Collections;
using System.Reflection;// IEnumerator
using HPCShelfCLIBase;
using org.hpcshelf.command;

public class SimpleKernel
{
    public static Session session;
    private static ICLIServices command_services;

    [STAThread]
    public static void Main(string[] args)
    {
        Console.Error.WriteLine(String.Format("Filename: {0}", args[0]));

        System.Net.ServicePointManager.DefaultConnectionLimit = 100;

        Start(args[0]);
    }

    public static void launchPlatformSAfe()
    {
        try
        {
            string path = Environment.GetEnvironmentVariable("Swirls_HOME");
            if (path == null)
            {
                Console.WriteLine("Swirls_HOME not set. Using the current directory.");
                path = Utils.CURRENT_FOLDER;
            }

            string script_command = Path.Combine(path, "run_platform_SAFe.sh");

            Console.WriteLine("STARTING PlatformSAFe services: {0}", script_command);
            Utils.commandExecBash(script_command);
            Console.WriteLine("ok !");
        }
        catch(Exception e)
        {
            Console.Error.WriteLine("\nError while launching PlatformSAFe services: {0}", e.Message);
            Console.Error.WriteLine("-- STACK TRACE -- \n{0}", e.StackTrace);
            if (e.InnerException != null)
            {
                Console.Error.WriteLine("INNER EXCEPTION: {0}", e.InnerException.Message);
                Console.Error.WriteLine("-- STACK TRACE -- \n{0}", e.InnerException.StackTrace);
            }
        }
    }
    public static Dictionary<string, object> dict(params object[] args)
    {
        var retval = new Dictionary<string, object>();
        for (int i = 0; i < args.Length; i += 2)
        {
            retval[args[i].ToString()] = args[i + 1];
        }
        return retval;
    }

    public static List<string> list(params object[] args)
    {
        var retval = new List<string>();
        foreach (object arg in args)
        {
            retval.Add(arg.ToString());
        }
        return retval;
    }

    public static List<byte[]> blist(params byte[][] args)
    {
        var retval = new List<byte[]>();
        foreach (byte[] arg in args)
        {
            retval.Add(arg);
        }
        return retval;
    }

    public static IDictionary<string, object> decode(string json)
    {
        return JsonConvert.DeserializeObject<IDictionary<string, object>>(
               json, new JsonConverter[] { new MyConverter() });
    }

    public static void Start(string config_file)
    {
        command_services = new CLIServices0();

        session = new Session(config_file);

        GLib.ExceptionManager.UnhandledException += session.HandleException;
        config_file = session.filename;
        launchPlatformSAfe();
        session.start();
        while (true)
        {
            if (session.need_restart)
            {
                session.need_restart = false;
                session.stop();
                session = new Session(config_file);
                launchPlatformSAfe();
                session.start();
            }
            if (session.request_quit)
            {
                session.stop();
                System.Environment.Exit(0);
            }
            Thread.Sleep((int)(1 * 1000)); // seconds
        }
    }

    public class Authorization
    {
        public string secure_key;
        public string digestmod;
        HMACSHA256 hmac;

        public Authorization(string secure_key, string digestmod)
        {
            this.secure_key = secure_key;
            this.digestmod = digestmod;
            hmac = new HMACSHA256(Encoding.UTF8.GetBytes(secure_key));
        }

        public string sign(List<string> list)
        {
            hmac.Initialize();
            foreach (string item in list)
            {
                byte[] sourcebytes = Encoding.UTF8.GetBytes(item);
                hmac.TransformBlock(sourcebytes, 0, sourcebytes.Length, null, 0);
            }
            hmac.TransformFinalBlock(new byte[0], 0, 0);
            return BitConverter.ToString(hmac.Hash).Replace("-", "").ToLower();
        }
    }

    class MyConverter : CustomCreationConverter<IDictionary<string, object>>
    {
        public override IDictionary<string, object> Create(Type objectType)
        {
            return new Dictionary<string, object>();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(object) || base.CanConvert(objectType);
        }

        public override object ReadJson(JsonReader reader,
                                             Type objectType,
                                             object existingValue,
                                             JsonSerializer serializer)
        {
            if ((reader.TokenType == JsonToken.StartObject) ||
            (reader.TokenType == JsonToken.Null))
            {
                return base.ReadJson(reader, objectType, existingValue, serializer);
            }
            else
            {
                return serializer.Deserialize(reader);
            }
        }
    }

    public class Session
    {
        internal bool blocking = true;
        public bool need_restart = false;
        public bool request_quit = false;
        public string filename;
        public HeartBeatChannel hb_channel;
        public ShellChannel shell_channel;
        public IOPubChannel iopub_channel;
        public ControlChannel control_channel;
        public StdInChannel stdin_channel;
        public Authorization auth;
        public string engine_identity;
        public int engine_identity_int = -1;
        public int current_execution_count = 0;
        public IDictionary<string, object> parent_header;
        public IDictionary<string, object> config;
        public System.IO.StreamWriter log;
        public bool rich_display = false;

        public Session(string filename)
        {
            this.filename = filename;
            engine_identity = System.Guid.NewGuid().ToString();
            string json;
            if (this.filename != "")
            {
                json = File.ReadAllText(this.filename);
                config = decode(json);
                Console.Error.WriteLine(String.Format("config: {0}", config["transport"]));
                Console.Error.WriteLine(String.Format("config: {0}", config["ip"]));
                Console.Error.WriteLine(String.Format("config: {0}", config["hb_port"]));
            }
            else
            {
                config = dict("key", System.Guid.NewGuid().ToString(),
                                       "signature_scheme", "hmac-sha256",
                                       "transport", "tcp",
                                       "ip", "127.0.0.1",
                                       "hb_port", "0",
                                       "shell_port", "0",
                                       "iopub_port", "0",
                                       "control_port", "0",
                                       "stdin_port", "0");
            }
            auth = new Authorization(config["key"].ToString(), config["signature_scheme"].ToString());

            hb_channel = new HeartBeatChannel(this, auth, config["transport"].ToString(), config["ip"].ToString(), config["hb_port"].ToString());
            shell_channel = new ShellChannel(this, auth, config["transport"].ToString(), config["ip"].ToString(), config["shell_port"].ToString());
            iopub_channel = new IOPubChannel(this, auth,config["transport"].ToString(), config["ip"].ToString(), config["iopub_port"].ToString());
            control_channel = new ControlChannel(this, auth, config["transport"].ToString(), config["ip"].ToString(), config["control_port"].ToString());
            stdin_channel = new StdInChannel(this, auth, config["transport"].ToString(), config["ip"].ToString(), config["stdin_port"].ToString());

            if (this.filename == "")
            {
                config["hb_port"] = hb_channel.port;
                config["shell_port"] = shell_channel.port;
                config["iopub_port"] = iopub_channel.port;
                config["control_port"] = control_channel.port;
                config["stdin_port"] = stdin_channel.port;
                string kernelname = String.Format("kernel-{0}.json",
                                                           System.Diagnostics.Process.GetCurrentProcess().Id);
                string ipython_config = ("{{\n" +
                             "  \"hb_port\": {0},\n" +
                             "  \"shell_port\": {1},\n" +
                             "  \"iopub_port\": {2},\n" +
                             "  \"control_port\": {3},\n" +
                             "  \"stdin_port\": {4},\n" +
                             "  \"ip\": \"{5}\",\n" +
                             "  \"signature_scheme\": \"{6}\",\n" +
                             "  \"key\": \"{7}\",\n" +
                             "  \"transport\": \"{8}\"\n" +
                             "}}");
                ipython_config = String.Format(ipython_config,
                                                        config["hb_port"],
                                                        config["shell_port"],
                                                        config["iopub_port"],
                                                        config["control_port"],
                                                        config["stdin_port"],
                                                        config["ip"],
                                                        config["signature_scheme"],
                                                        config["key"],
                                                        config["transport"]);
                System.IO.StreamWriter sw = new System.IO.StreamWriter(this.filename);
                sw.Write(ipython_config);
                sw.Close();
                Console.Error.WriteLine("IPython config file written to:");
                Console.Error.WriteLine("   \"{0}\"", this.filename);
                Console.Error.WriteLine("To exit, you will have to explicitly quit this process, by either sending");
                Console.Error.WriteLine("\"quit\" from a client, or using Ctrl-\\ in UNIX-like environments.");
                Console.Error.WriteLine("To read more about this, see https://github.com/ipython/ipython/issues/2049");
                Console.Error.WriteLine("To connect another client to this kernel, use:");
                Console.Error.WriteLine("    --existing {0} --profile calico", kernelname);
            }
        }

        public static Dictionary<string, object> Header(string msg_type)
        {
            var retval = new Dictionary<string, object>();
            retval["date"] = now();
            retval["msg_id"] = msg_id();
            retval["username"] = "kernel";
            retval["session"] = session.engine_identity;
            retval["msg_type"] = msg_type;
            return retval;
        }

        public static string encode(IDictionary<string, object> dict)
        {
            return JsonConvert.SerializeObject(dict);
        }

        public static string now()
        {
            return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffffff");
        }

        public static string msg_id()
        {
            return System.Guid.NewGuid().ToString();
        }

        public void SetBlocking(bool blocking)
        {
            this.blocking = blocking;
        }

        public void SetRichDisplay(bool value)
        {
            this.rich_display = value;
        }

        public bool GetRichDisplay()
        {
            return this.rich_display;
        }

        public void HandleException(GLib.UnhandledExceptionArgs args)
        {
            Console.Error.WriteLine(String.Format("Exception: {0}", args.ExceptionObject.ToString()));
        }

        public bool GetBlocking()
        {
            return blocking;
        }

        public string TitleCase(string text)
        {
            return char.ToUpper(text[0]) + text.Substring(1);
        }

        public void start()
        {
           // Console.WriteLine("hb_channel {0}", hb_channel.port);
           // Console.WriteLine("shell_channel {0}", shell_channel.port);
           // Console.WriteLine("control_channel {0}", control_channel.port);
           // Console.WriteLine("stdin_channel {0}", stdin_channel.port);

            hb_channel.thread.Start();
            shell_channel.thread.Start();
            control_channel.thread.Start();
            stdin_channel.thread.Start();
        }

        public void stop()
        {
            try
            {
                command_services.shutdownSystems();
                hb_channel.stop();
                shell_channel.stop();
                control_channel.stop();
                stdin_channel.stop();
            }
            catch
            {
                // ignore errors, shutting down
            }
        }

        public void send(Channel channel,
                              string msg_type,
                  IList<byte[]> identities,
                  IDictionary<string, object> parent_header,
                  IDictionary<string, object> metadata,
                  IDictionary<string, object> content
                  )
        {
            send(channel,
              msg_type,
              identities,
              encode(parent_header),
              encode(metadata),
              encode(content));
        }

        public void send(Channel channel,
                         string msg_type,
                         IList<byte[]> identities,
                         string parent_header,
                         string metadata,
                         string content)
        {

           // Console.Error.WriteLine(String.Format("send: {0}", msg_type));
            string header = encode(Header(msg_type));
            string signature = auth.sign(new List<string>() { header, parent_header, metadata, content });
            List<string> parts = list("<IDS|MSG>",
                                      signature,
                                      header,
                                      parent_header,
                                      metadata,
                                      content);
            foreach (byte[] msg in identities)
            {
                Console.Error.WriteLine(String.Format("send ident: {0}", msg));
                ZFrame msg_frame = new ZFrame(msg);
                channel.socket.SendFrameMore(msg_frame);
            }
            int count = 0;
            foreach (string msg in parts)
            {
                Console.Error.WriteLine(String.Format("send parts: {0}", msg));
                if (count < parts.Count - 1)
                {
                    ZFrame msg_frame = new ZFrame(msg, Encoding.UTF8);
                    channel.socket.SendFrameMore(msg_frame);
                }
                else
                {
                    ZFrame msg_frame = new ZFrame(msg, Encoding.UTF8);
                    channel.socket.SendFrame(msg_frame);
                }
                count++;
            }
        }
    }

    public class Channel
    {
        public Session session;
        public string transport;
        public string address;
        public ZContext context;
        public ZSocket socket;
        public string port;
        public Authorization auth;
        public Thread thread;
        string state = "normal"; // "waiting", "ready"
        public string reply = "";

        public Channel(Session session,
                       Authorization auth,
                       string transport,
                       string address,
                       string port,
                       ZSocketType socket_type)
        {
            this.session = session;
            this.auth = auth;
            this.transport = transport;
            this.address = address;
            this.port = port;
            context = new ZContext();  // ?? ZMQ.Context.Create ();
            socket = new ZSocket(context, socket_type); // context..Socket(socket_type);
            if (port == "0")
            {
                Random rand = new Random();
                int min_port = 49152;
                int max_port = 65536;
                int max_tries = 100;
                int p = 0;
                int i;
                for (i = 0; i < max_tries; i++)
                {
                    p = rand.Next(min_port, max_port);
                    string addr = String.Format("{0}://{1}:{2}", this.transport, this.address, p);
                    try
                    {
                        socket.Bind(addr);
                    }
                    catch
                    {
                        continue;
                    }
                    break;
                }
                if (i == 100)
                {
                    throw new System.Exception("Exhausted tries looking for random port");
                }
                this.port = "" + p;
            }
            else
            {
                socket.Bind(String.Format("{0}://{1}:{2}", this.transport, this.address, this.port));
            }
            socket.Identity = Encoding.UTF8.GetBytes(session.engine_identity);
            thread = new Thread(new ThreadStart(loop));
        }

        public void SetState(string newstate, string result)
        {
            lock (state)
            {
                state = newstate;
                reply = result;
            }
        }

        public string GetState()
        {
            lock (state)
            {
                return state;
            }
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public virtual void loop()
        {
            string signature, s_header, s_parent_header, s_metadata, s_content;
            byte[] bmessage = new byte[100];
            int size;
            List<byte[]> identities;
            while (!session.request_quit)
            {
                //Console.Error.WriteLine(String.Format("loop()"));
                identities = new List<byte[]>();
                var MyEncoding = System.Text.Encoding.GetEncoding("windows-1252");

              //  Console.Write("going receive1 {0} {1} ...", address, port);
                ZFrame zframe = socket.ReceiveFrame();
                bmessage = zframe.Read();
                size = bmessage.Length;

              //  Console.WriteLine("ok {0}", size);
              //  Console.Error.WriteLine(String.Format("Receive: {0}", MyEncoding.GetString(bmessage, 0, size)));

                while (MyEncoding.GetString(bmessage, 0, size) != "<IDS|MSG>")
                {
                    byte[] buffer = new byte[size];
                    for (int i = 0; i < size; i++)
                    {
                        buffer[i] = bmessage[i];
                    }
                    identities.Add(buffer);
                 //   Console.Write("going receive2 {0} {1} ...", address, port);
                    zframe = socket.ReceiveFrame();
                    bmessage = zframe.Read();
                    size = bmessage.Length;
                 //  Console.WriteLine("ok {0}", size);
                 //   Console.Error.WriteLine(String.Format("Receive: {0}", MyEncoding.GetString(bmessage, 0, size)));
                }

                signature = socket.ReceiveFrame().ReadString(Encoding.UTF8); //Console.WriteLine("signature: {0}", signature);
                s_header = socket.ReceiveFrame().ReadString(Encoding.UTF8); //Console.WriteLine("s_header: {0}", s_header);
                s_parent_header = socket.ReceiveFrame().ReadString(Encoding.UTF8); //Console.WriteLine("s_parent_header: {0}", s_parent_header);
                s_metadata = socket.ReceiveFrame().ReadString(Encoding.UTF8);//Console.WriteLine("s_metadata: {0}", s_metadata);
                s_content = socket.ReceiveFrame().ReadString(Encoding.UTF8); //Console.WriteLine("s_content: {0}", s_content);
                string comp_sig = auth.sign(new List<string>() { s_header, s_parent_header, s_metadata, s_content }); //Console.WriteLine("comp_sig: {0}", comp_sig);

                if (comp_sig != signature)
                {
                    throw new System.Exception("Error: signatures don't match!");
                }

                IDictionary<string, object> header = decode(s_header);
                IDictionary<string, object> parent_header = decode(s_parent_header);
                IDictionary<string, object> metadata = decode(s_metadata);
                IDictionary<string, object> content = decode(s_content);
                Console.Error.WriteLine(String.Format("msg_type: {0}", header["msg_type"]));

                on_recv(identities, signature, header, parent_header, metadata, content);
            }
        }

        public virtual void on_recv(List<byte[]> identities,
                                         string m_signature,
                                         IDictionary<string, object> m_header,
                                         IDictionary<string, object> m_parent_header,
                                         IDictionary<string, object> m_metadata,
                                         IDictionary<string, object> m_content)
        {
            //throw new Exception(this.ToString() + ": unknown msg_type: " + m_header["msg_type"]);
        }

        public void stop()
        {
            thread.Abort();
            socket.Linger = System.TimeSpan.FromSeconds(1);
            // ??? socket.Close ();
            //context.Terminate();
        }
    }

    public class ExecutionCounter
    {
        readonly object counter_lock = new object();
        int execution_count = 1;
        public int ExecutionCount { get { return execution_count; } }
        public int increment() { lock (counter_lock) { int c = execution_count++; return c; } }
        public int increment(int n) { lock (counter_lock) { int c = execution_count;  execution_count+=n; return c; } }

    }

    public class ShellChannel : Channel
    {
        public ExecutionCounter execution_count = new ExecutionCounter();

        public ShellChannel(Session session, Authorization auth, string transport, string address, string port) :
                base(session, auth, transport, address, port, ZSocketType.ROUTER)
        {
        }

        private static readonly object lock_message = new object();

        // Shell
        public override void on_recv(List<byte[]> identities,
                                          string m_signature,
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content)
        {
            // Shell handler
            string msg_type = m_header["msg_type"].ToString();
            if (msg_type == "execute_request")
            {
                var metadata0 = dict();
                var content0 = dict("execution_state", "busy");
                lock (lock_message) { session.send(session.iopub_channel, "status", blist(), m_header, metadata0, content0); }
                // ---------------------------------------------------
                //metadata = dict();
                Console.WriteLine("m_content[\"code\"]= --- {0} ---", m_content["code"]);

                AutoResetEvent async = new AutoResetEvent(false);
                AutoResetEvent async_set = new AutoResetEvent(false);
                string async_flag = null;

                Thread t = new Thread((obj) =>
                {
                    // var output = "complete!";
                    int count = execution_count.increment();

                    string command = m_content["code"].ToString();
                    command = adjust("; ", ";", ref command);
                    command = command.Replace(";\n", ";");
                    command = adjust("; ", ";", ref command);
                    command = command.Replace("\n", " ");
                    command = adjust("  ", " ", ref command);

                    var output = command_services.run_command(command.Trim().Split(' '), async, out async_flag, session, m_header, m_parent_header, m_metadata, m_content, execution_count, lock_message);
                    async_set.WaitOne();

                    //content = dict("execution_count", execution_count, "code", m_content["code"].ToString());
                    //session.send(session.iopub_channel, "pyin", blist(), m_header, metadata, content);
                    // ---------------------------------------------------
                    if (output != null)
                    {
                        var metadata3 = dict();
                        var content3 = dict("execution_count", execution_count.increment(), "data", dict("text/plain", output), "metadata", dict());
                        lock (lock_message) { session.send(session.iopub_channel, "pyout", blist(), m_header, metadata3, content3); }
                    }
                    // ---------------------------------------------------
                    var metadata1 = dict();
                    var content1 = dict("execution_state", "idle");
                    lock (lock_message) { session.send(session.iopub_channel, "status", blist(), m_header, metadata1, content1); }
                    var metadata2 = dict("dependencies_met", true, "engine", session.engine_identity, "status", "ok", "started", Session.now());
                    var content2 = dict(
                            "status", "ok",
                            "execution_count", count,
                            "user_variables", dict(),
                            "payload", blist(),
                            "user_expressions", dict()
                            );
                    lock (lock_message) { session.send(session.shell_channel, "execute_reply", identities, m_header, metadata2, content2); }

                    //if (async_flag != null && async_commands.ContainsKey(async_flag))
                    //    async_commands.Remove(async_flag);

                    Console.WriteLine("{0} finished !!!", m_content["code"]);
                });

                t.Start();
                async.WaitOne();
                if (async_flag == null)
                {
                    async_set.Set();
                    Console.WriteLine("SYNCHRONOUS");
                    t.Join();
                }
                else if (async_flag.StartsWith(","))
                {
                    string[] async_flag_array = async_flag.Substring(1).Split(new char[1] { ',' });
                    foreach (string async_flag_item in async_flag_array)
                    {
                        if (async_commands.ContainsKey(async_flag_item))
                        {
                            Console.WriteLine("ASYNC WAIT {0} - BEFORE JOIN", async_flag_item);
                            async_commands[async_flag_item].Join();
                            Console.WriteLine("ASYNC WAIT {0} - AFTER JOIN", async_flag_item);
                            async_commands.Remove(async_flag_item);
                        }
                        else
                            Console.WriteLine("async wait handle {0} not found !", async_flag_item);
                    }
                    async_set.Set();
                }
                else if (!async_flag.Equals("_"))
                {
                    Console.WriteLine("ASYNCHRONOUS {0}", async_flag);
                    async_commands[async_flag] = t;
                    async_set.Set();
                }
                else
                {
                    Console.WriteLine("ASYNCHRONOUS anonymous");
                    async_set.Set();
                }

            }
            else if (msg_type == "kernel_info_request")
            {
                var metadata = dict();
                var content = dict("protocol_version", new List<int>() { 5, 0 },
                                   "ipython_version", new List<object>() { 1, 1, 0, "" },
                                   "language_version", new List<int>() { 0, 0, 1 },
                                   "language", "hpcshelf",
                                   "implementation", "hpcshelf_kernel",
                                   "implementation_version", new List<int>() { 1, 1 },
                                   "language_info", dict("name", "simple_kernel",
                                                         "version", new List<int>() { 1, 0 },
                                                         "mimetype", "",
                                                         "file_extenson", ".cs",
                                                         "pygments_lexer", "",
                                                         "codemirror_mode", "",
                                                         "nbconverter_export", ""
                                                        ),
                                   "banner", ""
                                   );
                lock (lock_message) { session.send(session.shell_channel, "kernel_info_reply", identities, m_header, metadata, content); }

                content = dict("execution_state", "idle");

                lock (lock_message) { session.send(session.iopub_channel, "status", blist(), m_header, metadata, content); }
            }
            else if (msg_type == "is_complete_request")
            {

            }
            else
            {
                //throw new Exception("ShellChannel: unknown msg_type: " + msg_type);
            }
        }

        private static string adjust(string s1, string s2, ref string command)
        {
            string command_old;
            do
            {
                command_old = command;
                command = command.Replace(s1, s2);
            }
            while (command != command_old);
            return command_old;
        }
    }

    private static IDictionary<string, Thread> async_commands = new Dictionary<string, Thread>();

    public class IOPubChannel : Channel
    {
        public IOPubChannel(Session session,
                                 Authorization auth,
                                 string transport,
                                 string address,
                                 string port) :
                base(session, auth, transport, address, port, ZSocketType.PUB)
        {
        }
    }

    public class ControlChannel : Channel
    {
        public ControlChannel(Session session,
                                   Authorization auth,
                                   string transport,
                                   string address,
                                   string port) :
                base(session, auth, transport, address, port, ZSocketType.ROUTER)
        {
        }

        public override void on_recv(List<byte[]> identities,
                                          string m_signature,
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content)
        {
            // Control handler
            string msg_type = m_header["msg_type"].ToString();
            if (msg_type == "shutdown_request")
            {
                session.request_quit = true;
            }
            else
            {
            }
        }
    }

    public class StdInChannel : Channel
    {

        public StdInChannel(Session session,
                                 Authorization auth,
                                 string transport,
                                 string address,
                                 string port) :
                base(session, auth, transport, address, port, ZSocketType.ROUTER)
        {
        }


        // StdIn
        public override void on_recv(List<byte[]> identities,
                                          string m_signature,
                                          IDictionary<string, object> m_header,
                                          IDictionary<string, object> m_parent_header,
                                          IDictionary<string, object> m_metadata,
                                          IDictionary<string, object> m_content)
        {
            // StdIn handler
            string msg_type = m_header["msg_type"].ToString();
            if (msg_type == "input_reply")
            {
                SetState("ready", m_content["value"].ToString());
            }
            else
            {
            }
        }
    }

    public class HeartBeatChannel : Channel
    {
        public HeartBeatChannel(Session session,
                                Authorization auth,
                                string transport,
                                string address,
                                string port) :
                base(session, auth, transport, address, port, ZSocketType.REP)
        {
        }

        public override void loop()
        {
            while (!session.request_quit)
            {
                try
                {

                    ZFrame message = socket.ReceiveFrame();
                    socket.SendFrame(message);
                }
                catch
                {
                    break; // all done?
                }
            }
        }
    }
}

