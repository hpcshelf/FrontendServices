
#include <mpi.h>
#include <cstdlib>
#include <omp.h>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <random>
#include <hpcshelf.h>

using namespace std;

# define numNodes 1000

int main ( int argc, char **argv );
void init ( int matrizAdj[numNodes][numNodes], int dist[numNodes][numNodes] );
int *dijkstra_distance ( int matrizAdj[numNodes][numNodes], int* distDijkstra[numNodes] );
void floyd_warshall ( int dist[numNodes][numNodes] );
void find_nearest ( int s, int e, int distanceTree[numNodes], bool vetorConexao[numNodes], int *d,   int *v );
void update_distTree ( int s, int e, int mv, bool vetorConexao[numNodes], int matrizAdj[numNodes][numNodes], 
  int distanceTree[numNodes] );


int main ( int argc, char **argv )
{
  char message[256];

  int i;
  double start,stop;
  int id;
  int p;
  int j;
  int *distanceTree;
  int ierr;
  int matrizAdj[numNodes][numNodes];
  int dist[numNodes][numNodes];
  int* distDijkstra[numNodes];
  int i4_huge = 2147483647;

//
//  Inicia o MPI e cria o grafo com pesos aleatorios
//
  ierr =  MPI_Init(&argc, &argv);
  init ( matrizAdj, dist );
  if ( ierr != 0 )
  {
    cout << "\n";
    cout << "  Fatal error!\n";
    cout << "  MPI_Init returned nonzero ierr.\n";
    exit ( 1 );
  }
  
  ierr = MPI_Comm_size ( MPI_COMM_WORLD, &p );
  ierr = MPI_Comm_rank ( MPI_COMM_WORLD, &id );
  
 // HPCShelf_Browse("TESTE");
  
  if(id == 0){
  	cout << "\n  MPI - numero de processos:" << p << "\n";
  }
  
  if(p > 1)
  {
	  if ( id < p/2 ) 
	  {
		  start = omp_get_wtime();
		  distanceTree = dijkstra_distance ( matrizAdj , distDijkstra);
		  stop = omp_get_wtime();
		  if(id == 0)
		  {
			  cout << "\n  Tempo Djikstra:" << stop-start;
			  cout << "\n";			  
			  sprintf(message, "Tempo Djikstra: %f", stop-start);
			  HPCShelf_Browse(message);			  
		  }
		  delete [] distanceTree;
	  }
	  if ( id >= p/2) 
	  {
		  start = omp_get_wtime();
		  floyd_warshall ( dist );
		  stop = omp_get_wtime();
		  if(id == p-1)
		  {
			  cout << "\n  Tempo Floyd Warshall:" << stop-start;
			  cout << "\n";			  
			  sprintf(message, "Tempo Floyd Warshall: %f", stop-start);
			  HPCShelf_Browse(message);			  
		  }
	  }
  }
  else
  {
  	  start = omp_get_wtime();
	  distanceTree = dijkstra_distance ( matrizAdj,distDijkstra );
	  stop = omp_get_wtime();
	  cout << "\n  Tempo Djikstra:" << stop-start;
	  cout << "\n";			  
	  sprintf(message, "Tempo Djikstra: %f", stop-start);
	  HPCShelf_Browse(message);			  
	  
	  start = omp_get_wtime();
	  floyd_warshall ( dist );
	  stop = omp_get_wtime();
	  cout << "\n  Tempo Floyd Warshall:" << stop-start;
	  cout << "\n";
	  sprintf(message, "Tempo Floyd Warshall: %f", stop-start);
	  HPCShelf_Browse(message);	
	  		  
	  delete [] distanceTree;
  }
  
  HPCShelf_Browse("done");
  
  MPI_Finalize();

  return 0;
}


int *dijkstra_distance ( int matrizAdj[numNodes][numNodes], int* distDijkstra[numNodes]  )
{
  bool *vetorConexao;
  int m;
  int i;
  int i4_huge = 2147483647;
  int md;
  int *distanceTree;
  int mv;
  int inicioThread;
  int my_id;
  int fimThread;
  int my_md;
  int my_mv;
  int my_step;
  int numThreads;
//
//  Inicializa a arvore de conexões e distancia.
//
  vetorConexao = new bool[numNodes];
  distanceTree = new int[numNodes];
  
  for(m = 0; m < numNodes; m++)
  {
	  vetorConexao[m] = true;
	  for ( i = 0; i < numNodes; i++ )
	  { 
	    if (i != m){
	    	vetorConexao[i] = false;
	    }
	    distanceTree[i] = matrizAdj[m][i];
	  }
	  
	  # pragma omp parallel private ( inicioThread, my_id, fimThread, my_md, my_mv, my_step ) \
	  shared ( vetorConexao, md, distanceTree, mv, numThreads, matrizAdj )
	  {
	    my_id = omp_get_thread_num ( );
	    numThreads = omp_get_num_threads ( ); 
	    inicioThread =   (   my_id       * numNodes ) / numThreads;
	    fimThread  =   ( ( my_id + 1 ) * numNodes ) / numThreads - 1;
//
//  O thread que chegar primeiro vai exibir a mensagem e depois todos irão dizer os nós que serão verificados
//
/*	    # pragma omp single
	    {
	      cout << "\n";
	      cout << "  P" << my_id
		   << ": Iniciando região paralela com " << numThreads << " threads.\n";
	      cout << "\n";
	    }
	    cout << "  P" << my_id
		 << ":  Inicio=" << inicioThread
		 << "  Fim=" << fimThread << "\n";
*/

	    for ( my_step = 1; my_step < numNodes; my_step++ )
	    {
//
//  Reseta as variaveis compartilhadas antes do teste.  Apenas um thread executa esta operação
//
	      # pragma omp single 
	      {
			md = i4_huge;
			mv = -1; 
	      }
//
//  Cada thread encontra o nó mais próximo da sua parte do grafo
//
		  find_nearest ( inicioThread, fimThread, distanceTree, vetorConexao, &my_md, &my_mv );
//
//  Critical é usado para garantir que apenas uma thread execute o trecho por vez, garantindo o menor valor no final
//
	      # pragma omp critical
	      {
			if ( my_md < md )  
			{
			  md = my_md;
			  mv = my_mv;
			}
	      }
//
//  Barrier usado para aguardar que todos os threads tenham passado pelo Critical e em seguida é usado para garantir que o programa
//  continue com o valor correto no vetor vetorConexao
//
	      # pragma omp barrier
	      
	      # pragma omp single 
	      {
			if ( mv != - 1 )
			{
			  vetorConexao[mv] = true;
				  /*
			  cout << "  P" << my_id
				   << ": Conectando nó " << mv << "\n";;
					*/
			}
	      }
	      
	      # pragma omp barrier
//
//  Cada thread atualiza seu valor no vetor distanceTree e novamente barrier é usado para esperar o vetor estar atualizado
//
	      if ( mv != -1 )
	      {
			update_distTree ( inicioThread, fimThread, mv, vetorConexao, matrizAdj, distanceTree );
	      }
	      #pragma omp barrier
	    }
    /*# pragma omp single
    {
      cout << "\n";
      cout << "  P" << my_id
           << ": Saindo da região paralela Djikstra.\n";
    }*/
      }

    distDijkstra[m] = distanceTree;
  }

  delete [] vetorConexao;

  return distanceTree;
}


void find_nearest ( int s, int e, int distanceTree[numNodes], bool vetorConexao[numNodes], int *d, 
  int *v )
{
  int i;
  int i4_huge = 2147483647;

  *d = i4_huge;
  *v = -1;
  for ( i = s; i <= e; i++ )
  {
    if ( !vetorConexao[i] && distanceTree[i] < *d )
    {
      *d = distanceTree[i];
      *v = i;
    }
  }
  return;
}


void floyd_warshall(int dist[numNodes][numNodes]) {
    int i, j, k;
 
    for (k = 0; k < numNodes; ++k)
//  Utiliza o OpenMP para paralelizar o for, acelerando a execução do algoritmo
        #pragma omp parallel for private(i,j)
        for (i = 0; i < numNodes; ++i)
            for (j = 0; j < numNodes; ++j)
                if ((dist[i][k] * dist[k][j] != 0) && (i != j))
                    if ((dist[i][k] + dist[k][j] < dist[i][j]) || (dist[i][j] == 0))
                        dist[i][j] = dist[i][k] + dist[k][j];
  return;
        
}

void init ( int matrizAdj[numNodes][numNodes], int dist[numNodes][numNodes] )
{
  int i;
  int i4_huge = 2147483647;
  int j;

  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution(0, numNodes);

  for ( i = 0; i < numNodes; i++ )  
  {
    for ( j = 0; j < numNodes; j++ )
    {
      if ( i == j )
      {
        matrizAdj[i][i] = 0;
        dist[i][i] = 0;
      }
      else
      {
        matrizAdj[i][j] = distribution(generator);
        dist[i][j] = matrizAdj[i][j];
      }
    }
  }
  return;
}

void update_distTree ( int s, int e, int mv, bool vetorConexao[numNodes], int matrizAdj[numNodes][numNodes], 
  int distanceTree[numNodes] )
{
  int i;
  int i4_huge = 2147483647;

  for ( i = s; i <= e; i++ )
  {
    if ( !vetorConexao[i] )
    {
      if ( matrizAdj[mv][i] < i4_huge )
      {
        if ( distanceTree[mv] + matrizAdj[mv][i] < distanceTree[i] )  
        {
          distanceTree[i] = distanceTree[mv] + matrizAdj[mv][i];
        }
      }
    }
  }
  return;
}



