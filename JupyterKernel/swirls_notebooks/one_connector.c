#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "mpi.h"
#include "hpcshelf.h"

int main(int argc, char *argv[]) 
{
	  int rank;

	  MPI_Init(&argc, &argv);

	  MPI_Comm comm;
	  MPI_Comm_get_parent(&comm);
	  MPI_Comm_rank(comm, &rank);

	  char message[256];	

	  int facet;
      HPCShelf_Get_facet(0, &facet);
      sprintf(message, "%d: GET FACET -> facet = %d", rank, facet); 
      HPCShelf_Browse(message);
      
      int facet_count;
      HPCShelf_Get_facet_count(0, &facet_count);
      sprintf(message, "%d: GET FACET_COUNT -> facet_count = %d", rank, facet_count); 
      HPCShelf_Browse(message);

      int* facet_size = (int*) malloc(facet_count * sizeof(int));
      HPCShelf_Get_facet_size(0, facet_size);
      sprintf(message, "%d: GET FACET_SIZE -> facet_size[%d] = %d", rank, facet, *(facet_size + facet)); 
      HPCShelf_Browse(message);
      
      int* facet_instance = (int*) malloc(facet_count * sizeof(int));
      HPCShelf_Get_facet_instance(0, facet_instance);
      sprintf(message, "%d: GET FACET_INSTANCE -> facet_instance[%d] = %d", rank, facet, *(facet_instance + facet)); 
      HPCShelf_Browse(message);

	  int next_facet_ = facet == facet_count-1 ? 0 : facet + 1; 
	  int prev_facet_ = facet == 0 ? facet_count-1 : facet - 1; 
	  
	  int this_facet = *(facet_instance + facet);
	  int next_facet = *(facet_instance + next_facet_);
	  int prev_facet = *(facet_instance + prev_facet_);

	  sprintf(message, ">>>>>>>> RANK = %d, FACET = %d, THIS_FACET=%d, NEXT_FACET=%d, PREV_FACET=%d *************   \n", rank, facet, this_facet, next_facet, prev_facet);
	  HPCShelf_Browse(message);
	  
	  MPI_Barrier(MPI_COMM_WORLD);
	  
	  int value = this_facet;
	  for (int i=0; i<facet_count; i++)
	  {	  
	      sprintf(message,"BEGIN process %d at facet %d sending %d to %d at facet %d *** \n", rank, this_facet, value, rank, next_facet);
		  HPCShelf_Browse(message);
		  HPCShelf_Send(0, &value, 1, MPI_INT, next_facet, rank, 0); 
		  sprintf(message, "END process %d at facet %d sending %d to %d at facet %d *** \n", rank, this_facet, value, rank, next_facet);
		  HPCShelf_Browse(message);
		  sprintf(message, "process %d at facet %d receiving from %d at facet %d *** \n", rank, this_facet, rank, prev_facet);
		  HPCShelf_Browse(message);
		  int new_value;
		  HPCShelf_Recv(0, &new_value, 1, MPI_INT, prev_facet, rank, 0);
		  sprintf(message, "process %d at facet %d received %d from %d at facet %d 	\n", rank, this_facet, new_value, rank, prev_facet);
		  HPCShelf_Browse(message);
		  value = new_value;
	  }
	  
	  HPCShelf_Browse("done");
	  
	  free(facet_size);
	  free(facet_instance);
	  
	  MPI_Finalize();
}

