from mpi4py import MPI


def browse(*messages):
   comm = MPI.Comm.Get_parent()
   rank = dest=comm.Get_rank()
   
   message=str(rank) + ': '
   for m in messages:
      message = message + ' ' + str(m)
   
   comm.send(len(message), dest=rank, tag=111)
   comm.send(message, dest=rank, tag=222)



comm = MPI.COMM_WORLD
rank = comm.Get_rank()

browse ('hello world from process ', rank)
browse ('done')
