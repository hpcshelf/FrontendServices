# HPC Processador de Imagens Paralelas

# Motivação
Já precisou processar uma grande imagem e não conseguiu fazer em tempo hábil? Com o auxilio do OpenMP e do MPI,
agora podemos otimizar o desempenho das suas operações e expandir seu processamento com outras máquinas

# Instruções

# 1. openmp_example.c
Neste arquivo, foi exibido o processamento de uma imagem 16K, isso mesmo, 16K utilizando unicamente OpenMP, para compilar, utilize o comando:


gcc -Wall -fopenmp -o openmp_example openmp_example.c -lm

finalmente, para executa, use 


OMP_NUM_THREADS=8 ./openmp_example

# 2. mpi_image

Assim como no primeiro exemplo, é realizado o mesmo processamento de imagem, dessa vez, utilizando o MPI, ou seja, agora é possível expandir a quantidade de clusters que podem fazer parte da computação, vale lembrar que é utilizada a mesma função de filtro com OpenMP

para compilar, utilize


mpicc -Wall -fopenmp -o mpi_image mpi_image.c -lm


para rodar, utilize


mpirun -np 4 ./mpi_image


# Demonstração

Quem sabe faz ao [vivo](https://drive.google.com/file/d/1xTdzTxqEkZOEW1y6WADlURDxaQdFbAro/view?usp=sharing)! 