#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#include "hpcshelf.h"


int main(int argc, char *argv[]) 
{
  int rank;

  MPI_Init(&argc, &argv);

  MPI_Comm comm;
  MPI_Comm_get_parent(&comm);
  MPI_Comm_rank(comm, &rank);
  
  run_forward (rank);
  run_backward(rank);  
  
  HPCShelf_Browse("done");
  	  
  MPI_Finalize();
}


void run_forward(int rank)
{
	  int facet;
	  HPCShelf_Get_facet(0, &facet);

	  int facet_count;
	  HPCShelf_Get_facet_count(0, &facet_count);

	  int* facet_size = (int*) malloc(facet_count * sizeof(int));
	  HPCShelf_Get_facet_size(0, facet_size);

	  int* facet_instance = (int*) malloc(facet_count * sizeof(int));
	  HPCShelf_Get_facet_instance(0, facet_instance);

	  int next_facet_ = facet == facet_count-1 ? 0 : facet + 1; 
	  int prev_facet_ = facet == 0 ? facet_count-1 : facet - 1; 
	  
	  int this_facet = *(facet_instance + facet);
	  int next_facet = *(facet_instance + next_facet_);
	  int prev_facet = *(facet_instance + prev_facet_);

	  char message[256];	
	  
	  sprintf(message, ">>>>>>>> FORWARD RANK = %d, FACET = %d, THIS_FACET=%d, NEXT_FACET=%d, PREV_FACET=%d *************   ", rank, facet, this_facet, next_facet, prev_facet);
	  HPCShelf_Browse(message);
	  int value = this_facet;
	  for (int i=0; i<facet_count; i++)
	  {
		  sprintf(message, "FORWARD BEGIN process %d at facet %d sending %d to %d at facet %d *** ", rank, this_facet, value, rank, next_facet);
	      HPCShelf_Browse(message);
		  HPCShelf_Send(0, &value, 1, MPI_INT, next_facet, rank, 0);  
		  sprintf(message, "FORWARD END process %d at facet %d sending %d to %d at facet %d *** ", rank, this_facet, value, rank, next_facet);
	      HPCShelf_Browse(message);
		  sprintf(message, "FORWARD process %d at facet %d receiving from %d at facet %d *** ", rank, this_facet, rank, prev_facet);
	      HPCShelf_Browse(message);
		  int new_value;
		  HPCShelf_Recv(0, &new_value, 1, MPI_INT, prev_facet, rank, 0);
		  sprintf(message, "FORWARD process %d at facet %d received %d from %d at facet %d *** count=%d", rank, this_facet, new_value, rank, prev_facet, count);
	      HPCShelf_Browse(message);
		  value = new_value;
	  }
	  
  	  free(facet_size);
	  free(facet_instance);
}


void run_backward(int rank)
{
	  int facet;
	  HPCShelf_Get_facet(1, &facet);

	  int facet_count;
	  HPCShelf_Get_facet_count(1, &facet_count);

	  int* facet_size = (int*) malloc(facet_count * sizeof(int));
	  HPCShelf_Get_facet_size(1, facet_size);

	  int* facet_instance = (int*) malloc(facet_count * sizeof(int));
	  HPCShelf_Get_facet_instance(1, facet_instance);

	  int prev_facet_ = facet == facet_count-1 ? 0 : facet + 1; 
	  int next_facet_ = facet == 0 ? facet_count-1 : facet - 1; 
	  
	  int this_facet = *(facet_instance + facet);
	  int next_facet = *(facet_instance + next_facet_);
	  int prev_facet = *(facet_instance + prev_facet_);

	  char message[256];	
	  
	  sprintf(message, ">>>>>>>> BACKWARD RANK = %d, FACET = %d, THIS_FACET=%d, NEXT_FACET=%d, PREV_FACET=%d *************   ", rank, facet, this_facet, next_facet, prev_facet);
      HPCShelf_Browse(message);
	  
	  int value = this_facet;
	  for (int i=0; i<facet_count; i++)
	  {
		  sprintf(message, "BACKWARD BEGIN process %d at facet %d sending %d to %d at facet %d *** ", rank, this_facet, value, rank, next_facet);
	      HPCShelf_Browse(message);
		  HPCShelf_Send(1, &value, 1, MPI_INT, next_facet, rank, 0);  
		  sprintf(message, "BACKWARD END process %d at facet %d sending %d to %d at facet %d *** ", rank, this_facet, value, rank, next_facet);
	      HPCShelf_Browse(message);
		  sprintf(message, "BACKWARD process %d at facet %d receiving from %d at facet %d *** ", rank, this_facet, rank, prev_facet);
	      HPCShelf_Browse(message);
		  int new_value;
		  HPCShelf_Recv(1, &new_value, 1, MPI_INT, prev_facet, rank, 0);
		  sprintf(message, "BACKWARD process %d at facet %d received %d from %d at facet %d *** count=%d", rank, this_facet, new_value, rank, prev_facet, count);
	      HPCShelf_Browse(message);
		  value = new_value;
	  }
	  
  	  free(facet_size);
	  free(facet_instance);
}

