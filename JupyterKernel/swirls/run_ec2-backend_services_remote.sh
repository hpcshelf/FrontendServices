#!/bin/bash
echo "STARTING - EC2 Backend at $2"
ssh-keygen -f "~/.ssh/known_hosts" -R $2
ssh -i $1 -oStrictHostKeyChecking=no ubuntu@$2 uptime
scp -i $1 ~/.aws/config  ubuntu@$2:.aws/config
scp -i $1 ~/.aws/credentials  ubuntu@$2:.aws/credentials
nohup ssh -i $1 ubuntu@$2 "/home/ubuntu/swirls/run_ec2-backend_services_local.sh" &
echo "STARTED - EC2 Backend at $2"
