#!/bin/bash
cd $HPCShelf_HOME/BackendServices/
git reset --hard HEAD
git pull
./compile_DGAC.sh
./compile_worker.sh
./compile_PlatformServices.sh
./compile_BackendWebServices.sh
cd BackendServices
screen -dm bash -c './run_backend_services_local_no_kill.sh'
