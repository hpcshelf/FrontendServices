#!/bin/bash

mkdir -p ~/.local/share/jupyter/kernels/hpcshelf
START_SCRIPT_PATH=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)/swirls.exe
MONO_PATH=$(which mono)
CONTENT='{
   "argv": ["'${MONO_PATH}'", "'${START_SCRIPT_PATH}'", "{connection_file}"],
                "display_name": "Swirls",
                "language": "swirls"
}'
echo $CONTENT > ~/.local/share/jupyter/kernels/hpcshelf/kernel.json
#echo $CONTENT > ~/.ipython/kernels/hpcshelf/kernel.json
