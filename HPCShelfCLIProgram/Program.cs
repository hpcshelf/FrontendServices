﻿using System;
using System.ServiceModel;

namespace HPCShelfCLIProgram
{
    class MainClass
    {
		[ServiceContract]
		interface ICLIServices
		{
			[OperationContract]
			string run_command(string[] args);
		}
		
        public static void Main(string[] args)
        {
            ICLIServices s = Get("localhost", 8074);
            string return_message = s.run_command(args);
            Console.WriteLine(return_message);
        }

		private static ICLIServices Get(string strServer, int nPort)
		{
			ICLIServices mathSvcObj = null;

			ChannelFactory<ICLIServices> channelFactory = null;
			EndpointAddress ep = null;

			BasicHttpBinding httpb = new BasicHttpBinding();
			httpb.OpenTimeout = new TimeSpan(0, 10, 0);
			httpb.CloseTimeout = new TimeSpan(0, 10, 0);
			httpb.SendTimeout = new TimeSpan(0, 10, 0);
			httpb.ReceiveTimeout = new TimeSpan(0, 10, 0);
			channelFactory = new ChannelFactory<ICLIServices>(httpb);

			// End Point Address
			string strEPAdr = "http://" + strServer + ":" + nPort.ToString() + "/CLIService";
			try
			{
				// Create End Point
				ep = new EndpointAddress(strEPAdr);

				// Create Channel
				mathSvcObj = channelFactory.CreateChannel(ep);

				//channelFactory.Close();
			}
			catch (Exception eX)
			{
				// Something unexpected happended .. 
				Console.WriteLine("Error while performing operation [" +
				  eX.Message + "] \n\n Inner Exception [" + eX.InnerException + "]");
			}

			return mathSvcObj;
		}
	}
}
