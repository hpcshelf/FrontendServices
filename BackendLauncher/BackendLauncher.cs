﻿using org.hpcshelf.core;

namespace BackendLauncher
{
    public interface IBackendLauncher
    {
        string BackendAddress { get; }
        string launch(string name, string region_id, CoreServices core_services);
        void stop();
        string start();
        void terminate();
    }
}
