﻿using System;
using System.Threading;
using org.hpcshelf.backend.platform;
using org.hpcshelf.core;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.unit;
using org.hpcshelf.kinds;
using gov.cca;
using gov.cca.ports;

namespace org.hpcshelf.SAFe
{
    public class HShelfApplication 
	{
		protected class HShelfFramework : AbstractFramework
        {
			private string system_contract = null;
			private WorkerObject baseFramework = null;

			private CoreServices core_services;

			private IUnit application_unit = null;

			private string app_name = null;

            public HShelfFramework (string app_name, string system_contract)
			{
                this.system_contract = system_contract;
                this.app_name = app_name;
				init();
			}

			private void init()
			{

                core_services = new CoreServices(app_name, out _);
				core_services.open(system_contract);

				core_services.resolve (Constants.PLATFORM_SAFE_ID);
                core_services.resolve (Constants.APPLICATION_COMPONENT_ID);
				core_services.resolve (Constants.WORKFLOW_COMPONENT_ID);

			//	core_services.certify(Constants.WORKFLOW_COMPONENT_ID);

                core_services.deploy (Constants.PLATFORM_SAFE_ID);
				core_services.deploy (Constants.APPLICATION_COMPONENT_ID);
				core_services.deploy (Constants.WORKFLOW_COMPONENT_ID);

				core_services.instantiate (Constants.PLATFORM_SAFE_ID);
				core_services.instantiate (Constants.APPLICATION_COMPONENT_ID);
                core_services.instantiate(Constants.WORKFLOW_COMPONENT_ID);

                // baseFramework =  (WorkerObject)ManagerObject.SingleManagerObject.WorkerFramework;
                baseFramework = (WorkerObject)((ManagerObject)core_services.Framework).WorkerFramework;

                Services appServices = baseFramework.getServices(this.app_name + ".shelf_framework", "HShelfFramework", baseFramework.createTypeMap());

				appServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
				ComponentID host_cid = appServices.getComponentID();
				BuilderService bsPort = (BuilderService)appServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

				ComponentID workflow_cid = bsPort.getComponentID(this.app_name + "." + Constants.WORKFLOW_COMPONENT_ID);
				ComponentID core_cid = core_services.Services.getComponentID();
				bsPort.connect(workflow_cid, Constants.CORE_PROVENANCE_PORT_NAME, core_cid, Constants.CORE_PROVENANCE_PORT_NAME);
				bsPort.connect(workflow_cid, Constants.CORE_LIFECYCLE_PORT_NAME, core_cid, Constants.CORE_LIFECYCLE_PORT_NAME);

				//	baseFramework = (WorkerObject)((ManagerObject)platform_services.Session.Framework).WorkerFramework;
				//	Console.WriteLine(this.app_name + ".workflow");
				//	foreach (string k in baseFramework.UnitOf.Keys)
				//		Console.WriteLine("UnitOf[{0}] !", k);
				//	((IWorkflowKind)(baseFramework.UnitOf[this.app_name + ".workflow"])).CoreServices = core_services;
			}

			public TypeMap createTypeMap()
			{
				return baseFramework.createTypeMap ();
			}

			public Services getServices(string selfInstanceName, string selfClassName, TypeMap selfProperties)
            {
				application_unit = baseFramework.UnitOf[this.app_name + "." + Constants.APPLICATION_COMPONENT_ID];

				return application_unit.Services;
			}


			public void releaseServices(Services services)
			{
				baseFramework.releaseServices (services);
			}

			public void shutdownFramework()
			{
				baseFramework.shutdownFramework ();
			}

			public AbstractFramework createEmptyFramework()
			{
				return new HShelfFramework("framework", system_contract);
			}
		}

	}
}

