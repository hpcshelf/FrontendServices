﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using BackendLauncher;
using org.hpcshelf.backend;
using org.hpcshelf.backend.ec2;
using org.hpcshelf.command;
using org.hpcshelf.core;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;

public class EC2BackendLauncher : IBackendLauncher
{
    private string name = null;
    private CoreServices core_services = null;

    public string launch(string name, string region_id, CoreServices core_services)
    {
        this.name = name;
        this.core_services = core_services;

        if (!region_id.Equals("local"))
        {
            // launch backend service.
            launchEC2Instance(name, region_id);

            // configure environment
            launchEC2BackendService();

            // create abstract qualifier component.
            ComponentType c_abstract = createAbstractQualifier(name);

            // create concrete qualifier component.
            ComponentType c_concrete = createConcreteQualifier(name, backend_address);

            deployComponent(c_abstract, core_services);
            deployComponent(c_concrete, core_services);
        }
        else
            launchEC2BackendServiceLocally();

        return backend_address;
    }

    private string launchEC2BackendServiceLocally()
    {
        string script_command = Path.Combine(SWIRLS_HOME, "run_ec2-backend_services_local.sh");

        Utils.commandExecBash(string.Format("{0}", script_command));

        platform_address = "127.0.0.1";

        return (backend_address = string.Format("http://{0}:8077/BackendServices.asmx", platform_address));

    }

    private string SWIRLS_HOME
    {
        get
        {
            string path = Environment.GetEnvironmentVariable("Swirls_HOME");
            if (path == null)
            {
                Console.WriteLine("Swirls_HOME not set. Using the current directory.");
                path = Utils.CURRENT_FOLDER;
            }
            return path;
        }
    }


    private void launchEC2BackendService()
    {
        // DEFAULT_AMI_ID and run_ec2-backend_service_local.sh are placed at the platform.
        // 1. copy the user credentials ("scp -i $1 ~/.aws/config  ubuntu@$2:.aws/config", "scp -i $1 ~/.aws/credentials  ubuntu@$2:.aws/credentials").
        // 2. launch the service ("nohup rsh -i ~/hpc-shelf-credential.pem ubuntu@$1 "bash ~/run_ec2-backend_service.sh" &").


        string script_command = Path.Combine(SWIRLS_HOME, "run_ec2-backend_services_remote.sh");

        Utils.commandExecBash(string.Format("{0} {1} {2}", script_command, credential_file_name, platform_address));
    }

    public void stop()
    {
        // launch backend service.
        stopBackendService();
    }

    public string start()
    {
        // launch backend service.
        startBackendService();

        // create abstract qualifier component.
        ComponentType c_abstract = createAbstractQualifier(name);

        // create concrete qualifier component.
        ComponentType c_concrete = createConcreteQualifier(name, backend_address);

        deployComponent(c_abstract, core_services);
        deployComponent(c_concrete, core_services);

        return backend_address;
    }

    private string startBackendService()
    {
        if (region == null)
            throw new Exception("The operation 'start' does not apply to a local EC2 backend");

        InstancesManager.StartInstance(instanceId, client);

        string status_check;
        do
        {
            status_check = InstancesManager.Instance_statusCheck(region, instanceId);
            Console.WriteLine("Waiting to start {0} -- {1}", instanceId, status_check);
            Thread.Sleep(2000);

        }
        while (!status_check.Equals("ok"));

        IList<IVirtualMachine> VirtualMachines = new List<IVirtualMachine>();
        DataRegistry(client, instanceId, VirtualMachines);
        platform_address = VirtualMachines.ElementAt(0).PublicIpAddress;

        return (backend_address = string.Format("http://{0}:8077/BackendServices.asmx", platform_address));

    }

    private void stopBackendService()
    {
        if (region == null)
            throw new Exception("The operation 'stop' does not apply to a local EC2 backend");

        InstancesManager.StopInstance(instanceId, client);

      /*  string status_check;
        do
        {
            status_check = InstancesManager.Instance_statusCheck(region, instanceId);
            Console.WriteLine("Waiting to stop {0} -- {1}", instanceId, status_check);
            Thread.Sleep(2000);

        }
        while (!status_check.Equals("ok"));
        */
        backend_address = "<stopped>";
    }

    public void terminate()
    {
        InstancesManager.TerminateInstance(instanceId, client);
    }


    private string backend_address = "<pending>";
    private string platform_address = null;
    private InstanceType instanceId = null;
    private AmazonEC2Client client = null;
    private RegionEndpoint region = null;
    private string credential_file_name = null;

    public string BackendAddress { get { return backend_address; } }

    private string EC2_BACKEND_IMAGE_ID { get { return File.ReadAllLines(Path.Combine(SWIRLS_HOME, "EC2_BACKEND_IMAGE_ID"))[0]; } }

    private string launchEC2Instance(string qname, string region_id)
    {
        string amID = EC2_BACKEND_IMAGE_ID;
        region = RegionEndpoint.GetBySystemName(region_id);

        client = new AmazonEC2Client(region);

        string keyPairName = "hpc-shelf-credential";
        credential_file_name = Path.Combine(SWIRLS_HOME, string.Format("{0}.pem", keyPairName));
        InstancesManager.CreateKeyPair(client, keyPairName, credential_file_name);

        string instance_type = InstanceType.T2Micro.ToString();

        string securityGroupName = "hpc-shelf";

        instanceId = InstancesManager.LaunchInstance(instance_type, amID, keyPairName, securityGroupName, client);

        string status_check;
        do
        {
            status_check = InstancesManager.Instance_statusCheck(region, instanceId);
            Console.WriteLine("Waiting to launch {0} -- {1}", instanceId, status_check);
            Thread.Sleep(2000);

        }
        while (!status_check.Equals("ok"));

        IList<IVirtualMachine> VirtualMachines = new List<IVirtualMachine>();
        DataRegistry(client, instanceId, VirtualMachines);
        platform_address = VirtualMachines.ElementAt(0).PublicIpAddress;

        return (backend_address = string.Format("http://{0}:8077/BackendServices.asmx", platform_address)); 
    }

    protected void DataRegistry(AmazonEC2Client client, string instanceId, IList<IVirtualMachine> VirtualMachines)
    {
        //lock (lock_1)
        {
            DescribeInstancesRequest req = new DescribeInstancesRequest()
            {
                Filters = new List<Filter>()
                {
                    new Filter()
                    {
                        Name = "instance-id",
                        Values = new List<String>() { instanceId }
                    }
                }
            };
            IVirtualMachine vm = null;
            while (vm == null || vm.PublicIpAddress == null)
            {
                List<Reservation> result = client.DescribeInstances(req).Reservations;

                foreach (Amazon.EC2.Model.Reservation reservation in result)
                {
                    foreach (Instance runningInstance in reservation.Instances)
                    {
                        vm = new VirtualMachine
                        {
                            InstanceId = runningInstance.InstanceId,
                            PrivateIpAddress = runningInstance.PrivateIpAddress,
                            PublicIpAddress = runningInstance.PublicIpAddress
                        };
                    }
                }
                if (vm == null || vm.PublicIpAddress == null)
                    Thread.Sleep(1000); //Wait 1s for aws release public IP address
            }
            VirtualMachines.Add(vm);
        }
    }

    private void deployComponent(ComponentType c, CoreServices core_services)
    {
        string module_data_out_abstract = LoaderApp.serialize<ComponentType>(c);

        File.WriteAllText(c.header.name + ".hpe", module_data_out_abstract);

        string snk_fn = c.header.name + ".snk";
        string pub_fn = c.header.name + ".pub";

        byte[] snk_contents = File.ReadAllBytes(snk_fn);

        core_services.register(module_data_out_abstract, snk_contents);

        File.Delete(snk_fn);
        File.Delete(pub_fn);
    }

    private ComponentType createAbstractQualifier(string qname)
    {
        string packagePath = qname.Substring(0, qname.LastIndexOf('.'));
        string name = qname.Substring(qname.LastIndexOf('.') + 1);

        string snk_fn_abstract = name + ".snk";
        string pub_fn_abstract = name + ".pub";

        string uid_abstract = FileUtil.create_snk_file(snk_fn_abstract, pub_fn_abstract);

        ComponentType c = new ComponentType
        {
            header = new ComponentHeaderType
            {
                hash_component_UID = uid_abstract,
                isAbstract = true,
                isAbstractSpecified = true,
                kind = SupportedKinds.Environment,
                kindSpecified = true,
                name = name,
                packagePath = packagePath,
                baseType = new BaseTypeType()
                {
                    extensionType = new ExtensionTypeType
                    {
                        ItemElementName = ItemChoiceType.extends,
                        Item = true
                    },
                    component = new ComponentInUseType
                    {
                        localRef = "base",
                        location = "org.hpcshelf.platform.maintainer.EC2/EC2.hpe",
                        name = "EC2",
                        package = "org.hpcshelf.platform.maintainer",
                        version = "1.0.0.0",
                        unitBounds = new UnitBoundsType[]
                        {
                            new UnitBoundsType
                            {
                                facet = 0,
                                facet_instanceSpecified = true,
                                facet_instance = 0,
                                facet_instance_enclosing = 0,
                                parallelSpecified = true,
                                parallel = true,
                                uRef = "ec2"
                            }
                        }
                    }
                },
                versions = new VersionType[]
                {
                    new VersionType
                    {
                        field1Specified = true, field1 = 1,
                        field2Specified = true, field2 = 0,
                        field3Specified = true, field3 = 0,
                        field4Specified = true, field4 = 0
                    }
                },
                facet_configuration = new FacetConfigurationType[]
                {
                    new FacetConfigurationType
                    {
                        facet = 0, facetSpecified = true,
                        multiple = false, multipleSpecified = true
                    }
                }
            },
            componentInfo = new object[]
            {
                new InterfaceType
                {
                    iRef = string.Format("I{0}",name),
                    nArgs =0, nArgsSpecified = true
                    //sources = new SourceType[] {}
                },

                new UnitType
                {
                    facet = 0,
                    iRef = string.Format("I{0}",name),
                    multiple = true, multipleSpecified = true,
                    @private = false, privateSpecified = false,
                    replica = 0, replicaSpecified = true,
                    uRef = "ec2",
                    visibleInterface = true,
                    super = new UnitRefType[1]
                    {
                        new UnitRefType
                        {
                            cRef = "base",
                            slice_replica = 0,
                            uRef = "ec2"
                        }

                    }
                }
            }
        };

        return c;
    }

    private ComponentType createConcreteQualifier(string qname, string backend_address)
    {
        string packagePath_abstract = qname.Substring(0, qname.LastIndexOf('.'));
        string packagePath_concrete = string.Format("{0}.impl", packagePath_abstract);

        string name_abstract = qname.Substring(qname.LastIndexOf('.') + 1);
        string name_concrete = string.Format("{0}_impl", name_abstract);

        string snk_fn_concrete = string.Format("{0}.snk", name_concrete);
        string pub_fn_concrete = string.Format("{0}.pub", name_concrete);

        string uid_concrete = FileUtil.create_snk_file(snk_fn_concrete, pub_fn_concrete);

        ComponentType c = new ComponentType
        {
            header = new ComponentHeaderType
            {
                hash_component_UID = uid_concrete,
                isAbstract = false,
                isAbstractSpecified = true,
                kind = SupportedKinds.Environment,
                kindSpecified = true,
                name = name_concrete,
                packagePath = packagePath_concrete,
                baseType = new BaseTypeType()
                {
                    extensionType = new ExtensionTypeType
                    {
                        ItemElementName = ItemChoiceType.implements,
                        Item = true
                    },
                    component = new ComponentInUseType
                    {
                        localRef = "base",
                        location = string.Format("{0}.{1}/{1}.hpe", packagePath_abstract, name_abstract),
                        name = name_abstract,
                        package = packagePath_abstract,
                        version = "1.0.0.0",
                        unitBounds = new UnitBoundsType[]
                        {
                            new UnitBoundsType
                            {
                                facet = 0,
                                facet_instanceSpecified = true,
                                facet_instance = 0,
                                facet_instance_enclosing = 0,
                                parallelSpecified = true,
                                parallel = true,
                                uRef = name_abstract.ToLower()
                            }
                        }
                    }
                },
                versions = new VersionType[]
                {
                    new VersionType
                    {
                        field1Specified = true, field1 = 1,
                        field2Specified = true, field2 = 0,
                        field3Specified = true, field3 = 0,
                        field4Specified = true, field4 = 0
                    }
                },
                facet_configuration = new FacetConfigurationType[]
                {
                    new FacetConfigurationType
                    {
                        facet = 0, facetSpecified = true,
                        multiple = false, multipleSpecified = true
                    }
                }
            },
            componentInfo = new object[]
            {
                new InterfaceType
                {
                    iRef = string.Format("I{0}", name_concrete),
                    nArgs =0, nArgsSpecified = true,
                    sources = new SourceType[]
                    {
                        new SourceType
                        {
                            deployType = "platform.settings",
                            moduleName = "platform.settings",
                            sourceType = "platform.settings",
                            file = new SourceFileType[]
                            {
                                new SourceFileType
                                {
                                    contents = backend_address,
                                    fileType = "platform.settings",
                                    name = "platform.settings",
                                    srcType = "platform.settings"
                                }
                            }
                        }
                    }
                },

                new UnitType
                {
                    facet = 0,
                    iRef = string.Format("I{0}", name_concrete),
                    multiple = true, multipleSpecified = true,
                    @private = false, privateSpecified = false,
                    replica = 0, replicaSpecified = true,
                    uRef = "ec2",
                    visibleInterface = true,
                    super = new UnitRefType[1]
                    {
                        new UnitRefType
                        {
                            cRef = "base",
                            slice_replica = 0,
                            uRef = "ec2"
                        }

                    }
                }
            }

        };

        return c;
    }

}
