﻿using System;
using System.Collections.Generic;
using HPCShelfDriver;

namespace DriverTest
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            IDictionary<string, Tuple<string, object>> contract_signature = new Dictionary<string, Tuple<string, object>>()
            {
				{"input_data_host", new Tuple<string, object>("MData", new Dictionary<string,object>() {{"name","org.hpcshelf.platform.maintainer.DataHost"}})}
			};

            ISystem system = SystemBuilder.build("test_qualifiers", contract_signature);

            IDictionary<string, object> contract_platform = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.platform.Platform"},
                {"maintainer", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.maintainer.ComputeHost"} }}
            };

            IVirtualPlatform platform = system.newVirtualPlatform("platform", contract_platform);

            // 3.6. Specifying the contract of "reducer_1" and "reducer_2".
            IDictionary<string, object> contract_computation = new Dictionary<string, object>()
            {
                {"name", "test.TestQuantifiers"},
                {"first_parameter", 16},
                {"second_parameter", 4},
                {"third_parameter", 234.11111m},
                {"fourth_parameter", -0.21m},
                {"fifth_parameter", 100.99m},
                {"sixth_parameter", 900.0m}
            };


            // 3.7. Creating the objects that represent "reducer_1" and "reducer_2".
            IComputation computation = system.newComputation("computation", contract_computation, platform);

            IActionPortLifeCycle computation_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["computation.lifecycle"][0];

            IActionPortLifeCycle platform_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["platform.lifecycle"][0];

            platform_lifecycle.resolve();
            platform_lifecycle.deploy();
            platform_lifecycle.instantiate();

            computation_lifecycle.resolve();
            computation_lifecycle.deploy();
            computation_lifecycle.instantiate();

            computation_lifecycle.run();
        }
    }
}
