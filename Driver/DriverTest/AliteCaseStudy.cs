﻿using System;
using System.Collections.Generic;
using HPCShelfDriver;

namespace DriverTest
{
    // SET GLOBAL max_heap_table_size = 1024 * 1024 * 1024 * 4;
    // SET GLOBAL tmp_table_size = 1024 * 1024 * 1024 * 4;
    // exit
    // mysql -u HPCShelfCore -p hashmodel
    // source all.sql;
    // source update_parameters.sql;

    class MainClass
    {
        public static void Main(string[] args)
        {
            IDictionary<string, Tuple<string, object>> contract_signature = new Dictionary<string, Tuple<string, object>>()
            {
                //{"input_data_host", new Tuple<string, object>("MData", new Dictionary<string,object>() {{"name","org.hpcshelf.platform.maintainer.DataHost"}})}
            };

            ISystem system = SystemBuilder.build("alite_case_study_3", contract_signature);

            IDictionary<string, object> contract_platform_A = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.platform.Platform"},
                {"node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
        //        {"node-accelerator-architecture", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.accelerator.Architecture" } } },
                {"node-count", 8}
            };

            IDictionary<string, object> contract_platform_B = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.platform.Platform"},
                {"maintainer", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.maintainer.EC2" } }},
                {"node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                {"node-count", 8},
                {"node-accelerator-model",  new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.accelerator.model.Tesla_T4" } }}
            };

            IDictionary<string, object> contract_platform_C = new Dictionary<string, object>()
            {
                {"name", "br.ufc.mdcc.backend.ec2.EC2Platform"},
                {"node-locale", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.locale.Europe" } }},
                {"node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                {"node-count", 8},
                {"node-processor-microarchitecture", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake" } } },
                {"node-accelerator-architecture", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.accelerator.Architecture" } } }
            };

            IVirtualPlatform ec2_platform_A = system.newVirtualPlatform("ec2_platform_A", contract_platform_A);
            IVirtualPlatform ec2_platform_B = system.newVirtualPlatform("ec2_platform_B", contract_platform_B);
            IVirtualPlatform ec2_platform_C = system.newVirtualPlatform("ec2_platform_C-benchmark", contract_platform_C);

            IDictionary<string, object> contract_computation_A = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.blas.level1.DOT"},
                {"data_type",new Dictionary<string, object> { { "name", "br.ufc.mdcc.common.DComplex" } }}/*,
                {"platform-node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                {"platform-node-count", 8}*/
            };

            IComputation ec2_computation_A = system.newComputation("ec2_computation_A", contract_computation_A, ec2_platform_A);

            // --------------------------------------------------------------------------------------------------------------------------------------



            IDictionary<string, object> contract_computation_B = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.blas.level2.TRMV"},
                {"data_type",new Dictionary<string, object> { { "name", "br.ufc.mdcc.common.Integer" } }},
                {"uplo", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.uplo.L" } }},
                {"diag", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.diag.U" } }},
                {"trans", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.trans.N" } }},
                //{"platform-maintainer", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.maintainer.EC2" } }},
                //{"platform-node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                //{"platform-node-count", 8},
                {"platform-node-accelerator-architecture",  new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.accelerator.architecture.Turing" } }}
            };

            IComputation ec2_computation_B = system.newComputation("ec2_computation_B", contract_computation_B, ec2_platform_B);


            // --------------------------------------------------------------------------------------------------------------------------------------


            IDictionary<string, object> contract_computation_C = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.blas.level3.GEMM"},
                {"min_M", 200000},
                {"min_N", 200000},
                {"min_P", 200000},
                {"max_M", 300000},
                {"max_N", 300000},
                {"max_P", 300000},
                {"data_type",new Dictionary<string, object> { { "name", "br.ufc.mdcc.common.DFloat" } }},
                {"matrix_type",new Dictionary<string, object> { { "name", "org.hpcshelf.blas.matrixtype.General" } }},
                {"trans_A", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.trans.N" } }},
                {"trans_B", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.trans.T" } }},
                {"uplo", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.uplo.NA" } }},
                {"diag", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.diag.NA" } }},
                {"side", new Dictionary<string, object> { { "name", "org.hpcshelf.blas.configure.side.NA" } }} /*,
                {"platform-node-locale", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.locale.Europe" } }},
                {"platform-node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                {"platform-node-count", 8},
                {"platform-node-processor-microarchitecture", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.processor.microarchitecture.intel.Skylake" } } }
                */               
            };

            IComputation ec2_computation_C = system.newComputation("ec2_computation_C", contract_computation_C, ec2_platform_C);

            // --------------------------------------------------------------------------------------------------------------------------------------


            IActionPortLifeCycle computation_lifecycle_A = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_computation_A.lifecycle"][0];
            IActionPortLifeCycle platform_lifecycle_A = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_platform_A.lifecycle"][0];

            IActionPortLifeCycle computation_lifecycle_B = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_computation_B.lifecycle"][0];
            IActionPortLifeCycle platform_lifecycle_B = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_platform_B.lifecycle"][0];

            IActionPortLifeCycle computation_lifecycle_C = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_computation_C.lifecycle"][0];
            IActionPortLifeCycle platform_lifecycle_C = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_platform_C-benchmark.lifecycle"][0];

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("=> Iteration {0}", i);
                platform_lifecycle_C.resolve_system();
                platform_lifecycle_B.resolve_system();
                platform_lifecycle_A.resolve_system();
            }

            // try { computation_lifecycle_A.resolve(); } catch (System.Web.Services.Protocols.SoapException e) { }

            //try { computation_lifecycle_B.resolve(); } catch (System.Web.Services.Protocols.SoapException e) { }

            // try { computation_lifecycle_C.resolve(); } catch (System.Web.Services.Protocols.SoapException e) { }





        }
    }
}
