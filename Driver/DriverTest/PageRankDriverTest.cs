﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using br.ufc.mdcc.common.KVPair;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBase;
using org.hpcshelf.gust.example.pgr.DataPGRANK;
using org.hpcshelf.gust.graph.VertexBasic;
using org.hpcshelf.gust.port.environment.PortTypeDataSinkGraphInterface;
using org.hpcshelf.kinds;
using HPCShelfDriver;

namespace DriverTest
{
    class MainClass
    {
        static string OUTPUT_FILE_PATH = System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) + "/gust.output";//target to folder home
        static int N = int.Parse(System.Environment.GetEnvironmentVariable("NUM_GUSTY"));

        public static void Main(string[] args)
        {
            // 1. CREATING A SYSTEM COMPONENT (i.e. component that represents the parallel computing system)

            // 1.1. Specifying the context signature of the system, as dictionary associating context parameter identifiers to a variable and a bound.
            /* The variable is a string that may be referred in contracts of components included in the system.
             * The bound is a contract. Each contract is a dictionary with at least a "name" key (mandatory), which must be associated with a string 
             * containing the qualified name of the component. The other entries associate context arguments to the component referred in "name". 
             * Each context parameter is referred by its name, and the argument may be defined by a contract or a context variable. In the system contract,
             * the bound of "input_data_host" is a contract for the component "org.hpcshelf.platform.maintainer.DataHost", which does not have any
             * context parameter. Also, it is associated to the variale "MData".
             */
            IDictionary<string, Tuple<string, object>> contract_signature = new Dictionary<string, Tuple<string, object>>()
            {
                {"input_data_host", new Tuple<string, object>("MData", new Dictionary<string,object>() {{"name","org.hpcshelf.platform.maintainer.DataHost"}})}
            };

            ISystem system = SystemBuilder.build("teste", contract_signature);

            // 2. CREATING THE "source" COMPONENT, from which the graph is obtained.

            // 2.1. Specifying the contract of the virtual platform where the source component is placed, named "platform_data_source".
            /* The "maintainer" argument is associated to the value of "MData". */
            IDictionary<string, object> contract_platform_data_source = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.platform.Platform"},
                {"maintainer", "MData"}
            };

            // 2.2. Creating an object that will represent the virtual plataform "platform_data_    source"
            IVirtualPlatform platform_data_source = system.newVirtualPlatform("platform_data_source", contract_platform_data_source);

         //  IActionPortLifeCycle platform_source_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["platform_data_source.lifecycle"][0];

         //   platform_source_lifecycle.resolve();
         //   platform_source_lifecycle.deploy();
         //   platform_source_lifecycle.instantiate();


            // 2.3. Creating a set of system context parameters that will be necessary for "source".
            system.addParameter("key_type", "TKey", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.VertexBasic" } });
            system.addParameter("value_type", "TValue", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.DataObject" } });
            system.addParameter("graph_input_format", "GIF", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.InputFormat" } });
            system.addParameter("partition_function_action_port_type", "A", new Dictionary<string, object> { { "name", "br.ufc.mdcc.hpc.storm.binding.task.TaskPortType" } });

            // 2.4. Specifying an argument to the "value_type" context parameter. 
            /* Thus, all references to "TValue" will be associated to the argument. For context parameters that an argument is not defined, 
             * the bound is applied. Notice that the argument must be a subtype of the parameter bound.
             */
            system.setArgument("value_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.example.pgr.DataPGRANK" } });
            system.setArgument("partition_function_action_port_type", new Dictionary<string, object> { { "name", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType" } });

            // 2.5. Specifying the contract of "source".
            /* Notice that all arguments are system context variables, previously defined. */
            IDictionary<string, object> contract_source = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.gust.custom.io.DataSourceGraph"},
                {"platform_maintainer", "MData"},
                {"input_key_type", "TKey"},
                {"input_value_type", "TValue"},
                {"graph_input_format", "GIF"}

            };

            // 2.6. Creating an data source object that represent the "source" component.
            IDataSource source = system.newDataSource("source", contract_source, platform_data_source);

            // 3. CREATING THE COMPUTATION COMPONENTS, "reduce_1" and "reducer_2".

            // 3.1. Specifying the contract of the platforms where "reducer_1" and "reducer_2" will be placed, "platform_reduce_1" and "platform_reduce_2".
            IDictionary<string, object> contract_platform_reduce_0 = new Dictionary<string, object>()
               {
                   {"name", "org.hpcshelf.platform.Platform"},
                   {"maintainer", "MPeer0"},
                   {"node-count", 2}

            };

            IDictionary<string, object> contract_platform_reduce_1 = new Dictionary<string, object>()
               {
                   {"name", "org.hpcshelf.platform.Platform"},
                   {"maintainer", "MPeer1"},
                   {"node-count", 2}

            };

            /*	IDictionary<string, object> contract_platform_reduce_0 = new Dictionary<string, object>()
                {
                    {"name", "br.ufc.mdcc.backend.ec2.platform.general.EC2_T2_Micro"},
                    {"node_count", 8}
                };

                IDictionary<string, object> contract_platform_reduce_1 = new Dictionary<string, object>()
                {
                    {"name", "br.ufc.mdcc.backend.ec2.platform.general.EC2_T2_Micro"}, 
                    {"node_count", 8}
                };
                */

            IDictionary<string, object>[] contract_platform_reduce = new IDictionary<string, object>[2] { contract_platform_reduce_0, contract_platform_reduce_1 };


            // 3.2. Adding a new system context parameter for representing the maintainer of the virtual platforms.
            system.addParameter("compute_host_0", "MPeer0", new Dictionary<string, object> { { "name", /*"org.hpcshelf.platform.maintainer.EC2"*/ "org.hpcshelf.platform.maintainer.ComputeHost" } });
            system.addParameter("compute_host_1", "MPeer1", new Dictionary<string, object> { { "name", /*"org.hpcshelf.platform.maintainer.GCP"*/ "org.hpcshelf.platform.maintainer.ComputeHost" } });

            // 3.3. Crating the virtual platform objects that represent "platform_reduce_1" and "platform_reduce_2".
            IVirtualPlatform[] platform_reduce = new IVirtualPlatform[N];
            for (int i = 0; i < N; i++)
                platform_reduce[i] = system.newVirtualPlatform("platform_reduce_" + i, contract_platform_reduce[i % 2]);

            //3.4. Adding new system context parameters that will be referred in the contracts of "reducer_1" and "reducer_2"
            system.addParameter("graph_type", "G", new Dictionary<string, object> { { "name", "br.ufc.mdcc.common.Data" } });
            system.addParameter("action_port_type_reduce", "ATerminationPortType", new Dictionary<string, object> { { "name", "mapreduce.gust.iterative.port.task.TerminationFlagTaskPortType" } });
            system.addParameter("gusty_function", "RF", new Dictionary<string, object>
                                                            {
                                                                        {"name", "org.hpcshelf.gust.custom.GustyFunction" },
                                                                        {"input_key_type", "TKey"},
                                                                        {"input_value_type", "TValue"},
                                                                        {"output_key_type", "TKey"},
                                                                        {"output_value_type", "TValue"},
                                                                        {"graph_type", "G"},
                                                                        {"graph_input_format", "GIF"},
                                                                        {"action_port_type", "ATerminationPortType"}
                                                            });

            // 3.5. Set the arguments to be applied to two of the recently created context parameters. 
            /* For the other, "action_port_type_reduce", the bound is applied */

            system.setArgument("graph_type", new Dictionary<string, object>
                                                {          {"name", "org.hpcshelf.gust.graph.DirectedGraph"},
                                                           {"vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } }},
                                                           {"edge_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.EdgeWeighted" }, {"vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } } } }},
                                                           {"container", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.container.DataContainerKV" }, {"vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } } }, {"edge_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.EdgeWeighted" }, { "vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } } } } } }}
                                                });
            system.setArgument("gusty_function", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.example.pgr.PGRANK" } });

            // 3.6. Specifying the contract of "reducer_1" and "reducer_2".
            IDictionary<string, object> contract_reducer_0 = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.gust.computation.Gusty"},
                {"platform-maintainer", "MPeer0" /* new Dictionary<string, object> { { "name", "org.hpcshelf.platform.maintainer.EC2" } }*/},
                {"input_key_type", "TKey"},
                {"input_value_type", "TValue"},
                {"output_key_type", "TKey"},
                {"output_value_type", "TValue"},
                {"reduce_function", "RF"},
                {"graph_type", "G"},
                {"graph_input_format", "GIF"},
                {"reduce_function_action_port_type", "ATerminationPortType"}
            };

            IDictionary<string, object> contract_reducer_1 = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.gust.computation.Gusty"},
                {"platform-maintainer", "MPeer1" /* new Dictionary<string, object> { { "name", "org.hpcshelf.platform.maintainer.EC2" } }*/},
                {"input_key_type", "TKey"},
                {"input_value_type", "TValue"},
                {"output_key_type", "TKey"},
                {"output_value_type", "TValue"},
                {"reduce_function", "RF"},
                {"graph_type", "G"},
                {"graph_input_format", "GIF"},
                {"reduce_function_action_port_type", "ATerminationPortType"}
            };

            IDictionary<string, object>[] contract_reducer = new IDictionary<string, object>[2] { contract_reducer_0, contract_reducer_1 };

            // 3.7. Creating the objects that represent "reducer_1" and "reducer_2".
            IComputation[] reducer = new IComputation[N];
            for (int i = 0; i < N; i++)
                reducer[i] = system.newComputation("reducer_" + i, contract_reducer[i % 2], platform_reduce[i]);

            // 4. CREATE THE "flatten_output" COMPONENT, that will be placed in "platform_SAFe".

            // 4.1. Specifying the contract of the "flatten_output" component.
            IDictionary<string, object> contract_flatten_output = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.mapreduce.computation.Mapper"},
                {"platform-maintainer", new Dictionary<string,object> {{"name","org.hpcshelf.platform.maintainer.SAFeHost"}}},
                {"input_key_type", "TKey"},
                {"input_value_type", new Dictionary<string,object> {{"name","br.ufc.mdcc.common.Iterator"}, {"item_type","TValue"}}},
                {"output_key_type", "TKey"},
                {"output_value_type", "TValue"},
                {"map_function", new Dictionary<string, object> {{"name", "org.hpcshelf.mapreduce.custom.MapFunctionFlatten"},
                                                                 {"key_type", "TKey"},
                                                                 {"value_type","TValue"},
                                                                 {"action_port_type",new Dictionary<string,object> {{"name","br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType" }}}}},
                {"map_function_action_port_type", new Dictionary<string,object> {{"name","br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType" }}}
            };

            // 4.2. Creating the computation object that represent "flatten_output".
            IComputation flatten_output = system.newComputation("flatten_output", contract_flatten_output, system.PlatformSAFe);

            // 5. CREATING THE "shuffler" connector. 

            // 5.1. Adding context parameteres that will be referred in the contract of "shuffler".
            //system.addParameter("action_port_type_partition", "AA", new Dictionary<string, object> { { "name", "mapreduce.gust.iterative.port.task.TerminationFlagTaskPortType" } });
            system.addParameter("partition_function", "PF", new Dictionary<string, object>
                                                                {
                                                                   { "name", "org.hpcshelf.mapreduce.custom.PartitionFunctionIterative" },
                                                                   { "input_key", "TKey" },
                                                                   { "action_port_type", "A"}
                                                                });

            // 5.2. Specifying the contract of "shuffler".
            IDictionary<string, object> contract_shuffler = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.mapreduce.connector.Shuffler"},
                {"intermediate_key_type", "TKey"},
                {"intermediate_value_type", "TValue"},
                {"partition_function", "PF"},
                {"partition_function_action_port_type", "A"}
            };

            // 5.2. Specifying where the facets of "shuffler" will be placed.
            Tuple<int, IVirtualPlatform>[] platform_shuffler = new Tuple<int, IVirtualPlatform>[2 * (N + 1)];

            for (int i = 0; i < N; i++)
                platform_shuffler[i] = new Tuple<int, IVirtualPlatform>(0, platform_reduce[i]);
            platform_shuffler[N] = new Tuple<int, IVirtualPlatform>(0, system.PlatformSAFe);
            for (int i = N + 1; i < 2 * N + 1; i++)
                platform_shuffler[i] = new Tuple<int, IVirtualPlatform>(1, platform_reduce[i - N - 1]);
            platform_shuffler[2 * N + 1] = new Tuple<int, IVirtualPlatform>(1, platform_data_source);

            // 5.2. Creating the connector object that represents "shuffler".
            IConnector shuffler = system.newConnector("shuffler", contract_shuffler, platform_shuffler);

            // 6. CREATING THE ACTION PORTS OF THE "workflow" system component.
            /* They are necessary for connecting the bindings between the "workflow" component and solution components */


            // 7. CREATING THE ACTION BINDINGS
            /* The arguments define the type of the action binding, which determines the supported action names, and a set of action ports 
             * of the solution components that will synchronize among them. Notice that each port is defined by an array, because, once each port 
             * belongs to a single facet they may be replicated when a facet of a connector is replicated accross a set of virtual platforms, 
             * such as "shuffler". For instance, see "platform_shuffler", above, where shuffler facets 0 (feede) and 1 (collect) have multiplicity 3.
             */

            IActionBinding task_flatten_output = system.newActionBinding("task_flatten_output", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance", new IActionPort[] { system.Workflow.NewActionPort, flatten_output.ActionPort["task_map"][0] });

            IActionBinding[] task_reduce = new IActionBinding[N];
            for (int i = 0; i < N; i++)
                task_reduce[i] = system.newActionBinding("task_reduce_" + i, "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance", new IActionPort[] { system.Workflow.NewActionPort, reducer[i].ActionPort["task_reduce"][0] });

            IActionBinding task_shuffle_collector_active_status_N = system.newActionBinding("task_shuffle_collector_active_status_" + N, "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_collector_active_status"][N] });
            IActionBinding[] task_shuffle_collector_active_status = new IActionBinding[N];
            for (int i = 0; i < N; i++)
                task_shuffle_collector_active_status[i] = system.newActionBinding("task_shuffle_collector_active_status_" + i, "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_collector_active_status"][i] });

            IActionBinding task_shuffle_collector_read_chunk_N = system.newActionBinding("task_shuffle_collector_read_chunk_" + N, "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_collector_read_chunk"][N] });
            IActionBinding[] task_shuffle_collector_read_chunk = new IActionBinding[N];
            for (int i = 0; i < N; i++)
                task_shuffle_collector_read_chunk[i] = system.newActionBinding("task_shuffle_collector_read_chunk_" + i, "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_collector_read_chunk"][i] });

            IActionBinding task_shuffle_feeder_read_chunk_N = system.newActionBinding("task_shuffle_feeder_read_chunk_" + N, "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_feeder_read_chunk"][N] });
            IActionBinding[] task_shuffle_feeder_read_chunk = new IActionBinding[N];
            for (int i = 0; i < N; i++)
                task_shuffle_feeder_read_chunk[i] = system.newActionBinding("task_shuffle_feeder_read_chunk_" + i, "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_feeder_read_chunk"][i] });

            IActionBinding task_shuffle_feeder_chunk_ready_N = system.newActionBinding("task_shuffle_feeder_chunk_ready_" + N, "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_feeder_chunk_ready"][N] });
            IActionBinding[] task_shuffle_feeder_chunk_ready = new IActionBinding[N];
            for (int i = 0; i < N; i++)
                task_shuffle_feeder_chunk_ready[i] = system.newActionBinding("task_shuffle_feeder_chunk_ready_" + i, "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady", new IActionPort[] { system.Workflow.NewActionPort, shuffler.ActionPort["task_shuffle_feeder_chunk_ready"][i] });

            IActionBinding[] reduce_function_action_port = new IActionBinding[N];
            for (int i = 0; i < N; i++)
                reduce_function_action_port[i] = system.newActionBinding("reduce_function_action_port_" + i, "ATerminationPortType", new IActionPort[] { system.Workflow.NewActionPort, reducer[i].ActionPort["reduce_function_action_port"][0] });

            // 7. CREATING THE SERVICE BINDINGS

            // 7.1. Specifying the binding contracts.
            IDictionary<string, object> contract_service_input = new Dictionary<string, object>()
            {
                {"name", "br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect"},
                {"client_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}},
                {"server_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.gust.port.environment.PortTypeDataSourceGraphInterface"}}}
            };

            IDictionary<string, object> contract_service_pairs = new Dictionary<string, object>()
            {
                {"name", "br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect"},
                {"client_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}},
                {"server_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}}
            };

            IDictionary<string, object> contract_service_output = new Dictionary<string, object>()
            {
                {"name", "br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect"},
                {"client_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.gust.port.environment.PortTypeDataSinkGraphInterface"}}},
                {"server_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}}
            };

            // 7.2. Creating the objects that represent the bindings.
            IServiceBinding local_input_pairs = system.newServiceBinding("local_input_pairs", contract_service_input, shuffler.UserPort["collect_pairs"][N], source.ProviderPort["input_data"][0]);
            IServiceBinding local_output_pairs = system.newServiceBinding("local_output_pairs", contract_service_pairs, flatten_output.UserPort["collect_pairs"][0], shuffler.ProviderPort["feed_pairs"][N]); ;

            IServiceBinding[] global_input_pairs = new IServiceBinding[N];
            for (int i = 0; i < N; i++)
                global_input_pairs[i] = system.newServiceBinding("global_input_pairs_" + i, contract_service_pairs, reducer[i].UserPort["collect_pairs"][0], shuffler.ProviderPort["feed_pairs"][i]);

            IServiceBinding[] global_output_pairs = new IServiceBinding[N];
            for (int i = 0; i < N; i++)
                global_output_pairs[i] = system.newServiceBinding("global_output_pairs_" + i, contract_service_pairs, shuffler.UserPort["collect_pairs"][i], reducer[i].ProviderPort["feed_pairs"][0]);

            // 7.3. Adding a user port the the "application" system component, which will read the output.
            //system.Application.addDirectUserPort("output_data", contract_service_output);

            // 7.4. Creating a binding to connect the "output_data" port of "application" to the "feed_pairs" port of "flatten_output".
            IServiceBinding output_data = system.newServiceBinding("output_data", contract_service_output, (IUserServicePort)system.Application.NewServicePort, flatten_output.ProviderPort["feed_pairs"][0]);

            IActionPortWorkflow workflow_port_flatten_output = (IActionPortWorkflow)system.Workflow.ActionPort["task_flatten_output"][0];

            IActionPortWorkflow[] workflow_port_reduce = new IActionPortWorkflow[N];
            for (int i = 0; i < N; i++)
                workflow_port_reduce[i] = (IActionPortWorkflow)system.Workflow.ActionPort["task_reduce_" + i][0];

            IActionPortWorkflow workflow_port_active_status_N = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_collector_active_status_" + N][0];
            IActionPortWorkflow[] workflow_port_active_status = new IActionPortWorkflow[N];
            for (int i = 0; i < N; i++)
                workflow_port_active_status[i] = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_collector_active_status_" + i][0];

            IActionPortWorkflow workflow_port_collector_read_chunk_N = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_collector_read_chunk_" + N][0];
            IActionPortWorkflow[] workflow_port_collector_read_chunk = new IActionPortWorkflow[N];
            for (int i = 0; i < N; i++)
                workflow_port_collector_read_chunk[i] = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_collector_read_chunk_" + i][0];

            IActionPortWorkflow workflow_port_feeder_read_chunk_N = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_feeder_read_chunk_" + N][0];
            IActionPortWorkflow[] workflow_port_feeder_read_chunk = new IActionPortWorkflow[N];
            for (int i = 0; i < N; i++)
                workflow_port_feeder_read_chunk[i] = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_feeder_read_chunk_" + i][0];

            IActionPortWorkflow workflow_port_feeder_chunk_ready_N = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_feeder_chunk_ready_" + N][0];
            IActionPortWorkflow[] workflow_port_feeder_chunk_ready = new IActionPortWorkflow[N];
            for (int i = 0; i < N; i++)
                workflow_port_feeder_chunk_ready[i] = (IActionPortWorkflow)system.Workflow.ActionPort["task_shuffle_feeder_chunk_ready_" + i][0];

            IActionPortWorkflow[] workflow_port_reduce_function = new IActionPortWorkflow[N];
            for (int i = 0; i < N; i++)
                workflow_port_reduce_function[i] = (IActionPortWorkflow)system.Workflow.ActionPort["reduce_function_action_port_" + i][0];

            IActionPortLifeCycle source_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["source.lifecycle"][0];



            // 8. TAKING THE OBJECTS REPRESENTING LIFE CYCLE CONTROL PORTS FOR EACH SOLUTION COMPONENT.

            IActionPortLifeCycle[] reducer_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                reducer_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["reducer_" + i + ".lifecycle"][0];

            IActionPortLifeCycle flatten_output_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["flatten_output.lifecycle"][0];

            IActionPortLifeCycle shuffler_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["shuffler.lifecycle"][0];

            IActionPortLifeCycle platform_source_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["platform_data_source.lifecycle"][0];

            IActionPortLifeCycle[] platform_reducer_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                platform_reducer_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["platform_reduce_" + i + ".lifecycle"][0];

            IActionPortLifeCycle local_input_pairs_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["local_input_pairs.lifecycle"][0];

            IActionPortLifeCycle local_output_pairs_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["local_output_pairs.lifecycle"][0];

            IActionPortLifeCycle[] global_input_pairs_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                global_input_pairs_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["global_input_pairs_" + i + ".lifecycle"][0];

            IActionPortLifeCycle[] global_output_pairs_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                global_output_pairs_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["global_output_pairs_" + i + ".lifecycle"][0];

            IActionPortLifeCycle output_data_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["output_data.lifecycle"][0];

            IActionPortLifeCycle[] task_reduce_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                task_reduce_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["task_reduce_" + i + ".lifecycle"][0];

            IActionPortLifeCycle task_flatten_output_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["task_flatten_output.lifecycle"][0];

            IActionPortLifeCycle task_shuffle_collector_active_status_N_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_collector_active_status_" + N + ".lifecycle"][0];
            IActionPortLifeCycle[] task_shuffle_collector_active_status_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                task_shuffle_collector_active_status_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_collector_active_status_" + i + ".lifecycle"][0];

            IActionPortLifeCycle task_shuffle_collector_read_chunk_N_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_collector_read_chunk_" + N + ".lifecycle"][0];
            IActionPortLifeCycle[] task_shuffle_collector_read_chunk_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                task_shuffle_collector_read_chunk_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_collector_read_chunk_" + i + ".lifecycle"][0];

            IActionPortLifeCycle task_shuffle_feeder_read_chunk_N_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_feeder_read_chunk_" + N + ".lifecycle"][0];
            IActionPortLifeCycle[] task_shuffle_feeder_read_chunk_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                task_shuffle_feeder_read_chunk_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_feeder_read_chunk_" + i + ".lifecycle"][0];

            IActionPortLifeCycle task_shuffle_feeder_chunk_ready_N_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_feeder_chunk_ready_" + N + ".lifecycle"][0];
            IActionPortLifeCycle[] task_shuffle_feeder_chunk_ready_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                task_shuffle_feeder_chunk_ready_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["task_shuffle_feeder_chunk_ready_" + i + ".lifecycle"][0];

            IActionPortLifeCycle[] reduce_function_action_port_lifecycle = new IActionPortLifeCycle[N];
            for (int i = 0; i < N; i++)
                reduce_function_action_port_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort["reduce_function_action_port_" + i + ".lifecycle"][0];


            // 8. INITIATING THE ORCHESTRATION

            // 8.1. Firstly, creating all solution components.
                      
            platform_source_lifecycle.resolve();
            platform_source_lifecycle.deploy();
            platform_source_lifecycle.instantiate();

            for (int i = 0; i < N; i++)
            {
                platform_reducer_lifecycle[i].resolve();
                platform_reducer_lifecycle[i].deploy();
                platform_reducer_lifecycle[i].instantiate();
            }

            flatten_output_lifecycle.resolve();
            flatten_output_lifecycle.deploy();
            flatten_output_lifecycle.instantiate();

            for (int i = 0; i < N; i++)
            {
                reducer_lifecycle[i].resolve();
                reducer_lifecycle[i].deploy();
                reducer_lifecycle[i].instantiate();
            }

            shuffler_lifecycle.resolve();
            shuffler_lifecycle.deploy();
            shuffler_lifecycle.instantiate();

            source_lifecycle.resolve();
            source_lifecycle.deploy();
            source_lifecycle.instantiate();

            local_input_pairs_lifecycle.resolve();
            local_input_pairs_lifecycle.deploy();
            local_input_pairs_lifecycle.instantiate();

            local_output_pairs_lifecycle.resolve();
            local_output_pairs_lifecycle.deploy();
            local_output_pairs_lifecycle.instantiate();

            for (int i = 0; i < N; i++)
            {
                global_input_pairs_lifecycle[i].resolve();
                global_input_pairs_lifecycle[i].deploy();
                global_input_pairs_lifecycle[i].instantiate();
            }


            for (int i = 0; i < N; i++)
            {
                global_output_pairs_lifecycle[i].resolve();
                global_output_pairs_lifecycle[i].deploy();
                global_output_pairs_lifecycle[i].instantiate();
            }

            for (int i = 0; i < N; i++)
            {
                task_reduce_lifecycle[i].resolve();
                task_reduce_lifecycle[i].deploy();
                task_reduce_lifecycle[i].instantiate();
            }


            task_shuffle_collector_active_status_N_lifecycle.resolve();
            task_shuffle_collector_active_status_N_lifecycle.deploy();
            task_shuffle_collector_active_status_N_lifecycle.instantiate();

            for (int i = 0; i < N; i++)
            {
                task_shuffle_collector_active_status_lifecycle[i].resolve();
                task_shuffle_collector_active_status_lifecycle[i].deploy();
                task_shuffle_collector_active_status_lifecycle[i].instantiate();
            }

            task_shuffle_collector_read_chunk_N_lifecycle.resolve();
            task_shuffle_collector_read_chunk_N_lifecycle.deploy();
            task_shuffle_collector_read_chunk_N_lifecycle.instantiate();

            for (int i = 0; i < N; i++)
            {
                task_shuffle_collector_read_chunk_lifecycle[i].resolve();
                task_shuffle_collector_read_chunk_lifecycle[i].deploy();
                task_shuffle_collector_read_chunk_lifecycle[i].instantiate();
            }

            task_shuffle_feeder_read_chunk_N_lifecycle.resolve();
            task_shuffle_feeder_read_chunk_N_lifecycle.deploy();
            task_shuffle_feeder_read_chunk_N_lifecycle.instantiate();

            for (int i = 0; i < N; i++)
            {
                task_shuffle_feeder_read_chunk_lifecycle[i].resolve();
                task_shuffle_feeder_read_chunk_lifecycle[i].deploy();
                task_shuffle_feeder_read_chunk_lifecycle[i].instantiate();
            }

            for (int i = 0; i < N; i++)
            {
                task_shuffle_feeder_chunk_ready_lifecycle[i].resolve();
                task_shuffle_feeder_chunk_ready_lifecycle[i].deploy();
                task_shuffle_feeder_chunk_ready_lifecycle[i].instantiate();
            }

            for (int i = 0; i < N; i++)
            {
                reduce_function_action_port_lifecycle[i].resolve();
                reduce_function_action_port_lifecycle[i].deploy();
                reduce_function_action_port_lifecycle[i].instantiate();
            }

            source_lifecycle.run();
            for (int i = 0; i < N; i++)
                reducer_lifecycle[i].run();
            shuffler_lifecycle.run();

            Semaphore wait_output_data = new Semaphore(0, int.MaxValue);
            // 8.2. This is a thread for reiveing the output.

            Thread thread_receive_output = new Thread(() =>
            {
                Console.WriteLine("thread_receive_output 1");

                IClientBase<IPortTypeDataSinkGraphInterface> output_data_port = (IClientBase<IPortTypeDataSinkGraphInterface>)system.Application.Services.getPort("output_data");

                Console.WriteLine("thread_receive_output 2");

                // IOutputFormat<IVertexBasic, IDataPGRANK> graph_output_format = (IOutputFormat<IVertexBasic, IDataPGRANK>)system.Application.Services.getPort("output_format");

                Console.WriteLine("thread_receive_output 3");

                long t0 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                //IOutputFormatInstance<IVertexBasic, IDataPGRANK> gof = (IOutputFormatInstance<IVertexBasic, IDataPGRANK>)graph_output_format.Instance;
                Console.WriteLine("IDataSinkGraph: START 1 output_file_path = {0}", OUTPUT_FILE_PATH);
                Console.WriteLine("output_data is null ? {0}", output_data_port == null);
                wait_output_data.WaitOne();
                IPortTypeDataSinkGraphInterface pair_reader = output_data_port.Client;
                Console.WriteLine("pair_reader is null ? {0}", pair_reader == null);
                ConcurrentQueue<Tuple<object, int>> pairs = pair_reader.readPairs();
                Semaphore not_empty = pair_reader.NotEmpty;
                Console.WriteLine("IDataSinkGraph: BEFORE LOOP");
                Tuple<object, int> pair;

                int sum = 0;

                do
                {
                    Console.WriteLine("IDataSinkGraph: LOOP -- BEFORE not_empty LOCK");
                    not_empty.WaitOne();
                    Console.WriteLine("IDataSinkGraph: LOOP -- AFTER not_empty LOCK");
                    pairs.TryDequeue(out pair);
                    if (pair.Item2 > 0)
                        sendToFile(OUTPUT_FILE_PATH, pair.Item1);
                    sum += pair.Item2;
                } while (pair.Item2 > 0);

                Console.WriteLine("IDataSink: FINISH sum={0}", sum);
                long t1 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                Console.WriteLine("Time processing: " + (t1 - t0) + " ms");
                File.AppendAllText(OUTPUT_FILE_PATH + ".time", (t1 - t0) + "" + System.Environment.NewLine);

            });

            thread_receive_output.Start();

            // 8.3. TASK ORCHESTRATION ...

            Thread[] t_active_status_inactive = new Thread[N];
            for (int i = 0; i < N; i++)
            {
                t_active_status_inactive[i] = new Thread((object o) =>
                {
                    int ix = (int)o;
                    workflow_port_active_status[ix].invoke("CHANGE_STATUS_BEGIN");
                    workflow_port_active_status[ix].invoke("INACTIVE");
                    workflow_port_active_status[ix].invoke("CHANGE_STATUS_END");
                });
                t_active_status_inactive[i].Start(i);
            }

            for (int i = 0; i < N; i++)
                t_active_status_inactive[i].Join();

            while (workflow_port_collector_read_chunk_N.invoke(new string[] { "READ_CHUNK", "FINISH_CHUNK" }).Equals("READ_CHUNK"))
            {
                Thread[] t_reduce_first_iteration = new Thread[N];

                for (int i = 0; i < N; i++)
                {
                    t_reduce_first_iteration[i] = new Thread((object o) =>
                    {
                        int ix = (int)o;
                        workflow_port_feeder_read_chunk[ix].invoke("READ_CHUNK");
                        workflow_port_feeder_chunk_ready[ix].invoke("CHUNK_READY");
                        workflow_port_reduce[ix].invoke("READ_CHUNK");
                        workflow_port_reduce[ix].invoke("PERFORM");
                    });

                    t_reduce_first_iteration[i].Start(i);
                }

                for (int i = 0; i < N; i++)
                    t_reduce_first_iteration[i].Join();
            }

            IActionFuture[] f1 = new IActionFuture[N]; //, f2, f3, f4;
            IActionFuture[] f2 = new IActionFuture[N]; //, f2, f3, f4;

            for (int i = 0; i < N; i++) workflow_port_feeder_read_chunk[i].invoke("FINISH_CHUNK", out f1[i]);
            for (int i = 0; i < N; i++) workflow_port_reduce[i].invoke("FINISH_CHUNK", out f2[i]);

            IActionFutureSet fs = f1[0].createSet();
            for (int i = 1; i < N; i++) fs.addAction(f1[i]);
            for (int i = 0; i < N; i++) fs.addAction(f2[i]);
            fs.waitAll();

            for (int i = 0; i < N; i++) workflow_port_reduce[i].invoke("CHUNK_READY", out f1[i]);
            fs = f1[0].createSet(); for (int i = 1; i < N; i++) fs.addAction(f1[i]);
            fs.waitAll();

            Thread t_active_status_N_inactive = new Thread(() =>
            {
                workflow_port_active_status_N.invoke("CHANGE_STATUS_BEGIN");
                workflow_port_active_status_N.invoke("INACTIVE");
                workflow_port_active_status_N.invoke("CHANGE_STATUS_END");
            });
            t_active_status_N_inactive.Start();

            Thread[] t_active_status_active = new Thread[N];
            for (int i = 0; i < N; i++)
            {
                t_active_status_active[i] = new Thread((object o) =>
                {
                    int ix = (int)o;
                    workflow_port_active_status[ix].invoke("CHANGE_STATUS_BEGIN");
                    workflow_port_active_status[ix].invoke("ACTIVE");
                    workflow_port_active_status[ix].invoke("CHANGE_STATUS_END");
                });
                t_active_status_active[i].Start(i);
            }

            t_active_status_N_inactive.Join();
            for (int i = 0; i < N; i++)
                t_active_status_active[i].Join();

            Semaphore wait_a = new Semaphore(0, int.MaxValue);
            Semaphore wait_b = new Semaphore(0, int.MaxValue);

            Thread t_main_loop = new Thread(() =>
                {
                    bool main_loop_termination = false;
                    while (!main_loop_termination)
                    {
                        string main_loop_condition = workflow_port_reduce_function[0].invoke(new string[] { "CONTINUE", "TERMINATE" }) as string;
                        switch (main_loop_condition)
                        {
                            case "CONTINUE":
                                for (int i = 1; i < N; i++)
                                    workflow_port_reduce_function[i].invoke("CONTINUE");
                                bool next_iteration_termination = false;
                                while (!next_iteration_termination)
                                {
                                    string next_iteration_condition = workflow_port_collector_read_chunk[0].invoke(new string[] { "READ_CHUNK", "FINISH_CHUNK" }) as string;
                                    switch (next_iteration_condition)
                                    {
                                        case "READ_CHUNK":
                                            for (int i = 1; i < N; i++)
                                                workflow_port_collector_read_chunk[i].invoke("READ_CHUNK", out f1[i - 1]);
                                            fs = f1[0].createSet(); for (int i = 1; i < N - 1; i++) fs.addAction(f1[i]);
                                            fs.waitAll();
                                            Thread[] t_reduce_next_iteration = new Thread[N];
                                            for (int i = 0; i < N; i++)
                                            {
                                                t_reduce_next_iteration[i] = new Thread((object o) =>
                                                {
                                                    int ix = (int)o;
                                                    workflow_port_feeder_read_chunk[ix].invoke("READ_CHUNK");
                                                    workflow_port_feeder_chunk_ready[ix].invoke("CHUNK_READY");
                                                    workflow_port_reduce[ix].invoke("READ_CHUNK");
                                                    workflow_port_reduce[ix].invoke("PERFORM");
                                                });
                                                t_reduce_next_iteration[i].Start(i);
                                            }
                                            for (int i = 0; i < N; i++)
                                                t_reduce_next_iteration[i].Join();
                                            break;
                                        case "FINISH_CHUNK":
                                            for (int i = 1; i < N; i++)
                                                workflow_port_collector_read_chunk[i].invoke("FINISH_CHUNK", out f1[i - 1]);
                                            fs = f1[0].createSet(); for (int i = 1; i < N - 1; i++) fs.addAction(f1[i]);
                                            fs.waitAll();
                                            next_iteration_termination = true;
                                            break;
                                    }
                                }
                                for (int i = 0; i < N; i++) workflow_port_feeder_read_chunk[i].invoke("FINISH_CHUNK", out f1[i]);
                                for (int i = 0; i < N; i++) workflow_port_reduce[i].invoke("FINISH_CHUNK", out f2[i]);

                                fs = f1[0].createSet();
                                for (int i = 1; i < N; i++) fs.addAction(f1[i]);
                                for (int i = 0; i < N; i++) fs.addAction(f2[i]);
                                fs.waitAll();

                                for (int i = 0; i < N; i++) workflow_port_reduce[i].invoke("CHUNK_READY", out f1[i]);
                                fs = f1[0].createSet(); for (int i = 1; i < N; i++) fs.addAction(f1[i]);
                                fs.waitAll();

                                break;

                            case "TERMINATE":
                                Thread t_complete_shuffle_feeder_N = new Thread(() =>
                                {
                                    wait_a.WaitOne();
                                    workflow_port_feeder_read_chunk_N.invoke("READ_CHUNK");
                                    workflow_port_feeder_chunk_ready_N.invoke("CHUNK_READY");
                                    workflow_port_feeder_read_chunk_N.invoke("FINISH_CHUNK");
                                });

                                Thread t_complete_flatten_output = new Thread(() =>
                                {
                                    wait_b.WaitOne();
                                    workflow_port_flatten_output.invoke("READ_CHUNK");
                                    workflow_port_flatten_output.invoke("PERFORM");
                                    workflow_port_flatten_output.invoke("CHUNK_READY");
                                    workflow_port_flatten_output.invoke("FINISH_CHUNK");
                                });

                                for (int i = 1; i < N; i++) workflow_port_reduce_function[i].invoke("TERMINATE", out f1[i - 1]);
                                for (int i = 0; i < N; i++) workflow_port_collector_read_chunk[i].invoke("READ_CHUNK", out f2[i]);

                                Console.WriteLine("After TERMINATE #1");

                                fs = f1[0].createSet();
                                for (int i = 1; i < N - 1; i++) fs.addAction(f1[i]);
                                for (int i = 0; i < N; i++) fs.addAction(f2[i]);

                                Console.WriteLine("After TERMINATE #2");

                                t_complete_shuffle_feeder_N.Start();
                                t_complete_flatten_output.Start();

                                Console.WriteLine("After TERMINATE #3");

                                t_complete_shuffle_feeder_N.Join();
                                t_complete_flatten_output.Join();

                                Console.WriteLine("After TERMINATE #4");

                                fs.waitAll();

                                Console.WriteLine("After TERMINATE #5");


                                main_loop_termination = true;
                                break;
                        }
                    }


                });

            Thread t_create_output_components = new Thread(() =>
                {
                    task_shuffle_feeder_chunk_ready_N_lifecycle.resolve();
                    task_shuffle_feeder_chunk_ready_N_lifecycle.deploy();
                    task_shuffle_feeder_chunk_ready_N_lifecycle.instantiate();

                    wait_a.Release();

                    output_data_lifecycle.resolve();
                    output_data_lifecycle.deploy();
                    output_data_lifecycle.instantiate();

                    wait_output_data.Release();

                    task_flatten_output_lifecycle.resolve();
                    task_flatten_output_lifecycle.deploy();
                    task_flatten_output_lifecycle.instantiate();

                    wait_b.Release();

                    flatten_output_lifecycle.run();
                });

            t_main_loop.Start();
            t_create_output_components.Start();
            t_main_loop.Join();
            t_create_output_components.Join();

            thread_receive_output.Join();

            Console.WriteLine("...");
        }

        public static void sendToFile(string filename, object o)
        {
            Console.WriteLine("SEND TO FILE {0} --- TYPE: {1}", filename, o.GetType());
            IKVPairInstance<IVertexBasic, IDataPGRANK> kv = (IKVPairInstance<IVertexBasic, IDataPGRANK>)o;

            string s = kv.Key + " " + kv.Value;
            File.AppendAllText(filename, s + System.Environment.NewLine);
        }
    }
}
