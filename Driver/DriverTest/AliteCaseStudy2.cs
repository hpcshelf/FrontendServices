﻿using System;
using System.Collections.Generic;
using HPCShelfDriver;

namespace DriverTest
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            IDictionary<string, Tuple<string, object>> contract_signature = new Dictionary<string, Tuple<string, object>>()
            {
                //{"input_data_host", new Tuple<string, object>("MData", new Dictionary<string,object>() {{"name","org.hpcshelf.platform.maintainer.DataHost"}})}
            };

            ISystem system = SystemBuilder.build("alite_case_study", contract_signature);

            IDictionary<string, object> contract_platform_A = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.platform.Platform"},
                {"node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                {"node-count", 4}
            };

            IVirtualPlatform platform_A = system.newVirtualPlatform("ec2_platform_A", contract_platform_A);

            IDictionary<string, object> contract_computation_A = new Dictionary<string, object>()
            {
                {"name", "teste.TestAlite"},
                {"min_M", 16000},
				{"min_N", 16000},
				{"min_P", 16000},
				{"max_M", 18000},
				{"max_N", 18000},
				{"max_P", 18000},
				{"platform-node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                {"platform-node-count", 4}
            };

            IComputation computation_A = system.newComputation("ec2_computation_A", contract_computation_A, platform_A);

            IActionPortLifeCycle computation_lifecycle_A = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_computation_A.lifecycle"][0];
            IActionPortLifeCycle platform_lifecycle_A = (IActionPortLifeCycle)system.Workflow.ActionPort["ec2_platform_A.lifecycle"][0];

            platform_lifecycle_A.resolve_system();

          //  platform_lifecycle_B.resolve();

          //  platform_lifecycle_C.resolve();

           // try { computation_lifecycle_A.resolve(); } catch (System.Web.Services.Protocols.SoapException e) { }






        }
    }
}
