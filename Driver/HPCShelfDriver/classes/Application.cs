﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.kinds;
using gov.cca;

namespace HPCShelfDriver
{
    internal sealed class Application: ServiceSolutionComponent, IApplication
    {
		private IEnvironmentKind unit = null;

		private IServicePort new_service_port = null;
		public IServicePort NewServicePort => new_service_port;
		
        public Application(ISystem system_component, IPlatformSAFe platform_SAFe, CoreServices core_services) : base(system_component, Constants.APPLICATION_COMPONENT_ID, new Dictionary<string, object>(), core_services, new Tuple<int, IVirtualPlatform>[1] { new Tuple<int, IVirtualPlatform>(0, platform_SAFe) })
        {
            new_service_port = new UserProviderServicePort(this, Constants.APPLICATION_COMPONENT_ID, 0, 0);
			this.place = platform_SAFe;
            this.place.addComponent(this);
            createComponent();
			//Contract["name"] = CoreServices.getName_AbstractApplication();
		}

        private IPlatformSAFe place;

        public Services Services { get => unit.Services; }
		public IEnvironmentKind Unit { get => unit; set => unit = value; }

		public IUserServicePort addUserPort(string name, IDictionary<string, object> contract)
        {
            IUserServicePort user_port = new UserServicePort(this, name, 0, 0);
			addServicePort(new IUserServicePort[1] { user_port });
			Services.registerUsesPort(name, contract["name"] as string, new TypeMapImpl());
			return user_port;
		}

        public IProviderServicePort addProviderPort(string name, IDictionary<string, object> contract)
        {
            IProviderServicePort provider_port = new ProviderServicePort(this, name, 0, 0);
			addServicePort(new IProviderServicePort[1] { provider_port });
			Services.registerUsesPort(name, contract["name"] as string, new TypeMapImpl());

			return provider_port;
		}

		public IUserProviderServicePort addDirectUserPort(string name, IDictionary<string, object> contract)
		{
			IUserProviderServicePort user_port = new UserProviderServicePort(this, name, 0, 0);
			addServicePort(new IUserServicePort[1] { user_port });
			Services.registerUsesPort(name, contract["name"] as string, new TypeMapImpl());

			return user_port;
		}

		public IUserProviderServicePort addDirectProviderPort(string name, IDictionary<string, object> contract)
		{
			IUserProviderServicePort provider_port = new UserProviderServicePort(this, name, 0, 0);
			addServicePort(new IProviderServicePort[1] { provider_port });
			Services.registerUsesPort(name, contract["name"] as string, new TypeMapImpl());

			return provider_port;
		}

    }
}
