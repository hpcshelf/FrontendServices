﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.kinds;
using gov.cca;
using gov.cca.ports;

namespace HPCShelfDriver
{
    internal sealed class Workflow: ActionComponent, IWorkflow
    {
        private IWorkflowKind unit = null;
        private GoPort go_port = null;

        public Workflow(ISystem system_component, IPlatformSAFe platform_SAFe, CoreServices core_services) : base(system_component, Constants.WORKFLOW_COMPONENT_ID, new Dictionary<string, object>(), core_services, new Tuple<int, IVirtualPlatform>[1] { new Tuple<int, IVirtualPlatform>(0, platform_SAFe) })
        {
            new_action_port = new ActionPort(this, Constants.WORKFLOW_COMPONENT_ID, 0, 0);
            this.place = platform_SAFe;
            this.place.addComponent(this);
            createComponent();
           // Contract["name"] = CoreServices.getName_AbstractWorkflow();
		}

        private IPlatformSAFe place;

        public Services Services { get => unit.Services; }
        public IWorkflowKind Unit { get => unit; set => unit = value; }
        public GoPort Go { get => go_port; set => go_port = value; }

		public IActionPortWorkflow addActionPort(string name, string action_port_type)
        {
            IActionPortWorkflow port_return = new ActionPortWorkflow(this, name, action_port_type, CoreServices, Services);
            addActionPort(new IActionPortWorkflow[1] { port_return });
			return port_return;
        }

        internal IActionPortLifeCycle addLifeCyclePort(IComponent c)
		{
            IActionPortLifeCycle port_return = new ActionPortLifeCycle(this, c.Name, c.Name + ".lifecycle", CoreServices);
			addActionPort(new IActionPort[1] { port_return });
            addLifeCyclePortByKind(c, port_return);
            return port_return;
		}

        private IDictionary<string, int> kindOf = new Dictionary<string, int>();

        private void addLifeCyclePortByKind(IComponent c, IActionPortLifeCycle port_return)
        {
            if (c is IConnector)
            {
                IList<IActionPortLifeCycle> l;
                if (!action_life_cycle_port_bykind.TryGetValue(Constants.KIND_SYNCHRONIZER, out l))
                    action_life_cycle_port_bykind[Constants.KIND_SYNCHRONIZER] = l = new List<IActionPortLifeCycle>();
                kindOf[c.Name] = Constants.KIND_SYNCHRONIZER;
                l.Add(port_return);
            }
            else if (c is IComputation)
            {
                IList<IActionPortLifeCycle> l;
                if (!action_life_cycle_port_bykind.TryGetValue(Constants.KIND_COMPUTATION, out l))
                    action_life_cycle_port_bykind[Constants.KIND_COMPUTATION] = l = new List<IActionPortLifeCycle>();
                kindOf[c.Name] = Constants.KIND_COMPUTATION;
                l.Add(port_return);
            }
            else if (c is IDataSource)
            {
                IList<IActionPortLifeCycle> l;
                if (!action_life_cycle_port_bykind.TryGetValue(Constants.KIND_DATASTRUCTURE, out l))
                    action_life_cycle_port_bykind[Constants.KIND_DATASTRUCTURE] = l = new List<IActionPortLifeCycle>();
                kindOf[c.Name] = Constants.KIND_DATASTRUCTURE;
                l.Add(port_return);
            }
            else if (c is IBinding)
            {
                IList<IActionPortLifeCycle> l;
                if (!action_life_cycle_port_bykind.TryGetValue(Constants.KIND_BINDING, out l))
                    action_life_cycle_port_bykind[Constants.KIND_BINDING] = l = new List<IActionPortLifeCycle>();
                kindOf[c.Name] = Constants.KIND_BINDING;
                l.Add(port_return);
            }
            else if (c is IVirtualPlatform)
            {
                IList<IActionPortLifeCycle> l;
                if (!action_life_cycle_port_bykind.TryGetValue(Constants.KIND_PLATFORM, out l))
                    action_life_cycle_port_bykind[Constants.KIND_PLATFORM] = l = new List<IActionPortLifeCycle>();
                kindOf[c.Name] = Constants.KIND_PLATFORM;
                l.Add(port_return);
            }
            else
            {
                throw new Exception("Unexpected Kind !");
            }
        }

        private IDictionary<int, IList<IActionPortLifeCycle>> action_life_cycle_port_bykind = new Dictionary<int, IList<IActionPortLifeCycle>>();
        public IDictionary<int, IList<IActionPortLifeCycle>> ActionLifeCyclePortByKind { get { return action_life_cycle_port_bykind; } }

        public IDictionary<string, IActionPortLifeCycle> ActionPortLifeCycle => new Dictionary<string, IActionPortLifeCycle>(action_port_lifecycle);
		private readonly IDictionary<string, IActionPortLifeCycle> action_port_lifecycle = new Dictionary<string, IActionPortLifeCycle>();

        public IDictionary<string, IActionPortWorkflow> ActionPortWorkflow => new Dictionary<string, IActionPortWorkflow>(action_port_workflow);

        private IActionPort new_action_port = null;
        public IActionPort NewActionPort => new_action_port;

        private readonly IDictionary<string, IActionPortWorkflow> action_port_workflow = new Dictionary<string, IActionPortWorkflow>();

        protected override void addActionPort(IActionPort[] port)
        {
            base.addActionPort(port);
            if (port[0] is IActionPortLifeCycle)
                action_port_lifecycle[port[0].Name] = port[0] as IActionPortLifeCycle;
            else if (port[0] is IActionPortWorkflow)
				action_port_workflow[port[0].Name] = port[0] as IActionPortWorkflow;
		}

        public void releaseLifeCyclePort(string name)
        {
            string port_name = string.Format("{0}.lifecycle", name);
            IActionPortLifeCycle p = null;
            Console.WriteLine("releaseLifeCyclePort 1 {0}", name);
            if (action_port_lifecycle.ContainsKey(port_name))
            {
                Console.WriteLine("releaseLifeCyclePort 2 {0}", port_name);
                p = action_port_lifecycle[port_name];
                action_port_lifecycle.Remove(port_name);
                Console.WriteLine("releaseLifeCyclePort 3 {0}", port_name);

                if (kindOf.ContainsKey(name))
                {
                    Console.WriteLine("releaseLifeCyclePort 4 {0}", name);
                    if (action_life_cycle_port_bykind[kindOf[name]].Contains(p))
                    {
                        Console.WriteLine("releaseLifeCyclePort 5 {0}", name);
                        action_life_cycle_port_bykind[kindOf[name]].Remove(p);
                    }
                    kindOf.Remove(name);
                    Console.WriteLine("releaseLifeCyclePort 6 {0}", name);
                }
            }
        }

        /*       protected override void insertInCatalog()
               {
                   Contract["name"] = library_path_abstract = CoreServices.insertAbstractWorkflowInCatalog();
                   library_path_concrete = CoreServices.insertConcreteWorkflowInCatalog();
               }*/
    }
}
