﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;
using org.hpcshelf.DGAC;
using ComponentXML;
using HPCShelfDriver.classes;

namespace HPCShelfDriver
{
    internal abstract class Component : IComponent
    {
        readonly string cRef;
        private CoreServices core_services;
        protected ISystem system_component;
        private int id_abstract = 0;

        protected CoreServices CoreServices
        {
            set
            {
                CoreServices old_core_services = core_services;
                core_services = value;
              //  if (old_core_services == null && core_services != null)
               //     createComponent();
            }
            get 
            {
                return core_services;
            }
        }

        protected Component(ISystem system_component, string cRef, IDictionary<string, object> contract, CoreServices core_services, Tuple<int,IVirtualPlatform>[] placement)
        {
            this.system_component = system_component;
            this.cRef = cRef;
            this.placement = buildPlacement(placement);
            IDictionary<string, object> new_contract = new Dictionary<string, object>();
			determineContractVariables(contract, new_contract);
			this.contract = new_contract;
            this.core_services = core_services;
         }

        private IDictionary<int, IVirtualPlatform[]> buildPlacement(Tuple<int, IVirtualPlatform>[] placement_)
        {
			IDictionary<int, IVirtualPlatform[]> result = new Dictionary<int, IVirtualPlatform[]>();

			Dictionary<int, IList<IVirtualPlatform>> placement_list = new Dictionary<int, IList<IVirtualPlatform>>();
			foreach (Tuple<int, IVirtualPlatform> t in placement_)
			{
				IList<IVirtualPlatform> facet_placement;
				if (!placement_list.TryGetValue(t.Item1, out facet_placement))
					placement_list[t.Item1] = facet_placement = new List<IVirtualPlatform>();
				facet_placement.Add(t.Item2);
			}
			foreach (KeyValuePair<int, IList<IVirtualPlatform>> kv in placement_list)
			{
				IVirtualPlatform[] l = new IVirtualPlatform[kv.Value.Count];
				kv.Value.CopyTo(l, 0);
				result[kv.Key] = l;
			}

            return result;
		}

        protected void determineContractVariables(IDictionary<string, object> contract, IDictionary<string, object> new_contract)
        {
            if (contract.Count > 0)
            {
                string argument = (string)contract["name"];

				new_contract["name"] = system_component.VarMapping.ContainsKey(argument) ?
                                            ((IDictionary<string,object>)system_component.Contract[system_component.VarMapping[argument]])["name"] : contract["name"];

                contract.Remove("name");
                foreach (KeyValuePair<string, object> context_argument in contract)
                {
                    if (context_argument.Value is string)
                    {
                        new_contract[context_argument.Key] = system_component.Contract[system_component.VarMapping[(string)context_argument.Value]];
                    }
                    else if (context_argument.Value is IDictionary<string, object>)
                    {
                        IDictionary<string, object> context_argument_value = (IDictionary<string, object>)context_argument.Value;
                        IDictionary<string, object> context_argument_value_new = new Dictionary<string, object>();
                        determineContractVariables(context_argument_value, context_argument_value_new);
                        new_contract[context_argument.Key] = context_argument_value_new;
                    } else if (context_argument.Value is int)
                    {
                        new_contract[context_argument.Key] = contract[context_argument.Key];
					}
                    else if (context_argument.Value is decimal)
                    {
						new_contract[context_argument.Key] = contract[context_argument.Key];
					}
                }
                contract["name"] = argument;
            }
        }

        // DELAYING contract and core_services
        protected Component(string cRef)
		{
			this.cRef = cRef;
		}

		protected virtual void createComponent()
		{
            insertInCatalog();
        }

		protected virtual void insertInCatalog()
        {
            IDriverComponent driver_component = XMLConverter.fromIComponentToIDriverComponent(this);
			string driver_component_xml = LoaderApp.serialize<IDriverComponent>(driver_component);
            IDriverPortInfo driver_port_info = LoaderApp.deserialize<IDriverPortInfo>(CoreServices.insertComponent(driver_component_xml));
            if (driver_port_info.port != null)
            {
                Tuple<string, int, int, string>[] ports = new Tuple<string, int, int, string>[driver_port_info.port.Length];
                for (int i = 0; i < ports.Length; i++)
                    ports[i] = new Tuple<string, int, int, string>(driver_port_info.port[i].name, driver_port_info.port[i].index, driver_port_info.port[i].facet, driver_port_info.port[i].role);
                populatePorts(ports);
            }
        }

        protected virtual void populatePorts(Tuple<string, int, int, string>[] ports)
        {
            
        }

	//	protected BackEnd.ResolveComponentHierarchy resolution_tree;
    //    public BackEnd.ResolveComponentHierarchy ResolutionTree { set => resolution_tree = value; get => resolution_tree; }


        public string Name => cRef;

		private IDictionary<int, IVirtualPlatform[]> placement;
		public IDictionary<int, IVirtualPlatform[]> Placement { get { return placement; } }

		private IDictionary<string, object> contract;
		public IDictionary<string, object> Contract { get => contract; set => contract = value; }
    }
}
