﻿using System;
using System.Collections.Generic;
using System.IO;
using org.hpcshelf.core;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.kinds;
using ComponentXML;
using gov.cca;
using gov.cca.ports;
using HPCShelfDriver.classes;
using ENV = System.Environment;

namespace HPCShelfDriver
{
    public class SystemBuilder
    {
        public static ISystem build(string cRef, IDictionary<string, Tuple<string, object>> contextual_signature, out string session_id)
        {
            return new System(cRef, contextual_signature, out session_id);
        }

		public static ISystem build(string cRef, IDictionary<string, Tuple<string, object>> contextual_signature)
		{
			return new System(cRef, contextual_signature, out _);
		}
	}


    internal sealed class System : Component, ISystem
    {
        private string cRef;
        private PlatformSAFe platform_SAFe;
        private CoreServices core_services;
        private WorkerObject baseFramework = null;

        private IDictionary<string, Tuple<string, object>> contextual_signature;
		public IDictionary<string, Tuple<string, object>> ContextualSignature { get => contextual_signature; set => contextual_signature = value; }

		private IDictionary<string, string> var_mapping = new Dictionary<string, string>();
        public IDictionary<string, string> VarMapping { get { return new Dictionary<string, string>(var_mapping); } }

        public System(string cRef, IDictionary<string, Tuple<string, object>> contextual_signature, out string session_id) : base(cRef)
        {
            this.cRef = cRef;
            this.system_component = this;
            this.ContextualSignature = contextual_signature;

            // Building the DEFAULT contract of the system component, triavially calculated from the signature.
            IDictionary<string, object> default_contract = base.Contract = new Dictionary<string, object>();
            foreach (KeyValuePair<string, Tuple<string, object>> pair in contextual_signature)
            {
                default_contract[pair.Key] = pair.Value.Item2;
                if (!pair.Value.Item1.Equals("name"))
                    var_mapping[pair.Value.Item1] = pair.Key;
            }

            core_services = new CoreServices(cRef, out session_id);
            this.CoreServices = core_services;

            createComponent();

            Component[cRef] = VirtualPlatform[cRef] = platform_SAFe = new PlatformSAFe(this, core_services);

            workflow = new Workflow(this, platform_SAFe, core_services);
            application = new Application(this, platform_SAFe, core_services);

            core_services.open();

            core_services.updateContract(LoaderApp.serialize<IDriverContract>(XMLConverter.convertContract(this.Contract)));

            core_services.resolve(Constants.PLATFORM_SAFE_ID);
            core_services.resolve(Constants.APPLICATION_COMPONENT_ID);
            core_services.resolve(Constants.WORKFLOW_COMPONENT_ID);

            core_services.deploy(Constants.PLATFORM_SAFE_ID);              // *
            core_services.deploy(Constants.APPLICATION_COMPONENT_ID);      // *
            core_services.deploy(Constants.WORKFLOW_COMPONENT_ID);         // *

            core_services.instantiate(Constants.PLATFORM_SAFE_ID);         // *
			core_services.instantiate(Constants.APPLICATION_COMPONENT_ID); // *
			core_services.instantiate(Constants.WORKFLOW_COMPONENT_ID);    // *

            //baseFramework = (WorkerObject)ManagerObject.SingleManagerObject.WorkerFramework;
            baseFramework = (WorkerObject)((ManagerObject)core_services.Framework).WorkerFramework;

            IEnvironmentKind application_unit = (IEnvironmentKind)baseFramework.UnitOf[cRef + "." + Constants.APPLICATION_COMPONENT_ID];  // *
			this.application.Unit = application_unit;             // *

            IWorkflowKind workflow_unit = (IWorkflowKind)baseFramework.UnitOf[cRef + "." + Constants.WORKFLOW_COMPONENT_ID]; // *
			this.workflow.Unit = workflow_unit; // *

            Services appServices = baseFramework.getServices(cRef + ".shelf_framework", "HShelfFramework", baseFramework.createTypeMap());
            appServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
            ComponentID host_cid = appServices.getComponentID();
            BuilderService bsPort = (BuilderService)appServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

            ComponentID workflow_cid = bsPort.getComponentID(cRef + "." + Constants.WORKFLOW_COMPONENT_ID); // *
			ComponentID core_cid = core_services.Services.getComponentID();  // *
			conn_provenance = bsPort.connect(workflow_cid, Constants.CORE_PROVENANCE_PORT_NAME, core_cid, Constants.CORE_PROVENANCE_PORT_NAME); // *
			conn_lifecycle = bsPort.connect(workflow_cid, Constants.CORE_LIFECYCLE_PORT_NAME, core_cid, Constants.CORE_LIFECYCLE_PORT_NAME); // *

            go_port_name = workflow_cid.getInstanceName() + "_" + Constants.GO_PORT_NAME; // *
            appServices.registerUsesPort(go_port_name, Constants.GO_PORT_TYPE, new TypeMapImpl()); // *
            conn_go = bsPort.connect(host_cid, go_port_name, workflow_cid, Constants.GO_PORT_NAME); // *
            this.workflow.Go = (GoPort)appServices.getPort(go_port_name); // *

            appServices.releasePort(Constants.BUILDER_SERVICE_PORT_NAME);
            appServices.unregisterUsesPort(Constants.BUILDER_SERVICE_PORT_NAME);
		}

        string go_port_name;

        public void terminate()
        {
            foreach (IUserServicePort[] port_list in Application.UserPort.Values)
                foreach (IUserServicePort port in port_list)
                    Application.Services.releasePort(port.Name);

            if (workflow.ActionLifeCyclePortByKind.ContainsKey(Constants.KIND_SYNCHRONIZER))
                foreach (IActionPortLifeCycle p in workflow.ActionLifeCyclePortByKind[Constants.KIND_SYNCHRONIZER])
                    p.release();

            if (workflow.ActionLifeCyclePortByKind.ContainsKey(Constants.KIND_COMPUTATION))
                foreach (IActionPortLifeCycle p in workflow.ActionLifeCyclePortByKind[Constants.KIND_COMPUTATION])
                    p.release();

            if (workflow.ActionLifeCyclePortByKind.ContainsKey(Constants.KIND_DATASTRUCTURE))
                foreach (IActionPortLifeCycle p in workflow.ActionLifeCyclePortByKind[Constants.KIND_DATASTRUCTURE])
                    p.release();

            if (workflow.ActionLifeCyclePortByKind.ContainsKey(Constants.KIND_BINDING))
                foreach (IActionPortLifeCycle p in workflow.ActionLifeCyclePortByKind[Constants.KIND_BINDING])
                    p.release();

            if (workflow.ActionLifeCyclePortByKind.ContainsKey(Constants.KIND_PLATFORM))
                foreach (IActionPortLifeCycle p in workflow.ActionLifeCyclePortByKind[Constants.KIND_PLATFORM])
                    p.release();

            foreach (IUserServicePort[] port_list in Application.UserPort.Values)
                foreach (IUserServicePort port in port_list)
                    Application.Services.unregisterUsesPort(port.Name);

            foreach (IProviderServicePort[] port_list in Application.ProviderPort.Values)
                foreach (IProviderServicePort port in port_list)
                    Application.Services.removeProvidesPort(port.Name);

            Services appServices = baseFramework.getServices(cRef + ".shelf_framework", "HShelfFramework", baseFramework.createTypeMap());
            appServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
            ComponentID host_cid = appServices.getComponentID();
            BuilderService bsPort = (BuilderService)appServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

            appServices.releasePort(go_port_name);
            bsPort.disconnect(conn_go, int.MaxValue);
            appServices.unregisterUsesPort(go_port_name);

            bsPort.disconnect(conn_lifecycle, int.MaxValue);
            bsPort.disconnect(conn_provenance, int.MaxValue);

            appServices.releasePort(Constants.BUILDER_SERVICE_PORT_NAME);
            appServices.unregisterUsesPort(Constants.BUILDER_SERVICE_PORT_NAME);

            core_services.release(Constants.WORKFLOW_COMPONENT_ID);
            core_services.release(Constants.APPLICATION_COMPONENT_ID);
            core_services.release(Constants.PLATFORM_SAFE_ID);
            core_services.close();

            foreach (IComponent c in this.Component.Values)
                this.release(c.Name);
        }

        public void release(string name)
        {
            if (component.ContainsKey(name))
                component.Remove(name);
            if (computation.ContainsKey(name))
                computation.Remove(name);
            if (connector.ContainsKey(name))
                connector.Remove(name);
            if (virtualplatform.ContainsKey(name))
                virtualplatform.Remove(name);
            if (datasource.ContainsKey(name))
                datasource.Remove(name);
            if (binding.ContainsKey(name))
                binding.Remove(name);
            if (servicebinding.ContainsKey(name))
                servicebinding.Remove(name);
            if (actionbinding.ContainsKey(name))
                actionbinding.Remove(name);

            workflow.releaseLifeCyclePort(name);
        }

        private IWorkflow workflow;
        public IWorkflow Workflow { set => workflow = value; get => workflow; }

        private IApplication application;
        public IApplication Application { set => application = value; get => application; }

        private IDictionary<string, IComponent> component = new Dictionary<string, IComponent>();
        public IDictionary<string, IComponent> Component => new Dictionary<string, IComponent>(component);

        private IDictionary<string, IServiceSolutionComponent> solutioncomponent = new Dictionary<string, IServiceSolutionComponent>();
        public IDictionary<string, IServiceSolutionComponent> SolutionComponent => new Dictionary<string, IServiceSolutionComponent>(solutioncomponent);

        private IDictionary<string, IComputation> computation = new Dictionary<string, IComputation>();
        public IDictionary<string, IComputation> Computation => new Dictionary<string, IComputation>(computation);

        private IDictionary<string, IConnector> connector = new Dictionary<string, IConnector>();
        public IDictionary<string, IConnector> Connector => new Dictionary<string, IConnector>(connector);

        private IDictionary<string, IVirtualPlatform> virtualplatform = new Dictionary<string, IVirtualPlatform>();
        public IDictionary<string, IVirtualPlatform> VirtualPlatform => new Dictionary<string, IVirtualPlatform>(virtualplatform);

        private IDictionary<string, IDataSource> datasource = new Dictionary<string, IDataSource>();
        public IDictionary<string, IDataSource> DataSource => new Dictionary<string, IDataSource>(datasource);

        private IDictionary<string, IBinding> binding = new Dictionary<string, IBinding>();
        public IDictionary<string, IBinding> Binding => new Dictionary<string, IBinding>(binding);

        private IDictionary<string, IServiceBinding> servicebinding = new Dictionary<string, IServiceBinding>();
        public IDictionary<string, IServiceBinding> ServiceBinding => new Dictionary<string, IServiceBinding>(servicebinding);

        private IDictionary<string, IActionBinding> actionbinding = new Dictionary<string, IActionBinding>();
        public IDictionary<string, IActionBinding> ActionBinding => new Dictionary<string, IActionBinding>(actionbinding);

        private ConnectionID conn_provenance;
        private ConnectionID conn_lifecycle;
        private ConnectionID conn_go;


        public IPlatformSAFe PlatformSAFe => platform_SAFe;

        public IActionBinding newActionBinding(string cRef, string port_type, IActionPort[] action_port)
        {
            IDictionary<string, object> contract_ = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.binding.task.TaskBindingBase"},
                {"task_port_type", new Dictionary<string,object>() {{"name", port_type}}}
            };

            IDictionary<string, object> contract = new Dictionary<string, object>();
            determineContractVariables(contract_, contract);

            for (int i = 0; i < action_port.Length; i++)
            {
                IActionPort p = action_port[i];
                if (p.Name.Equals(Constants.WORKFLOW_COMPONENT_ID))
                {
                    action_port[i] = ((Workflow)p.Component).addActionPort(cRef, port_type);
                    break;
                }
            }

            IActionBinding new_binding = new ActionBinding(this, cRef, contract, action_port, core_services);
            component[cRef] = binding[cRef] = actionbinding[cRef] = new_binding;
            return new_binding;
        }

        public IComputation newComputation(string cRef, IDictionary<string, object> contract, IVirtualPlatform placement)
        {
            IComputation new_computation = new Computation(this, cRef, contract, placement, core_services);
            component[cRef] = solutioncomponent[cRef] = computation[cRef] = new_computation;
            return new_computation;
        }

        public IConnector newConnector(string cRef, IDictionary<string, object> contract, Tuple<int, IVirtualPlatform>[] facet_placement)
        {
            IConnector new_connector = new Connector(this, cRef, contract, core_services, facet_placement);
            component[cRef] = solutioncomponent[cRef] = connector[cRef] = new_connector;
            return new_connector;
        }

        public IDataSource newDataSource(string cRef, IDictionary<string, object> contract, IVirtualPlatform placement)
        {
            IDataSource new_datasource = new DataSource(this, cRef, contract, placement, core_services);
            component[cRef] = solutioncomponent[cRef] = datasource[cRef] = new_datasource;
            return new_datasource;
        }

        public IServiceBinding newServiceBinding(string cRef, IDictionary<string, object> contract, IUserProviderServicePort user_port, IUserProviderServicePort provider_port)
        {
            if (user_port.Name.Equals(Constants.APPLICATION_COMPONENT_ID))
                user_port = ((Application)user_port.Component).addDirectUserPort(cRef, contract);

            if (provider_port.Name.Equals(Constants.APPLICATION_COMPONENT_ID))
                provider_port = ((Application)provider_port.Component).addDirectProviderPort(cRef, contract);

            IServiceBinding new_servicebinding = new ServiceBinding(this, cRef, contract, user_port, provider_port, core_services);
            component[cRef] = binding[cRef] = servicebinding[cRef] = new_servicebinding;
            return new_servicebinding;
        }

        public IServiceBinding newServiceBinding(string cRef, IDictionary<string, object> contract, IUserServicePort user_port, IProviderServicePort provider_port)
        {
            if (user_port.Name.Equals(Constants.APPLICATION_COMPONENT_ID))
                if (provider_port is IUserProviderServicePort)
                    user_port = ((Application)user_port.Component).addDirectUserPort(cRef, contract);
                else if (provider_port is IProviderServicePort)
                    user_port = ((Application)user_port.Component).addUserPort(cRef, contract);

            if (provider_port.Name.Equals(Constants.APPLICATION_COMPONENT_ID))
                if (user_port is IUserProviderServicePort)
                    provider_port = ((Application)provider_port.Component).addDirectProviderPort(cRef, contract);
                else if (user_port is IUserServicePort)
                    provider_port = ((Application)provider_port.Component).addProviderPort(cRef, contract);

            IServiceBinding new_servicebinding = new ServiceBinding(this, cRef, contract, user_port, provider_port, core_services);
            component[cRef] = binding[cRef] = servicebinding[cRef] = new_servicebinding;
            return new_servicebinding;
        }


        public IVirtualPlatform newVirtualPlatform(string cRef, IDictionary<string, object> contract)
        {
            IVirtualPlatform new_virtualplatform = new FinalVirtualPlatform(this, cRef, contract, core_services);
            component[cRef] = virtualplatform[cRef] = new_virtualplatform;
            return new_virtualplatform;
        }

        public void createComputation(string qname, string[] sources)
        {
            createComputation(qname, null, sources, "MPI", "C");
        }

        public void createComputation(string qname, string[] sources, IDictionary<string, object>[] ports)
        {
            createComputation(qname, null, sources, ports, "MPI", "C");
        }

        public void createComputation(string qname, string[] sources, string mp_library, string language)
        {
            createComputation(qname, null, sources, mp_library, language);
        }

        public void createComputation(string qname, string[] sources, string language)
        {
            createComputation(qname, null, sources, "MPI", language);
        }

        public void createComputation(string qname, string wrapper_source_fn, string[] sources)
        {
            createComputation(qname, wrapper_source_fn, sources, "MPI", "C");
        }

        public void createComputation(string qname, string wrapper_source_fn, string[] sources, string language)
        {
            createComputation(qname, wrapper_source_fn, sources, null, "MPI", language);
        }

        public void createComputation(string qname, string wrapper_source_fn, string[] sources, string mp_library, string language)
        {
            createComputation(qname, wrapper_source_fn, sources, null, mp_library, language);
        }

        public void createComputation(string qname, string wrapper_source_fn, string[] sources, IDictionary<string, object>[] ports, string mp_library, string language)
        {
            if (mp_library.Equals("MPI") && language.Equals("C"))
                //createComputation_C_MPI(qname, wrapper_source_fn, sources, ports, language);
                createComputation_CPlusPlus_MPI(qname, wrapper_source_fn, sources, ports, language);
            else if (mp_library.Equals("MPI") && language.Equals("C++"))
                createComputation_CPlusPlus_MPI(qname, wrapper_source_fn, sources, ports, language);
            else if (mp_library.Equals("MPI") && language.Equals("Python"))
                createComputation_Python_MPI(qname, wrapper_source_fn, sources, ports, language);
            else
                throw new NotImplementedException();
        }

        private void createComputation_Python_MPI(string qname, string wrapper_source_fn, string[] sources, IDictionary<string, object>[] ports, string language)
        {
            string packagePath = qname.Substring(0, qname.LastIndexOf('.'));
            string name = qname.Substring(qname.LastIndexOf('.') + 1);

            // ABSTRACT COMPONENT

            string snk_fn_abstract = qname + ".snk";
            string pub_fn_abstract = qname + ".pub";

            string uid_abstract = FileUtil.create_snk_file(snk_fn_abstract, pub_fn_abstract);

            string hpe_file_name_abstract = Path.Combine(Constants.HPCShelf_HOME, ENV.GetEnvironmentVariable("TEMPLATE_CODE_ABSTRACT_MPI")); // TEMPLATE CODE ...
            string module_data_abstract = File.ReadAllText(hpe_file_name_abstract);
            ComponentType c_abstract = LoaderApp.deserialize<ComponentType>(module_data_abstract);

            c_abstract.header.hash_component_UID = uid_abstract;
            c_abstract.header.name = name;
            c_abstract.header.packagePath = packagePath;

            // look for the single interface element
            for (int k = 0; k < c_abstract.componentInfo.Length; k++)
            {
                object o = c_abstract.componentInfo[k];
                if (o is InterfaceType)
                {
                    InterfaceType it = (InterfaceType)o;
                    it.sources[0].moduleName = packagePath + "." + name + ".IProcessLauncher";
                    foreach (SourceFileType sft in it.sources[0].file)
                        sft.contents = sft.contents.Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);
                }
            }

            string module_data_out_abstract = LoaderApp.serialize<ComponentType>(c_abstract);

            File.WriteAllText(hpe_file_name_abstract + ".xxx", module_data_out_abstract);

            byte[] snk_contents_abstract = File.ReadAllBytes(snk_fn_abstract);

            CoreServices.register(module_data_out_abstract, snk_contents_abstract);

            // CONCRETE COMPONENT

            IDictionary<string, string[]> source_list = takeSources(sources);

            string snk_fn_concrete = qname + "_impl.snk";
            string pub_fn_concrete = qname + "_impl.pub";

            string uid_concrete = FileUtil.create_snk_file(snk_fn_concrete, pub_fn_concrete);

            string hpe_file_name_concrete = Path.Combine(Constants.HPCShelf_HOME, ENV.GetEnvironmentVariable("TEMPLATE_CODE_CONCRETE_MPI_PYTHON")); // TEMPLATE CODE ...
            string module_data_concrete = File.ReadAllText(hpe_file_name_concrete);
            ComponentType c_concrete = LoaderApp.deserialize<ComponentType>(module_data_concrete);

            // Configure the new computation component ...

            c_concrete.header.hash_component_UID = uid_concrete;
            c_concrete.header.name = name + "Impl";
            c_concrete.header.packagePath = packagePath;
            c_concrete.header.baseType.component.name = name;
            c_concrete.header.baseType.component.package = packagePath;
            c_concrete.header.baseType.component.hash_component_UID = uid_abstract;

            // look for the single interface element
            for (int k = 0; k < c_concrete.componentInfo.Length; k++)
            {
                object o = c_concrete.componentInfo[k];
                if (o is InterfaceType)
                {
                    InterfaceType it = (InterfaceType)o;
                    SourceType[] it_sources = new SourceType[source_list.Count + 2];

                    string main_module_name = null;
                    int i = 2;
                    foreach (KeyValuePair<string, string[]> source_item in source_list)
                    {
                        string module_name = source_item.Key;
                        if (main_module_name == null) main_module_name = module_name;
                        it_sources[i] = new SourceType();
                        it_sources[i].moduleName = module_name;
                        it_sources[i].deployType = "Python/MPI Interoperability";
                        it_sources[i].sourceType = language;
                        it_sources[i].file = new SourceFileType[source_item.Value.Length];
                        for (int j = 0; j < source_item.Value.Length; j++)
                        {
                            string file_path = source_item.Value[j];
                            string file_name = removeFirstDir(module_name, file_path).Trim(); // Path.GetFileName(file_path);

                            object file_ext = Path.GetExtension(file_name);
                            string srcType = file_ext.Equals(".py") ? "source" :
                                                                       file_ext.Equals(".bin") ? "binary" :
                                                                                                 file_ext.Equals(".txt") ? "text" :
                                                                                                                           "binary";
                            string fileType = file_ext.Equals(".py") ? "Python source code" :
                                                                       file_ext.Equals(".bin") ? "resource" :
                                                                                                 file_ext.Equals(".txt") ? "resource" :
                                                                                                                           "unkonwn";

                            it_sources[i].file[j] = new SourceFileType();
                            it_sources[i].file[j].srcType = srcType;
                            it_sources[i].file[j].name = file_name;
                            it_sources[i].file[j].fileType = fileType;
                            string contents = srcType.Equals("binary") ? Convert.ToBase64String(File.ReadAllBytes(file_path)) : File.ReadAllText(file_path);

                            it_sources[i].file[j].contents = contents;
                        }
                        i++;
                    }

                    it_sources[0] = it.sources[0];

                    it_sources[1] = it.sources[1];
                    it_sources[1].moduleName = packagePath + "." + name + "Impl.IProcessLauncherForPythonImpl";
                    foreach (SourceFileType sft in it.sources[1].file)
                    {
                        sft.contents = sft.contents.Replace("org.hpcshelf.mpi.impl.MPILauncherForPythonImpl", packagePath + "." + name + "Impl").Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);
                        sft.contents = sft.contents.Replace("<module_name>", main_module_name);
                        sft.contents = sft.contents.Replace("<file_name>", main_module_name + ".py");
                    }

                    if (wrapper_source_fn != null)
                        changeUserWrapperSource(it_sources[1], wrapper_source_fn);


                    it.sources = it_sources;
                }
            }

            string module_data_out = LoaderApp.serialize<ComponentType>(c_concrete);
            File.WriteAllText(hpe_file_name_concrete + ".xxx", module_data_out);

            byte[] snk_contents_concrete = File.ReadAllBytes(snk_fn_concrete);

            CoreServices.register(module_data_out, snk_contents_concrete);
        }

        private void createComputation_C_MPI(string qname, string wrapper_source_fn, string[] sources, IDictionary<string, object>[] ports, string language)
        {
            string packagePath = qname.Substring(0, qname.LastIndexOf('.'));
            string name = qname.Substring(qname.LastIndexOf('.') + 1);

            // ABSTRACT COMPONENT

            string snk_fn_abstract = qname + ".snk";
            string pub_fn_abstract = qname + ".pub";

            string uid_abstract = FileUtil.create_snk_file(snk_fn_abstract, pub_fn_abstract);

            string hpe_file_name_abstract = Path.Combine(Constants.HPCShelf_HOME, ENV.GetEnvironmentVariable("TEMPLATE_CODE_ABSTRACT_MPI")); // TEMPLATE CODE ...
            string module_data_abstract = File.ReadAllText(hpe_file_name_abstract);
            ComponentType c_abstract = LoaderApp.deserialize<ComponentType>(module_data_abstract);

            c_abstract.header.hash_component_UID = uid_abstract;
            c_abstract.header.name = name;
            c_abstract.header.packagePath = packagePath;

            // look for the single interface element
            for (int k = 0; k < c_abstract.componentInfo.Length; k++)
            {
                object o = c_abstract.componentInfo[k];
                if (o is InterfaceType)
                {
                    InterfaceType it = (InterfaceType)o;
                    it.sources[0].moduleName = packagePath + "." + name + ".IProcessLauncher";
                    foreach (SourceFileType sft in it.sources[0].file)
                        sft.contents = sft.contents.Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);
                }
            }

            if (ports != null)
            {
                IList<InnerComponentType> inner_port_list = new List<InnerComponentType>();
                IList<InnerComponentType> inner_client_port_type_list = new List<InnerComponentType>();
                IList<ParameterSupplyType> supply_client_port_type_list = new List<ParameterSupplyType>();
                IList<InterfaceSliceType> port_interface_slice_type_list = new List<InterfaceSliceType>();
                IList<InterfacePortType> port_interface_port_type_list = new List<InterfacePortType>();
                IList<UnitSliceType> port_unit_slice_type_list = new List<UnitSliceType>();

                for (int i = 0; i < ports.Length; i++)
                {
                    IDictionary<string, object> port_contract = ports[i];

                    string type = "intercommunicator_port_type";

                    string inner_port_name = ports.Length > 1 ? string.Format("port_{0}", i) : "port";
                    string inner_port_type_name = "intercommunicator_port_type";
                    string varS = ports.Length > 1 ? string.Format("S{0}", i) : "S";
                    string variable_name = varS;

                    string port_type = (string)((IDictionary<string, object>)port_contract[type])["name"];

                    InnerComponentType inner_port = new InnerComponentType();
                    inner_port_list.Add(inner_port);
                    inner_port.localRef = inner_port_name;
                    inner_port.multipleSpecified = true;
                    inner_port.multiple = false;
                    inner_port.name = "IntercommunicatorBinding";
                    inner_port.package = "org.hpcshelf.mpi.binding";
                    inner_port.exposedSpecified = true;
                    inner_port.exposed = true;
                    inner_port.parameter = new ParameterRenaming[1];
                    inner_port.parameter[0] = new ParameterRenaming();
                    inner_port.parameter[0].formFieldId = "intercommunicator_port_type";
                    inner_port.parameter[0].varName = varS;

                    InnerComponentType inner_client_port_type = new InnerComponentType();
                    inner_client_port_type_list.Add(inner_client_port_type);
                    inner_client_port_type.localRef = inner_port_type_name;
                    inner_client_port_type.multipleSpecified = true;
                    inner_client_port_type.multiple = false;
                    inner_client_port_type.name = port_type.Substring(port_type.LastIndexOf('.') + 1);
                    inner_client_port_type.package = port_type.Substring(0, port_type.LastIndexOf('.'));
                    inner_client_port_type.exposedSpecified = true;
                    inner_client_port_type.exposed = false;

                    ParameterSupplyType supply_client_port_type = new ParameterSupplyType();
                    supply_client_port_type_list.Add(supply_client_port_type);
                    supply_client_port_type.cRef = inner_port_type_name;
                    supply_client_port_type.direct = true;
                    supply_client_port_type.varName = variable_name;

                    InterfaceSliceType port_interface_slice_type = new InterfaceSliceType();
                    port_interface_slice_type_list.Add(port_interface_slice_type);
                    port_interface_slice_type.isRef = inner_port_name;
                    port_interface_slice_type.originRef = new InterfaceRefType();
                    port_interface_slice_type.originRef.cRef = inner_port_name;
                    port_interface_slice_type.originRef.iRef = "IIntercommunicatorBinding";

                    InterfacePortType port_interface_port_type = new InterfacePortType();
                    port_interface_port_type_list.Add(port_interface_port_type);
                    port_interface_port_type.name = inner_port_name;
                    port_interface_port_type.slice = new PortSliceType[1];
                    port_interface_port_type.slice[0] = new PortSliceType();
                    port_interface_port_type.slice[0].pRef = inner_port_name;
                    port_interface_port_type.slice[0].sRef = inner_port_name;

                    UnitSliceType port_unit_slice_type = new UnitSliceType();
                    port_unit_slice_type_list.Add(port_unit_slice_type);
                    port_unit_slice_type.cRef = inner_port_name;
                    port_unit_slice_type.inner_replica = 0;
                    port_unit_slice_type.slice_replica = 0;
                    port_unit_slice_type.uRef = "intercommunicator_binding";
                    port_unit_slice_type.sliceName = inner_port_name;
                    port_unit_slice_type.transitiveSpecified = true;
                    port_unit_slice_type.transitive = false;
                }

                object[] o_list = c_abstract.componentInfo;
                c_abstract.componentInfo = new object[o_list.Length + 3 * ports.Length];

                int j;
                for (j = 0; j < o_list.Length; j++)
                {
                    c_abstract.componentInfo[j] = o_list[j];

                    if (o_list[j] is InterfaceType)
                    {
                        InterfaceType i = (InterfaceType)o_list[j];

                        InterfaceSliceType[] slice = i.slice;
                        i.slice = new InterfaceSliceType[slice.Length + port_interface_slice_type_list.Count];
                        slice.CopyTo(i.slice, port_interface_slice_type_list.Count);
                        for (int jj = 0; jj < port_interface_slice_type_list.Count; jj++)
                            i.slice[jj] = port_interface_slice_type_list[jj];

                        InterfacePortType[] port = i.port;
                        i.port = new InterfacePortType[port.Length + port_interface_port_type_list.Count];
                        port.CopyTo(i.port, port_interface_port_type_list.Count);
                        for (int jj = 0; jj < port_interface_port_type_list.Count; jj++)
                            i.port[jj] = port_interface_port_type_list[jj];
                    }
                    else if (o_list[j] is UnitType)
                    {
                        UnitType u = (UnitType)o_list[j];

                        UnitSliceType[] slice = u.slices;
                        u.slices = new UnitSliceType[slice.Length + port_unit_slice_type_list.Count];
                        slice.CopyTo(u.slices, port_unit_slice_type_list.Count);
                        for (int jj = 0; jj < port_unit_slice_type_list.Count; jj++)
                            u.slices[jj] = port_unit_slice_type_list[jj];
                    }
                }

                foreach (InnerComponentType item in inner_port_list)
                    c_abstract.componentInfo[j++] = item;
                foreach (InnerComponentType item in inner_client_port_type_list)
                    c_abstract.componentInfo[j++] = item;
                foreach (ParameterSupplyType item in supply_client_port_type_list)
                    c_abstract.componentInfo[j++] = item;
            }

            string module_data_out_abstract = LoaderApp.serialize<ComponentType>(c_abstract);

            File.WriteAllText(hpe_file_name_abstract + ".xxx", module_data_out_abstract);

            byte[] snk_contents_abstract = File.ReadAllBytes(snk_fn_abstract);

            CoreServices.register(module_data_out_abstract, snk_contents_abstract);

            // CONCRETE COMPONENT

            IDictionary<string, string[]> source_list = takeSources(sources);

            string snk_fn_concrete = qname + "_impl.snk";
            string pub_fn_concrete = qname + "_impl.pub";

            string uid_concrete = FileUtil.create_snk_file(snk_fn_concrete, pub_fn_concrete);

            string hpe_file_name_concrete = Path.Combine(Constants.HPCShelf_HOME, ENV.GetEnvironmentVariable("TEMPLATE_CODE_CONCRETE_MPI_C")); // TEMPLATE CODE ...
            string module_data_concrete = File.ReadAllText(hpe_file_name_concrete);
            ComponentType c_concrete = LoaderApp.deserialize<ComponentType>(module_data_concrete);

            // Configure the new computation component ...

            c_concrete.header.hash_component_UID = uid_concrete;
            c_concrete.header.name = name + "Impl";
            c_concrete.header.packagePath = packagePath;
            c_concrete.header.baseType.component.name = name;
            c_concrete.header.baseType.component.package = packagePath;
            c_concrete.header.baseType.component.hash_component_UID = uid_abstract;

            // look for the single interface element
            for (int k = 0; k < c_concrete.componentInfo.Length; k++)
            {
                object o = c_concrete.componentInfo[k];
                if (o is InterfaceType)
                {
                    InterfaceType it = (InterfaceType)o;
                    SourceType[] it_sources = new SourceType[source_list.Count + 1];

                    string main_module_name = null;
                    int i = 1;
                    foreach (KeyValuePair<string, string[]> source_item in source_list)
                    {
                        string module_name = source_item.Key;
                        if (main_module_name == null) main_module_name = module_name;
                        it_sources[i] = new SourceType();
                        it_sources[i].moduleName = module_name;
                        it_sources[i].deployType = /*module_name.Equals("mpirun") ?*/ "C/MPI Interoperability" /*: "C Interoperability"*/;
                        it_sources[i].sourceType = language;
                        it_sources[i].file = new SourceFileType[source_item.Value.Length];
                        for (int j = 0; j < source_item.Value.Length; j++)
                        {
                            string file_path = source_item.Value[j];
                            string contents = File.ReadAllText(file_path);
                            string file_name = Path.GetFileName(file_path);

                            object file_ext = Path.GetExtension(file_name);
                            string srcType = file_ext.Equals(".h") ? "header" : file_ext.Equals(".c") ? "source" : "unkown";

                            it_sources[i].file[j] = new SourceFileType();
                            it_sources[i].file[j].srcType = srcType;
                            it_sources[i].file[j].name = file_name;
                            it_sources[i].file[j].fileType = "C source code";
                            it_sources[i].file[j].contents = contents;
                        }
                        i++;
                    }

                    it_sources[0] = it.sources[0];
                    it_sources[0].moduleName = packagePath + "." + name + "Impl.IProcessLauncherForCImpl";
                    foreach (SourceFileType sft in it.sources[0].file)
                    {
                        sft.contents = sft.contents.Replace("org.hpcshelf.mpi.impl.MPILauncherForCImpl", packagePath + "." + name + "Impl").Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);
                        sft.contents = sft.contents.Replace("mpirun.so", main_module_name + ".so");
                    }

                    if (wrapper_source_fn != null)
                        changeUserWrapperSource(it_sources[0], wrapper_source_fn);


                    it.sources = it_sources;
                }
            }

            if (ports != null)
            {
                IList<InnerComponentType> inner_port_list = new List<InnerComponentType>();
                IList<InnerComponentType> inner_client_port_type_list = new List<InnerComponentType>();
                IList<ParameterSupplyType> supply_client_port_type_list = new List<ParameterSupplyType>();

                for (int i = 0; i < ports.Length; i++)
                {
                    IDictionary<string, object> port_contract = ports[i];

                    string type = "intercommunicator_port_type";

                    string inner_port_name = ports.Length > 1 ? string.Format("port_{0}", i) : "port";
                    string inner_port_type_name = "intercommunicator_port_type";
                    string varS = ports.Length > 1 ? string.Format("S{0}", i) : "S";
                    string variable_name = varS;

                    string port_type = (string)((IDictionary<string, object>)port_contract[type])["name"];

                    InnerComponentType inner_port = new InnerComponentType();
                    inner_port_list.Add(inner_port);
                    inner_port.index_replica = 0;
                    inner_port.localRef = inner_port_name;
                    inner_port.multipleSpecified = true;
                    inner_port.multiple = false;
                    inner_port.name = "IntercommunicatorBinding";
                    inner_port.package = "org.hpcshelf.mpi.binding";
                    inner_port.exposedSpecified = true;
                    inner_port.exposed = true;
                    inner_port.parameter = new ParameterRenaming[1];
                    inner_port.parameter[0] = new ParameterRenaming();
                    inner_port.parameter[0].formFieldId = "intercommunicator_port_type";
                    inner_port.parameter[0].varName = varS;

                    InnerComponentType inner_client_port_type = new InnerComponentType();
                    inner_client_port_type_list.Add(inner_client_port_type);
                    inner_client_port_type.localRef = inner_port_type_name;
                    inner_client_port_type.multipleSpecified = true;
                    inner_client_port_type.multiple = false;
                    inner_client_port_type.name = port_type.Substring(port_type.LastIndexOf('.') + 1);
                    inner_client_port_type.package = port_type.Substring(0, port_type.LastIndexOf('.'));
                    inner_client_port_type.exposedSpecified = true;
                    inner_client_port_type.exposed = false;

                    ParameterSupplyType supply_client_port_type = new ParameterSupplyType();
                    supply_client_port_type_list.Add(supply_client_port_type);
                    supply_client_port_type.cRef = inner_port_type_name;
                    supply_client_port_type.direct = true;
                    supply_client_port_type.varName = variable_name;
                }

                object[] o_list = c_concrete.componentInfo;
                c_concrete.componentInfo = new object[o_list.Length + 3 * ports.Length];

                int j;
                for (j = 0; j < o_list.Length; j++)
                    c_concrete.componentInfo[j] = o_list[j];

                foreach (InnerComponentType item in inner_port_list)
                    c_concrete.componentInfo[j++] = item;
                foreach (InnerComponentType item in inner_client_port_type_list)
                    c_concrete.componentInfo[j++] = item;
                foreach (ParameterSupplyType item in supply_client_port_type_list)
                    c_concrete.componentInfo[j++] = item;
            }

            string module_data_out = LoaderApp.serialize<ComponentType>(c_concrete);
            File.WriteAllText(hpe_file_name_concrete + ".xxx", module_data_out);

            byte[] snk_contents_concrete = File.ReadAllBytes(snk_fn_concrete);

            CoreServices.register(module_data_out, snk_contents_concrete);
        }

        private void createComputation_CPlusPlus_MPI(string qname, string wrapper_source_fn, string[] sources, IDictionary<string, object>[] ports, string language)
        {
            string packagePath = qname.Substring(0, qname.LastIndexOf('.'));
            string name = qname.Substring(qname.LastIndexOf('.') + 1);

            // ABSTRACT COMPONENT

            string snk_fn_abstract = qname + ".snk";
            string pub_fn_abstract = qname + ".pub";

            string uid_abstract = FileUtil.create_snk_file(snk_fn_abstract, pub_fn_abstract);

            string hpe_file_name_abstract = ENV.GetEnvironmentVariable("TEMPLATE_CODE_ABSTRACT_MPI"); // TEMPLATE CODE ...
            string module_data_abstract = File.ReadAllText(hpe_file_name_abstract);
            ComponentType c_abstract = LoaderApp.deserialize<ComponentType>(module_data_abstract);

            c_abstract.header.hash_component_UID = uid_abstract;
            c_abstract.header.name = name;
            c_abstract.header.packagePath = packagePath;

            // look for the single interface element
            for (int k = 0; k < c_abstract.componentInfo.Length; k++)
            {
                object o = c_abstract.componentInfo[k];
                if (o is InterfaceType)
                {
                    InterfaceType it = (InterfaceType)o;
                    it.sources[0].moduleName = packagePath + "." + name + ".IProcessLauncher";
                    foreach (SourceFileType sft in it.sources[0].file)
                        sft.contents = sft.contents.Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);
                }
            }

            if (ports != null)
            {
                IList<InnerComponentType> inner_port_list = new List<InnerComponentType>();
                IList<InnerComponentType> inner_client_port_type_list = new List<InnerComponentType>();
                IList<ParameterSupplyType> supply_client_port_type_list = new List<ParameterSupplyType>();
                IList<InterfaceSliceType> port_interface_slice_type_list = new List<InterfaceSliceType>();
                IList<InterfacePortType> port_interface_port_type_list = new List<InterfacePortType>();
                IList<UnitSliceType> port_unit_slice_type_list = new List<UnitSliceType>();

                for (int i = 0; i < ports.Length; i++)
                {
                    IDictionary<string, object> port_contract = ports[i];

                    string type = "intercommunicator_port_type";

                    string inner_port_name = ports.Length > 1 ? string.Format("port_{0}", i) : "port";
                    string inner_port_type_name = "intercommunicator_port_type";
                    string varS = ports.Length > 1 ? string.Format("S{0}", i) : "S";
                    string variable_name = varS;

                    string port_type = (string)((IDictionary<string, object>)port_contract[type])["name"];

                    InnerComponentType inner_port = new InnerComponentType();
                    inner_port_list.Add(inner_port);
                    inner_port.localRef = inner_port_name;
                    inner_port.multipleSpecified = true;
                    inner_port.multiple = false;
                    inner_port.name = "IntercommunicatorBinding";
                    inner_port.package = "org.hpcshelf.mpi.binding";
                    inner_port.exposedSpecified = true;
                    inner_port.exposed = true;
                    inner_port.parameter = new ParameterRenaming[1];
                    inner_port.parameter[0] = new ParameterRenaming();
                    inner_port.parameter[0].formFieldId = "intercommunicator_port_type";
                    inner_port.parameter[0].varName = varS;

                    InnerComponentType inner_client_port_type = new InnerComponentType();
                    inner_client_port_type_list.Add(inner_client_port_type);
                    inner_client_port_type.localRef = inner_port_type_name;
                    inner_client_port_type.multipleSpecified = true;
                    inner_client_port_type.multiple = false;
                    inner_client_port_type.name = port_type.Substring(port_type.LastIndexOf('.') + 1);
                    inner_client_port_type.package = port_type.Substring(0, port_type.LastIndexOf('.'));
                    inner_client_port_type.exposedSpecified = true;
                    inner_client_port_type.exposed = false;

                    ParameterSupplyType supply_client_port_type = new ParameterSupplyType();
                    supply_client_port_type_list.Add(supply_client_port_type);
                    supply_client_port_type.cRef = inner_port_type_name;
                    supply_client_port_type.direct = true;
                    supply_client_port_type.varName = variable_name;

                    InterfaceSliceType port_interface_slice_type = new InterfaceSliceType();
                    port_interface_slice_type_list.Add(port_interface_slice_type);
                    port_interface_slice_type.isRef = inner_port_name;
                    port_interface_slice_type.originRef = new InterfaceRefType();
                    port_interface_slice_type.originRef.cRef = inner_port_name;
                    port_interface_slice_type.originRef.iRef = "IIntercommunicatorBinding";

                    InterfacePortType port_interface_port_type = new InterfacePortType();
                    port_interface_port_type_list.Add(port_interface_port_type);
                    port_interface_port_type.name = inner_port_name;
                    port_interface_port_type.slice = new PortSliceType[1];
                    port_interface_port_type.slice[0] = new PortSliceType();
                    port_interface_port_type.slice[0].pRef = inner_port_name;
                    port_interface_port_type.slice[0].sRef = inner_port_name;

                    UnitSliceType port_unit_slice_type = new UnitSliceType();
                    port_unit_slice_type_list.Add(port_unit_slice_type);
                    port_unit_slice_type.cRef = inner_port_name;
                    port_unit_slice_type.inner_replica = 0;
                    port_unit_slice_type.slice_replica = 0;
                    port_unit_slice_type.uRef = "intercommunicator_binding";
                    port_unit_slice_type.sliceName = inner_port_name;
                    port_unit_slice_type.transitiveSpecified = true;
                    port_unit_slice_type.transitive = false;
                }

                object[] o_list = c_abstract.componentInfo;
                c_abstract.componentInfo = new object[o_list.Length + 3 * ports.Length];

                int j;
                for (j = 0; j < o_list.Length; j++)
                {
                    c_abstract.componentInfo[j] = o_list[j];

                    if (o_list[j] is InterfaceType)
                    {
                        InterfaceType i = (InterfaceType)o_list[j];

                        InterfaceSliceType[] slice = i.slice;
                        i.slice = new InterfaceSliceType[slice.Length + port_interface_slice_type_list.Count];
                        slice.CopyTo(i.slice, port_interface_slice_type_list.Count);
                        for (int jj = 0; jj < port_interface_slice_type_list.Count; jj++)
                            i.slice[jj] = port_interface_slice_type_list[jj];

                        InterfacePortType[] port = i.port;
                        i.port = new InterfacePortType[port.Length + port_interface_port_type_list.Count];
                        port.CopyTo(i.port, port_interface_port_type_list.Count);
                        for (int jj = 0; jj < port_interface_port_type_list.Count; jj++)
                            i.port[jj] = port_interface_port_type_list[jj];
                    }
                    else if (o_list[j] is UnitType)
                    {
                        UnitType u = (UnitType)o_list[j];

                        UnitSliceType[] slice = u.slices;
                        u.slices = new UnitSliceType[slice.Length + port_unit_slice_type_list.Count];
                        slice.CopyTo(u.slices, port_unit_slice_type_list.Count);
                        for (int jj = 0; jj < port_unit_slice_type_list.Count; jj++)
                            u.slices[jj] = port_unit_slice_type_list[jj];
                    }
                }

                foreach (InnerComponentType item in inner_port_list)
                    c_abstract.componentInfo[j++] = item;
                foreach (InnerComponentType item in inner_client_port_type_list)
                    c_abstract.componentInfo[j++] = item;
                foreach (ParameterSupplyType item in supply_client_port_type_list)
                    c_abstract.componentInfo[j++] = item;
            }

            string module_data_out_abstract = LoaderApp.serialize<ComponentType>(c_abstract);

            File.WriteAllText(hpe_file_name_abstract + ".xxx", module_data_out_abstract);

            byte[] snk_contents_abstract = File.ReadAllBytes(snk_fn_abstract);

            CoreServices.register(module_data_out_abstract, snk_contents_abstract);

            // CONCRETE COMPONENT

            IDictionary<string, string[]> source_list = takeSources(sources);

            string snk_fn_concrete = qname + "_impl.snk";
            string pub_fn_concrete = qname + "_impl.pub";

            string uid_concrete = FileUtil.create_snk_file(snk_fn_concrete, pub_fn_concrete);

            string hpe_file_name_concrete = ENV.GetEnvironmentVariable("TEMPLATE_CODE_CONCRETE_MPI_CPLUSPLUS"); // TEMPLATE CODE ...
            string module_data_concrete = File.ReadAllText(hpe_file_name_concrete);
            ComponentType c_concrete = LoaderApp.deserialize<ComponentType>(module_data_concrete);

            // Configure the new computation component ...

            c_concrete.header.hash_component_UID = uid_concrete;
            c_concrete.header.name = name + "Impl";
            c_concrete.header.packagePath = packagePath;
            c_concrete.header.baseType.component.name = name;
            c_concrete.header.baseType.component.package = packagePath;
            c_concrete.header.baseType.component.hash_component_UID = uid_abstract;

            // look for the single interface element
            for (int k = 0; k < c_concrete.componentInfo.Length; k++)
            {
                object o = c_concrete.componentInfo[k];
                if (o is InterfaceType)
                {
                    InterfaceType it = (InterfaceType)o;
                    SourceType[] it_sources = new SourceType[source_list.Count + 2];

                    string main_module_name = null;
                    int i = 2;
                    foreach (KeyValuePair<string, string[]> source_item in source_list)
                    {
                        string module_name = source_item.Key;
                        if (main_module_name == null) main_module_name = module_name;
                        it_sources[i] = new SourceType();
                        it_sources[i].moduleName = module_name;
                        it_sources[i].deployType = /*module_name.Equals("mpirun") ?*/ "C++ Interoperability" /*: "C Interoperability"*/;
                        it_sources[i].sourceType = language;
                        it_sources[i].file = new SourceFileType[source_item.Value.Length];
                        for (int j = 0; j < source_item.Value.Length; j++)
                        {
                            string file_path = source_item.Value[j];
                            string file_name = removeFirstDir(module_name, file_path).Trim(); // Path.GetFileName(file_path);

                            string fileType, srcType, fileExt;

                            fileTypeCPlusPlus(file_name, out srcType, out fileType, out fileExt);

                            it_sources[i].file[j] = new SourceFileType();
                            it_sources[i].file[j].srcType = srcType;
                            it_sources[i].file[j].name = file_name;
                            it_sources[i].file[j].fileType = fileType;

                            string contents = srcType.Equals("binary") ? Convert.ToBase64String(File.ReadAllBytes(file_path)) : File.ReadAllText(file_path);

                            it_sources[i].file[j].contents = contents;

                            Console.WriteLine("###### FILE: {0} -- SIZE={1} -- extension={2}", file_name, contents.Length, fileExt);
                        }
                        i++;
                    }

                    it_sources[0] = it.sources[0];
                    it_sources[1] = it.sources[1];
                    it_sources[1].moduleName = packagePath + "." + name + "Impl.IProcessLauncherForCPlusPlusImpl";
                    foreach (SourceFileType sft in it.sources[1].file)
                    {
                        sft.contents = sft.contents.Replace("org.hpcshelf.mpi.impl.MPILauncherForCPlusPlusImpl", packagePath + "." + name + "Impl").Replace("org.hpcshelf.mpi.MPILauncher", packagePath + "." + name);
                        sft.contents = sft.contents.Replace("<module_name>", main_module_name);
                        sft.contents = sft.contents.Replace("<file_name>", main_module_name + ".exe");
                    }

                    if (wrapper_source_fn != null)
                        changeUserWrapperSource(it_sources[1], wrapper_source_fn);


                    it.sources = it_sources;
                }
            }

            if (ports != null)
            {
                IList<InnerComponentType> inner_port_list = new List<InnerComponentType>();
                IList<InnerComponentType> inner_client_port_type_list = new List<InnerComponentType>();
                IList<ParameterSupplyType> supply_client_port_type_list = new List<ParameterSupplyType>();

                for (int i = 0; i < ports.Length; i++)
                {
                    IDictionary<string, object> port_contract = ports[i];

                    string type = "intercommunicator_port_type";

                    string inner_port_name = ports.Length > 1 ? string.Format("port_{0}", i) : "port";
                    string inner_port_type_name = "intercommunicator_port_type";
                    string varS = ports.Length > 1 ? string.Format("S{0}", i) : "S";
                    string variable_name = varS;

                    string port_type = (string)((IDictionary<string, object>)port_contract[type])["name"];

                    InnerComponentType inner_port = new InnerComponentType();
                    inner_port_list.Add(inner_port);
                    inner_port.index_replica = 0;
                    inner_port.localRef = inner_port_name;
                    inner_port.multipleSpecified = true;
                    inner_port.multiple = false;
                    inner_port.name = "IntercommunicatorBinding";
                    inner_port.package = "org.hpcshelf.mpi.binding";
                    inner_port.exposedSpecified = true;
                    inner_port.exposed = true;
                    inner_port.parameter = new ParameterRenaming[1];
                    inner_port.parameter[0] = new ParameterRenaming();
                    inner_port.parameter[0].formFieldId = "intercommunicator_port_type";
                    inner_port.parameter[0].varName = varS;

                    InnerComponentType inner_client_port_type = new InnerComponentType();
                    inner_client_port_type_list.Add(inner_client_port_type);
                    inner_client_port_type.localRef = inner_port_type_name;
                    inner_client_port_type.multipleSpecified = true;
                    inner_client_port_type.multiple = false;
                    inner_client_port_type.name = port_type.Substring(port_type.LastIndexOf('.') + 1);
                    inner_client_port_type.package = port_type.Substring(0, port_type.LastIndexOf('.'));
                    inner_client_port_type.exposedSpecified = true;
                    inner_client_port_type.exposed = false;

                    ParameterSupplyType supply_client_port_type = new ParameterSupplyType();
                    supply_client_port_type_list.Add(supply_client_port_type);
                    supply_client_port_type.cRef = inner_port_type_name;
                    supply_client_port_type.direct = true;
                    supply_client_port_type.varName = variable_name;
                }

                object[] o_list = c_concrete.componentInfo;
                c_concrete.componentInfo = new object[o_list.Length + 3 * ports.Length];

                int j;
                for (j = 0; j < o_list.Length; j++)
                    c_concrete.componentInfo[j] = o_list[j];

                foreach (InnerComponentType item in inner_port_list)
                    c_concrete.componentInfo[j++] = item;
                foreach (InnerComponentType item in inner_client_port_type_list)
                    c_concrete.componentInfo[j++] = item;
                foreach (ParameterSupplyType item in supply_client_port_type_list)
                    c_concrete.componentInfo[j++] = item;
            }

            string module_data_out = LoaderApp.serialize<ComponentType>(c_concrete);
            File.WriteAllText(hpe_file_name_concrete + ".xxx", module_data_out);

            byte[] snk_contents_concrete = File.ReadAllBytes(snk_fn_concrete);

            CoreServices.register(module_data_out, snk_contents_concrete);
        }

        private void fileTypeCPlusPlus(string file_name, out string srcType, out string fileType, out string fileExt)
        {
            fileExt = Path.GetExtension(file_name);

            if (file_name.ToLower().Contains("makefile"))
            {
                srcType = "make";
                fileType = "build";
            }
            else
            {
                srcType = fileExt.Equals(".h") || fileExt.Equals(".hpp") || fileExt.Equals(".cuh") ? "header" :
                                                         fileExt.Equals(".c") || fileExt.Equals(".cpp") || fileExt.Equals(".cu") ? "source" :
                                                                                                            fileExt.Equals(".bin") ? "binary" :
                                                                                                                                      fileExt.Equals(".txt") ? "text" :
                                                                                                                                                                "binary";
                fileType = fileExt.Equals(".h") || fileExt.Equals(".hpp") || fileExt.Equals(".c") || fileExt.Equals(".cpp")  ? 
                                       "C++ source code" :
                                       fileExt.Equals(".cuh") || fileExt.Equals(".cuh") ?
                                                "CUDA source code" :
                                                fileExt.Equals(".bin") ? "resource" : fileExt.Equals(".txt") ? "resource" :
                                                                                                               "unkonwn";
            }
        }

        private static void changeUserWrapperSource(SourceType st, string fn)
        {
            if (!File.Exists(fn))
                throw new Exception("Wrapper source file name not found (" + fn + ")");

            foreach (SourceFileType sft in st.file)
                if (sft.srcType.Equals("user"))
                    sft.contents = File.ReadAllText(fn);
        }

        private static IDictionary<string, string[]> takeSources(string[] sources)
        {
            IDictionary<string, string[]> result = new Dictionary<string, string[]>();

            foreach (string file_descriptor in sources)
            {
                // Test whether the file_descriptor is a directory or not
                if (Directory.Exists(file_descriptor))
                    result[file_descriptor] = Directory.GetFiles(file_descriptor, "*.*", SearchOption.AllDirectories);
//                result[file_descriptor] = removeFirstDir(file_descriptor, Directory.GetFiles(file_descriptor, "*.*", SearchOption.AllDirectories));
                    // Otherwise, test whether it is a file_name or not (in this case, take all files with with some extension).
                else
                {
                    string current_directory = ".";
                    result[file_descriptor] = Directory.GetFiles(current_directory, file_descriptor + ".*");
                }
            }

            return result;
        }

        private static string removeFirstDir(string file_descriptor, string source_path_in)
        {
            string source_path_out = source_path_in.Remove(0, source_path_in.IndexOf(Path.DirectorySeparatorChar) + 1);
            return source_path_out;
        }

        public string createConfiguration(string system_namespace)
        {
            ComponentType c = new ComponentType();

            //c.header.baseType;
            //c.header.hash_component_UID;
            //c.header.visualAttributes;
            //c.header.locationURI;

            c.header.facet_configuration = new FacetConfigurationType[VirtualPlatform.Count + 1];
            int facet_instance = 0;
            foreach (FacetConfigurationType fct in c.header.facet_configuration)
            {
                fct.facet = facet_instance++;
                fct.multiple = false;
            }
            c.header.isAbstract = true;
            c.header.isAbstractSpecified = true;
            c.header.kind = SupportedKinds.System;
            c.header.kindSpecified = true;
            c.header.name = "System";
            c.header.packagePath = system_namespace;
            c.header.versions = new VersionType[1];
            c.header.versions[0].field1 = c.header.versions[0].field2 = c.header.versions[0].field3 = c.header.versions[0].field4 = 0;

            IList<object> component_info_list = new List<object>();

            // PARAMETERS
            setParameters(component_info_list);

            // INNER COMPONENTS
            setInnerComponents(component_info_list);

            // INNER RENAMINGS
            setInnerRenamingsFromBindings(component_info_list);
            setInnerRenamingsFromVirtualPlatforms(component_info_list);

            // FUSIONS
            setFusionsFromBindings(component_info_list);
            setFusionsFromVirtualPlatforms(component_info_list);

            // UNITS AND INTERFACES
            setUnits(component_info_list);

            c.componentInfo = new object[component_info_list.Count];
            component_info_list.CopyTo(c.componentInfo, 0);

            string cX = LoaderApp.serialize<ComponentType>(c);

            return cX;
        }

        private void setUnits(IList<object> component_info_list)
        {
            int facet_instance = 0;

            IList<Tuple<string, string, bool>> slices = new List<Tuple<string, string, bool>>();

            // DETERMINE THE SET OF SLICES ...
            foreach (KeyValuePair<string, IVirtualPlatform> p in VirtualPlatform)
            {
                IVirtualPlatform virtual_platform = p.Value;

                slices.Add(new Tuple<string, string, bool>(virtual_platform.Name, "node", true));
                foreach (IServiceSolutionComponent c in virtual_platform.Component)
                {
                    slices.Add(new Tuple<string, string, bool>(c.Name, null, false));
                    foreach (IPort[] c_port_list in c.Port.Values)
                    {
                        foreach (IPort c_port in c_port_list)
                        {
                            IBinding p_binding = c_port.Binding;
                            foreach (IPort other_port in p_binding.Port)
                            {
                                // This connector is placed in the current virtual platform.
                                // It is necessary to take the facet unit.
                                // This connector may be touched several times in this loop. In each time, the facet unit must be the same. Check !

                                IComponent c_partner = other_port.Component;

                                IVirtualPlatform partner_placement = virtual_platform; // default for connectors
                                if (c_partner is IComputation)
                                    partner_placement = ((IComputation)c_partner).VirtualPlatform;
                                if (c_partner is IDataSource)
                                    partner_placement = ((IDataSource)c_partner).VirtualPlatform;

                                string u_name = p_binding is IActionBinding ? "peer" :
                                                partner_placement == virtual_platform ? "client_server" :
                                                c_port is IUserServicePort ? "client" :
                                             /* c_port is ProviderSErvicePort */        "server";

                                if (partner_placement == virtualplatform && (c_partner == c || c_partner is IConnector))
                                    slices.Add(new Tuple<string, string, bool>(p_binding.Name, u_name, true));

                                // Add the connector slice
                                if (c_partner is IConnector)
                                    slices.Add(new Tuple<string, string, bool>(c_partner.Name, null, false));
                            }
                        }
                    }
                }



                InterfaceType i = new InterfaceType();
                component_info_list.Add(i);

                //i.action;
                //i.condition;
                //i.externalReferences;
                i.iRef = "Interface_" + facet_instance;
                //TODO            i.nArgs;
                //TODO            i.nArgsSpecified;
                //TODO            i.parameter;
                //TODO            i.port;
                //i.protocol;
                //TODO            i.slice;
                //TODO            i.sources;
                //i.visualDescription;


                UnitType u = new UnitType();
                component_info_list.Add(u);

                u.uRef = "unit_" + facet_instance;
                u.iRef = i.iRef;
                u.@private = false;
                u.privateSpecified = true;
                u.facet = facet_instance;
                //TODO          u.multiple;
                //TODO          u.multipleSpecified;
                //TODO          u.replica;
                //TODO          u.replicaSpecified;
                //TODO          u.slices;
                //u.super;
                //u.visibleInterface;
                //u.visualDescription;

                facet_instance++;
            }

        }

        private void setFusionsFromVirtualPlatforms(IList<object> component_info_list)
        {
            foreach (KeyValuePair<string, IVirtualPlatform> vp in VirtualPlatform)
            {
                FusionType f = new FusionType();
                component_info_list.Add(f);

                f.pRef = vp.Key;
                f.cRefs = new string[vp.Value.Component.Length];

                for (int i = 0; i < vp.Value.Component.Length; i++)
                    f.cRefs[i] = vp.Value.Component[i].Name;
            }
        }

        private void setInnerRenamingsFromVirtualPlatforms(IList<object> component_info_list)
        {
            foreach (KeyValuePair<string, IVirtualPlatform> vp in VirtualPlatform)
            {
                foreach (IServiceSolutionComponent p in vp.Value.Component)
                {
                    InnerRenamingType ir = new InnerRenamingType();
                    component_info_list.Add(ir);

                    ir.cRef = p.Name;
                    ir.cNewName = vp.Key;
                    ir.cOldName = Constants.VIRTUAL_PLATFORM_DEFAULT_ID;
                }
            }
        }

        private void setFusionsFromBindings(IList<object> component_info_list)
        {
            foreach (KeyValuePair<string, IBinding> b in Binding)
            {
                FusionType f = new FusionType();
                component_info_list.Add(f);

                f.pRef = b.Key;
                f.cRefs = new string[b.Value.Port.Length];

                for (int i = 0; i < b.Value.Port.Length; i++)
                    f.cRefs[i] = b.Value.Port[i].Item1.Component.Name;
            }
        }

        private void setInnerRenamingsFromBindings(IList<object> component_info_list)
        {
            foreach (KeyValuePair<string, IBinding> b in Binding)
            {
                foreach (Tuple<IPort, int> p in b.Value.Port)
                {
                    InnerRenamingType ir = new InnerRenamingType();
                    component_info_list.Add(ir);

                    ir.cRef = p.Item1.Component.Name;
                    ir.cNewName = b.Key;
                    ir.cOldName = p.Item1.Name;
                    ir.index_replica = p.Item1.Index;
                }
            }
        }

        private void setInnerComponents(IList<object> component_info_list)
        {
            foreach (KeyValuePair<string, IServiceSolutionComponent> c in SolutionComponent)
            {
                string qualified_name = (string)c.Value.Contract["name"];
                int pos = qualified_name.LastIndexOf('.');
                string name = qualified_name.Substring(pos + 1, qualified_name.Length - 1);
                string package = qualified_name.Substring(0, pos - 1);

                IList<ParameterRenaming> parameter_list = new List<ParameterRenaming>();
                calculateParameters(component_info_list, parameter_list, c.Value.Contract);

                IList<InnerComponentType> port_list = new List<InnerComponentType>();
                calculatePorts(component_info_list, port_list, c.Value);

                InnerComponentType ic = new InnerComponentType();
                component_info_list.Add(ic);

                ic.exposed = false;
                ic.exposedSpecified = true;
                //ic.hash_component_UID;
                //ic.index_replica;
                ic.localRef = c.Key;
                ic.multiple = false;
                ic.multipleSpecified = true;
                ic.name = name;
                ic.package = package;
                ic.location = package + Path.PathSeparator + name + ".hpe"; ;
                ic.parameter = new ParameterRenaming[parameter_list.Count];
                parameter_list.CopyTo(ic.parameter, 0);
                //ic.parameter_id;
                ic.port = new InnerComponentType[port_list.Count];
                port_list.CopyTo(ic.port, 0);
                //ic.revokeMultipleFacet;
                //ic.unitBounds;
                //ic.version;
                //ic.visualDescription;

            }
        }

        private void calculatePorts(IList<object> component_info_list, IList<InnerComponentType> port_list, IServiceSolutionComponent c)
        {
            foreach (IPort[] port_array in c.Port.Values)
            {
                foreach (IPort port in port_array)
                {
                    string qualified_name = null; // TODO: (string)port.Contract["name"];
                    int pos = qualified_name.LastIndexOf('.');
                    string name = qualified_name.Substring(pos + 1, qualified_name.Length - 1);
                    string package = qualified_name.Substring(0, pos - 1);

                    InnerComponentType c_port = new InnerComponentType();
                    port_list.Add(c_port);

                    IList<ParameterRenaming> parameter_list = new List<ParameterRenaming>();
                    calculateParameters(component_info_list, parameter_list, null /*TODO: port.Contract*/);

                    c_port.localRef = port.Name;
                    c_port.exposed = false;
                    c_port.exposedSpecified = true;
                    //c_port.hash_component_UID;
                    c_port.index_replica = port.Index;
                    c_port.multiple = false;
                    c_port.multipleSpecified = true;
                    c_port.name = name;
                    c_port.package = package;
                    c_port.location = package + Path.PathSeparator + name + ".hpe"; ;
                    c_port.parameter = new ParameterRenaming[parameter_list.Count];
                    parameter_list.CopyTo(c_port.parameter, 0);
                    //c_port.parameter_id;
                    //c_port.port;
                    //c_port.revokeMultipleFacet;
                    //c_port.unitBounds;
                    //c_port.version;
                    //c_port.visualDescription;
                }
            }
        }

        private void setParameters(IList<object> component_info_list)
        {
            int order = 0;
            foreach (KeyValuePair<string, object> context_parameter in Contract)
            {
                Tuple<string, IDictionary<string, object>> context_parameter_desc = (Tuple<string, IDictionary<string, object>>)context_parameter.Value;
                string parameter_id = context_parameter.Key;
                string var_id = context_parameter_desc.Item1;
                IDictionary<string, object> bound = context_parameter_desc.Item2;

                ParameterType p = new ParameterType();

                p.componentRef = newParameterInnerComponent(component_info_list, parameter_id, bound);
                p.formFieldId = parameter_id;
                p.order = order++;
                p.orderSpecified = true;
                p.variance = VarianceType.contravariant;
                p.varName = var_id;

                component_info_list.Add(p);
            }
        }

        private string newParameterInnerComponent(IList<object> component_info_list, string parameter_id, IDictionary<string, object> bound)
        {
            string qualified_name = (string)bound["name"];
            int pos = qualified_name.LastIndexOf('.');
            string name = qualified_name.Substring(pos + 1, qualified_name.Length - 1);
            string package = qualified_name.Substring(0, pos - 1);

            IList<ParameterRenaming> parameter_list = new List<ParameterRenaming>();
            calculateParameters(component_info_list, parameter_list, bound);

            InnerComponentType inner = new InnerComponentType();
            component_info_list.Add(inner);

            inner.exposed = false;
            inner.exposedSpecified = false;
            //inner.hash_component_UID;
            //inner.index_replica;
            inner.localRef = parameter_id;
            inner.multiple = false;
            inner.multipleSpecified = true;
            inner.location = package + Path.PathSeparator + name + ".hpe";
            inner.name = name;
            inner.package = package;
            inner.parameter = new ParameterRenaming[parameter_list.Count];
            parameter_list.CopyTo(inner.parameter, 0);
            inner.parameter_id = parameter_id;
            //inner.port;
            //inner.revokeMultipleFacet;
            //inner.unitBounds;
            //inner.version;
            //inner.visualDescription;

            // TODO: argumentos do bound ...

            return inner.localRef;
        }


        private void calculateParameters(IList<object> component_info_list, IList<ParameterRenaming> parameter_list, IDictionary<string, object> bound)
        {
            foreach (KeyValuePair<string, object> par in bound)
                if (!par.Key.Equals("name"))
                {
                    ParameterRenaming par_renaming = new ParameterRenaming();
                    par_renaming.formFieldId = par.Key;
                    if (par.Value is IDictionary<string, object>)
                    {
                        par_renaming.varName = "v" + DateTime.Now.Millisecond;
                        ParameterSupplyType par_supply = new ParameterSupplyType();
                        par_supply.cRef = newParameterInnerComponent(component_info_list, par.Key, (IDictionary<string, object>)par.Value);
                        par_supply.direct = true;
                        par_supply.varName = par_renaming.varName;
                        component_info_list.Add(par_supply);
                    }
                    else if (par.Value is string)
                    {
                        par_renaming.varName = (string)par.Value;
                    }
                    parameter_list.Add(par_renaming);
                }
        }

        public void register()
        {
            throw new NotImplementedException();
        }

        public void addParameter(string parameter_id, string var_id, IDictionary<string, object> bound)
        {
            this.var_mapping[var_id] = parameter_id;

            IDictionary<string, object> new_bound = new Dictionary<string, object>();
            determineContractVariables(bound, new_bound);

            this.ContextualSignature[parameter_id] = new Tuple<string, object>(var_id, new_bound);
            this.Contract[parameter_id] = new_bound;

            CoreServices.insertParameter(parameter_id, LoaderApp.serialize<IDriverContract>(XMLConverter.convertContract(new_bound)));
            CoreServices.updateContract(LoaderApp.serialize<IDriverContract>(XMLConverter.convertContract(this.Contract)));
        }

        public void setArgument(string parameter_id, object argument)
        {
            this.Contract[parameter_id] = argument;
            CoreServices.updateContract(LoaderApp.serialize<IDriverContract>(XMLConverter.convertContract(this.Contract)));
        }

        public string getVirtualPlatformAddress(string name)
        {
           return CoreServices.getVirtualPlatformAddress(name);
        }
    }
}
