﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal abstract class ServiceSolutionComponent : SolutionComponent, IServiceSolutionComponent
    {
        public IDictionary<string, IUserServicePort[]> UserPort => new Dictionary<string, IUserServicePort[]>(user_port);
        public IDictionary<string, IProviderServicePort[]> ProviderPort => new Dictionary<string, IProviderServicePort[]>(provider_port);
        public IDictionary<string, IServicePort[]> ServicePort => new Dictionary<string, IServicePort[]>(service_port);
        public IDictionary<string, IPort[]> Port => new Dictionary<string, IPort[]>(port);

        readonly IDictionary<string, IUserServicePort[]> user_port = new Dictionary<string, IUserServicePort[]>();
        readonly IDictionary<string, IProviderServicePort[]> provider_port = new Dictionary<string, IProviderServicePort[]>();
        readonly IDictionary<string, IServicePort[]> service_port = new Dictionary<string, IServicePort[]>(); // user_port + provider_port
        readonly protected IDictionary<string, IPort[]> port = new Dictionary<string, IPort[]>();

        protected void addServicePort(IServicePort[] port)
        {
            string name = port[0].Name;

            this.port[name] = port;
            this.service_port[name] = port;

            if (port is IUserServicePort[] || port is IUserProviderServicePort[])
                this.user_port[name] = (IUserServicePort[])port;

            if (port is IProviderServicePort[] || port is IUserProviderServicePort[])
                this.provider_port[name] = (IProviderServicePort[])port;
        }

        protected ServiceSolutionComponent(ISystem system_component, string cRef, IDictionary<string, object> contract, CoreServices core_services, Tuple<int, IVirtualPlatform>[] placement) : base(system_component, cRef, contract, core_services, placement)
        {
        }


        protected override void createComponent()
        {
            base.createComponent();
        }


        protected void populateServicePorts(Tuple<string, int, int, string>[] l)
        {
            string id_inner = null;
            IList<IPort> port_list = new List<IPort>();
            IList<IUserServicePort> user_port_list = new List<IUserServicePort>();
            IList<IProviderServicePort> provider_port_list = new List<IProviderServicePort>();
            IList<IServicePort> service_port_list = new List<IServicePort>();

            if (l.Length > 0)
            {
                foreach (Tuple<string, int, int, string> e in l)
                    if (e.Item4.Equals("client") || e.Item4.Equals("server") || e.Item4.Equals("client+server")) 
                    {
                        int i = e.Item2;
                        int facet = e.Item3;
                        string role = e.Item4;

                        if (id_inner == null)
                        {
                            id_inner = e.Item1;
                        }
                        else if (!id_inner.Equals(e.Item1))
                        {
                            if (user_port_list.Count > 0)
                            {
                                user_port[id_inner] = new IUserServicePort[user_port_list.Count];
                                user_port_list.CopyTo(user_port[id_inner], 0);
                                user_port_list = new List<IUserServicePort>();
                            }

                            if (provider_port_list.Count > 0)
                            {
                                provider_port[id_inner] = new IProviderServicePort[provider_port_list.Count];
                                provider_port_list.CopyTo(provider_port[id_inner], 0);
                                provider_port_list = new List<IProviderServicePort>();
                            }

                            if (service_port_list.Count > 0)
                            {
                                service_port[id_inner] = new IServicePort[service_port_list.Count];
                                service_port_list.CopyTo(service_port[id_inner], 0);
                                service_port_list = new List<IServicePort>();
                            }

                            if (port_list.Count > 0)
                            {
                                port[id_inner] = new IPort[port_list.Count];
                                port_list.CopyTo(port[id_inner], 0);
                                port_list = new List<IPort>();
                            }

                            id_inner = e.Item1;
                        }

                        IServicePort port_ = null;

                        if (role.Equals("client"))
                        {
                            port_ = new UserServicePort(this, id_inner, facet, i);
                            user_port_list.Add((IUserServicePort)port_);
                        }
                        else if (role.Equals("server"))
                        {
                            port_ = new ProviderServicePort(this, id_inner, facet, i);
                            provider_port_list.Add((IProviderServicePort)port_);
                        }
                        else if (role.Equals("client+server"))
                        {
                            IUserProviderServicePort user_provider_port = new UserProviderServicePort(this, id_inner, facet, i);
                            user_port_list.Add(user_provider_port);
                            provider_port_list.Add(user_provider_port);
                        }

                        service_port_list.Add(port_);
                        port_list.Add(port_);

                    }

                if (user_port_list.Count > 0)
                {
                    user_port[id_inner] = new IUserServicePort[user_port_list.Count];
                    user_port_list.CopyTo(user_port[id_inner], 0);
                }

                if (provider_port_list.Count > 0)
                {
                    provider_port[id_inner] = new IProviderServicePort[provider_port_list.Count];
                    provider_port_list.CopyTo(provider_port[id_inner], 0);
                }

                if (service_port_list.Count > 0)
                {
                    service_port[id_inner] = new IServicePort[service_port_list.Count];
                    service_port_list.CopyTo(service_port[id_inner], 0);
                }

                if (port_list.Count > 0)
                {
                    port[id_inner] = new IPort[port_list.Count];
                    port_list.CopyTo(port[id_inner], 0);
                }
            }
        }
    }
}
