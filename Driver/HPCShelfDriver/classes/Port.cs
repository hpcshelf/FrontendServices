﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    internal abstract class Port: IPort
    {
        readonly string name;
        private IBinding binding;
		//readonly IDictionary<string, object> contract;
        readonly IServiceSolutionComponent component;
        readonly int index;
        readonly int facet;


		protected Port(IServiceSolutionComponent component, string name, int facet, int index/*, IDictionary<string, object> contract*/)
        {
            //this.contract = contract;
            this.facet = facet;
            this.index = index;
            this.component = component;
            this.name = name;
        }

        public void setBinding(IBinding binding)
        {
            this.binding = binding;
        }

        public string Name => name;
        public IBinding Binding => binding;
       // public IDictionary<string, object> Contract => contract;
        public IServiceSolutionComponent Component => component;
        public int Index => index;
        public int Facet => facet;
	}
}
