﻿using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal sealed class FinalVirtualPlatform : VirtualPlatform
    {
        private IList<IServiceSolutionComponent> component = new List<IServiceSolutionComponent>();

        internal FinalVirtualPlatform(ISystem system_component, string cRef, IDictionary<string, object> contract, CoreServices core_services) : base(system_component, cRef, contract, core_services)
        {
			createComponent();
			((Workflow)system_component.Workflow).addLifeCyclePort(this);
		}

	}
}