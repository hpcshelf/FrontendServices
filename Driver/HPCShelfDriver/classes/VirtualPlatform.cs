﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal abstract class VirtualPlatform : SolutionComponent, IVirtualPlatform
    {
        private IList<IServiceSolutionComponent> component = new List<IServiceSolutionComponent>();

        internal VirtualPlatform(ISystem system_component, string cRef, IDictionary<string, object> contract, CoreServices core_services) : base(system_component, cRef, contract, core_services, new Tuple<int, IVirtualPlatform> [0] {})
        {
		}

        public void addComponent(IServiceSolutionComponent component)
        {
            this.component.Add(component);
        }

        protected override void createComponent()
        {
           // CoreServices.updatePlatforms(this.Name);
            base.createComponent();
        }

		public IServiceSolutionComponent[] Component
        {
            get
            {
                IServiceSolutionComponent[] result = new IServiceSolutionComponent[component.Count];
                component.CopyTo(result, 0);
                return result;
            }
        }
    }
}
