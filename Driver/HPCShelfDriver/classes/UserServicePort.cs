﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    internal class UserServicePort: ServicePort, IUserServicePort
    {
        internal UserServicePort(IServiceSolutionComponent component, string name, int facet, int index/*, IDictionary<string, object> contract*/) : base(component, name, facet, index/*, contract*/)        {
        }
    }
}
