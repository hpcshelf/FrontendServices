﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal abstract class SolutionComponent : Component, ISolutionComponent
    {
        protected SolutionComponent(ISystem system_component, string cRef, IDictionary<string, object> contract, CoreServices core_services, Tuple<int, IVirtualPlatform>[] placement) : base(system_component, cRef, contract, core_services, placement)
		{
        }

	}
}
