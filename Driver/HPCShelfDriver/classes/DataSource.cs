﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal sealed class DataSource: ServiceSolutionComponent, IDataSource
    {
        //readonly IVirtualPlatform placement;

        internal DataSource(ISystem system_component, string cRef, IDictionary<string, object> contract, IVirtualPlatform placement, CoreServices core_services) : base(system_component, cRef, contract, core_services, new Tuple<int, IVirtualPlatform>[1] { new Tuple<int, IVirtualPlatform>(0, placement) })
        {
			createComponent();
			((Workflow)system_component.Workflow).addLifeCyclePort(this);
		}

		protected override void populatePorts(Tuple<string, int, int, string>[] ports)
		{
			populateServicePorts(ports);
		}

		public IVirtualPlatform VirtualPlatform => this.Placement[0][0];

    }
}
