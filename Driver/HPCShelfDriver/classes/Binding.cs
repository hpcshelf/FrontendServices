﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal abstract class Binding: SolutionComponent, IBinding
    {
        readonly string cRef;
        readonly Tuple<IPort,int>[] port;
        readonly IDictionary<IVirtualPlatform, IPort[]> port_placement = new Dictionary<IVirtualPlatform, IPort[]>();
        readonly IDictionary<string, object> contract;

        protected Binding(ISystem system_component, string cRef, IDictionary<string, object> contract, Tuple<IPort, int>[] port, CoreServices core_services) : base(system_component, cRef, contract, core_services, mk(port))
        {
            this.port = port;
            this.cRef = cRef;
            this.contract = contract;
            foreach (Tuple<IPort, int> p in port)
                p.Item1.setBinding(this);
            this.port_placement = mapPortToVirtualPlatform(port);
		}

        private static Tuple<int, IVirtualPlatform>[] mk(Tuple<IPort, int>[] port_placement)
        {
            IList<Tuple<int,IVirtualPlatform >> placement = new List<Tuple<int, IVirtualPlatform>>();
            foreach (Tuple<IPort,int> t in port_placement)
            {
                IPort port = t.Item1;
                int port_facet = port.Facet;
                int index = port.Index;
                placement.Add(new Tuple<int, IVirtualPlatform>(t.Item2, port.Component.Placement[port_facet][index]));
            }

            Tuple<int, IVirtualPlatform>[] result = new Tuple<int, IVirtualPlatform>[placement.Count];
            placement.CopyTo(result,0);
            return result;
        }

		private static IDictionary<IVirtualPlatform, IPort[]> mapPortToVirtualPlatform(Tuple<IPort, int>[] port_placement)
		{
            IList<Tuple<IVirtualPlatform, IPort>> m = new List<Tuple<IVirtualPlatform, IPort>>();
			foreach (Tuple<IPort, int> t in port_placement)
			{
				IPort port = t.Item1;
				int port_facet = port.Facet;
                int index = port.Index;
                m.Add(new Tuple<IVirtualPlatform, IPort>(port.Component.Placement[port_facet][index], port));
			}

            IDictionary<IVirtualPlatform, IPort[]> result = new Dictionary<IVirtualPlatform, IPort[]>();

            foreach (Tuple<IVirtualPlatform, IPort> t in m)
            {
                IPort[] ps;
                if (!result.TryGetValue(t.Item1, out ps))
                    result[t.Item1] = new IPort[1] { t.Item2 };
                else
                {
                    result.Remove(t.Item1);
                    result[t.Item1] = new IPort[ps.Length + 1];
                    ps.CopyTo(result[t.Item1], 0);
                    result[t.Item1][ps.Length] = t.Item2;
                }
            }

			return result;
		}


		public Tuple<IPort, int>[] Port => port;

        public IDictionary<IVirtualPlatform, IPort[]> PortPlacement => port_placement;
    }
}
