﻿using System;
using System.Collections.Generic;
using org.hpcshelf.DGAC;

namespace HPCShelfDriver
{
    public interface ISystem : IComponent
    {
        IDictionary<string, Tuple<string, object>> ContextualSignature { get; set; }

        IWorkflow Workflow { set; get; }
        IApplication Application { set; get; }
        IPlatformSAFe PlatformSAFe { get; }

        IDictionary<string, IComponent> Component { get; }
        IDictionary<string, IServiceSolutionComponent> SolutionComponent { get; }
        IDictionary<string, IComputation> Computation { get; }
        IDictionary<string, IConnector> Connector { get; }
        IDictionary<string, IVirtualPlatform> VirtualPlatform { get; }
        IDictionary<string, IDataSource> DataSource { get; }
        IDictionary<string, IBinding> Binding { get; }
        IDictionary<string, IServiceBinding> ServiceBinding { get; }
        IDictionary<string, IActionBinding> ActionBinding { get; }

        IDictionary<string, string> VarMapping { get; }

        void register();

        string createConfiguration(string system_namespace);

        void addParameter(string parameter_id, string var_id, IDictionary<string, object> bound);
        void setArgument(string parameter_id, object argument);

        IComputation newComputation(string cRef, IDictionary<string, object> contract, IVirtualPlatform placement);
        IConnector newConnector(string cRef, IDictionary<string, object> contract, Tuple<int, IVirtualPlatform>[] facet_placement);
        IVirtualPlatform newVirtualPlatform(string cRef, IDictionary<string, object> contract);
        IDataSource newDataSource(string cRef, IDictionary<string, object> contract, IVirtualPlatform placement);
        IServiceBinding newServiceBinding(string cRef, IDictionary<string, object> contract, IUserServicePort user_port, IProviderServicePort provider_port);
        IActionBinding newActionBinding(string cRef, string port_type, IActionPort[] action_port);
        void createComputation(string qname, string wrapper_source_fn, string[] sources, IDictionary<string, object>[] ports, string mp_library, string language);
        void createComputation(string qname, string wrapper_source_fn, string[] sources, string language);
        void createComputation(string qname, string wrapper_source_fn, string[] sources, string mp_library, string language);
        void createComputation(string qname, string wrapper_source_fn, string[] sources);
        void createComputation(string qname, string[] sources, string mp_library, string language);
        void createComputation(string qname, string[] sources, string language);
        void createComputation(string qname, string[] sources, IDictionary<string, object>[] ports);
        void createComputation(string qname, string[] sources);
        void terminate();
        void release(string name);
        string getVirtualPlatformAddress(string name);
    }
}
