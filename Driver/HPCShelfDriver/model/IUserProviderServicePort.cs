﻿using System;
namespace HPCShelfDriver
{
    public interface IUserProviderServicePort : IUserServicePort, IProviderServicePort
    {
    }
}
