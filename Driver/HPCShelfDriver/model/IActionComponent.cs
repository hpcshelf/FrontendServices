﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    public interface IActionComponent: IServiceSolutionComponent
    {
		IDictionary<string, IActionPort[]> ActionPort { get; }
	}
}
